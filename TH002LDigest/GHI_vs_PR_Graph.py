import requests.auth
import pandas as pd
import datetime
import os
import re 
import time
import shutil
import pytz
import sys
import pyodbc
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.patches as mpatch
import matplotlib.dates as mdates
from scipy import stats
from matplotlib.ticker import MaxNLocator
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error, r2_score

date=sys.argv[1]
path='/home/admin/Graphs/Graph_Extract/TH-002/[TH-002L] Graph '+date+' - PR Evolution.txt'
path_write='/home/admin/Graphs/'

df=pd.read_csv(path,sep='\t')

df_pr=df[['Date','GTI','W_PR2']]
df_pr=df_pr[df_pr['Date']>'2019-12-31']
df_pr=df_pr[df_pr['Date']<=date]
df_pr=df_pr.loc[((df_pr['W_PR2']>0) & (df_pr['W_PR2']<95)),:]
no_points=len(df_pr)
avg_pr=df_pr['W_PR2'].mean()
df_pr['Date']=pd.to_datetime(df_pr['Date'])

plt.rcParams.update({'font.size': 28})

#Plot 3
fig3 = plt.figure(num=None, figsize=(55  , 30))
ax3 = fig3.add_subplot(111)
a=ax3.scatter(df_pr['GTI'].tolist(), df_pr['W_PR2'],color='darkorange',s=110)
ax3.set_ylabel('Performance Ratio [%]', fontsize=32)
ax3.set_xlabel('Global Horizontal Irradiance [W/m$^2$]', fontsize=32)
ax3.set_ylim([0,100])
plt.legend([a],['PR'],scatterpoints=1,loc=(0.02,.9),ncol=1,fontsize=25)
plt.gca().xaxis.set_major_locator(MaxNLocator(prune='lower'))

X = df_pr['GTI'].values.reshape(-1, 1)  # values converts it into a numpy array
Y = df_pr['W_PR2'].values.reshape(-1, 1)  # -1 means that calculate the dimension of rows, but have 1 column
linear_regressor = LinearRegression()  # create object for the class
linear_regressor.fit(X, Y)  # perform linear regression
Y_pred = linear_regressor.predict(X)  # make predictions
print(Y_pred)
plt.plot(X, Y_pred, color='red', linewidth=9)
r=(r2_score(Y, Y_pred))

ax3.annotate('No. of Points: '+str(no_points)+'\n\nAverage PR [%]: '+str(round(avg_pr,1))+'\n\nR$^2$: : '+str(round(r,6)), (.68, .1),xytext=(4, -4),xycoords='axes fraction',textcoords='offset points',fontweight='bold',color='black',ha='left', bbox=dict(boxstyle="square", fc="none", ec="black",lw=5), va='bottom',size=25)
fig3.savefig(path_write+"Graph_Output/TH-002/[TH-002] Graph - PR vs GHI (2020).pdf", bbox_inches='tight')
