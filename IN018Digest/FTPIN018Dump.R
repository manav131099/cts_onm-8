require('RCurl')
require('R.utils')
require('utils')
source('/home/admin/CODE/MasterMail/timestamp.R')
FIREMAIL = 0
retryCnt = 0
serverdownmail = function()
{
	send.mail(from = 'operations@cleantechsolar.com',
	          to = c('operations@cleantechsolar.com'),
						subject = "IN-018X FTP server down",
						body = " ",
						smtp = list(host.name = "smtp.office365.com", port = 587, user.name = 'shravan.karthik@cleantechsolar.com', passwd = 'CTS&*(789', tls = TRUE),
						authenticate = TRUE,
						send = TRUE,																												
						debug = F)
}
serverupmail = function()
{
	send.mail(from = 'operations@cleantechsolar.com',
	          to = c('operations@cleantechsolar.com'),
						subject = "IN-018X FTP server up and running",
						body = " ",
						smtp = list(host.name = "smtp.office365.com", port = 587, user.name = 'shravan.karthik@cleantechsolar.com', passwd = 'CTS&*(789', tls = TRUE),
						authenticate = TRUE,
						send = TRUE,																												
						debug = F)
}
dumpftp = function(days,path,mtno)
{
	print('Calling function')
  url = "ftp://IN-018:GD62NSJD8KWU@ftpnew.cleantechsolar.com/"
  filenames = try(evalWithTimeout(getURLContent(url,ftp.use.epsv = FALSE,dirlistonly = TRUE,timeout = 600,ssl.verifypeer = FALSE,useragent="R"),
	 timeout = 600, onTimeout = "error"),silent=T)
	if(class(filenames) == 'try-error')
	{
		print('Error in FTP server, will reconnect in 10 mins')
		retryCnt <<- retryCnt + 1
		if(FIREMAIL==0 && retryCnt > 3)
		{
			serverdownmail()
			FIREMAIL<<-1
		}
		return(0)
	}
	retryCnt <<-0
	if(FIREMAIL==1)
	{
		print('Server Back running...')
		serverupmail()
		FIREMAIL<<-0
	}
	print('Passed test')
	recordTimeMaster("IN-018X","FTPProbe")
	newfilenames = try(unlist(strsplit(filenames,"\n")),silent = T)
	if(class(newfilenames)=='try-error')
	return(1)
	print('Strsplit done')
  newfilenames = newfilenames[grepl("zip",newfilenames)]
  newfilenames = newfilenames[(!grepl("tmp",newfilenames))]
	newfilenames = newfilenames[grepl("S&P",newfilenames)]
	print('zip match found')
	days2 = unlist(strsplit(days,"\\."))
	seq1 = seq(from = 1,to = length(days2),by=2)
	days2 = days2[seq1]
	newfilenames2 = unlist(strsplit(newfilenames,"\\."))
	seq1 = seq(from=1,to=length(newfilenames2),by=2)
	newfilenames2 = newfilenames2[seq1]
  match = match(days2,newfilenames2)
	print('Matches found')
	match = match[complete.cases(match)]
	if(length(match) < 1)
	{
	  print('No new files')
		return(1)
	}
  newfilenames = newfilenames[-match]
  if(length(newfilenames) < 1)
	{
	  print('No new files')
		return(1)
	}

	for(y in 1 : length(newfilenames))
  {
    urlnew = paste(url,newfilenames[y],sep="")
    file = try(download.file(urlnew,destfile = paste(path,newfilenames[y],sep="/")),silent = T)
		if(class(file) == 'try-error')
		{
			print(paste('couldnt download',newfilenames[y]))
			next
		}
	  unzipping = try(unzip(paste(path,newfilenames[y],sep="/"),exdir = path),silent = T)
		if(class(unzipping) == 'try-error')
		{
			print(paste('Couldnt extract',newfilenames[y]))
		}
		system(paste('rm "',path,'/',newfilenames[y],'"',sep = ""))
	}
	recordTimeMaster("IN-018X","FTPNewFiles",as.character(newfilenames[length(newfilenames)]))
	return(1)
}
