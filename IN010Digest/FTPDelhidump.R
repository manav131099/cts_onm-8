require('RCurl')
require('utils')
require('R.utils')
source('/home/admin/CODE/MasterMail/timestamp.R')
FIREMAIL = 0
retryCnt = 0
serverdownmail = function()
{
	retryCnt= 0;
	while(retryCnt < 10)
	{
		retryCnt = retryCnt + 1
		mailSend = try(send.mail(from = 'operations@cleantechsolar.com',
	  	        to = c('operations@cleantechsolar.com'),
							subject = "IN-010X FTP server down",
							body = " ",
							smtp = list(host.name = "smtp.office365.com", port = 587, 
							user.name = 'shravan.karthik@cleantechsolar.com', passwd = 'CTS&*(789', tls = TRUE),
							authenticate = TRUE,
							send = TRUE,																												
							debug = F),silent=T)
	
		if(class(mailSend)=='try-error')
			next
		break
	}
}

serverupmail = function()
{
	retryCnt= 0;
	while(retryCnt < 10)
	{
		retryCnt = retryCnt + 1
		mailSend = try(send.mail(from = 'operations@cleantechsolar.com',
	          to = c('operations@cleantechsolar.com'),
						subject = "IN-010X FTP server up and running",
						body = " ",
						smtp = list(host.name = "smtp.office.com", port = 587, 
						user.name = 'shravan.karthik@cleantechsolar.com', passwd = 'CTS&*(789', tls = TRUE),
						authenticate = TRUE,
						send = TRUE,																												
						debug = F),silent=T)
		if(class(mailSend)=='try-error')
			next
		break
	}
}
dumpftp = function(days,path)
{
 	print('Enter function')
  url = "ftp://IN-010X:RqobbfyPERE3RAig@52.148.87.44"
  filenames = try(evalWithTimeout(getURLContent(url,ftp.use.epsv = FALSE,dirlistonly = TRUE),
	 timeout = 600, onTimeout = "error"),silent=T)
	print('try success')
	if(class(filenames) == 'try-error')
	{
		print('Error in FTP server, will reconnect in 10 mins')
		retryCnt <<- retryCnt + 1
		if(FIREMAIL==0 && retryCnt > 3)
		{
			serverdownmail()
			FIREMAIL<<-1
		}
		return(0)
	}
	retryCnt <<-0
	if(FIREMAIL==1)
	{
	  print('Server up and running...')
		serverupmail()
		FIREMAIL<<-0
	}
	if(class(filenames)!='character')
	{
		print('Cant split filename as not character')
		return(0)
	}
	recordTimeMaster("IN-010X","FTPProbe")
	print('Filenames obtained')
  newfilenames = unlist(strsplit(filenames,"\n"))
	print('Filenames split')
  newfilenames = newfilenames[grepl("csv",newfilenames)]
	print('CSVs found')
 # match = match(days,newfilenames)
#	match = match[complete.cases(match)]
	match = match(newfilenames,days)
	match = which(match %in% NA)
	if(length(match) < 1)
	{
	  print('No new files')
		return(1)
	}
	print('Match Done')
  newfilenames = newfilenames[match]
  if(length(newfilenames) < 1)
	{
	  print('No new files')
		return(1)
	}
	for(y in 1 : length(newfilenames))
  {
    urlnew = paste(url,newfilenames[y],sep="/")
    file = try(download.file(urlnew,destfile = paste(path,newfilenames[y],sep="/")) ,silent=T)
  }
	recordTimeMaster("IN-010X","FTPNewFiles",as.character(newfilenames[length(newfilenames)]))
	rm(filenames,newfilenames)
	gc()
	return(1)
}
