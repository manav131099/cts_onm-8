rm(list = ls())
TIMESTAMPSALARM = NULL
ltcutoff = .001
CABLOSSTOPRINT = 0
INSTCAP=c(53.76)
TOTSOLAR = 0
timetomins = function(x)
{
  hr = unlist(strsplit(x,":"))
	seq1 = seq(from = 1,to = length(hr),by =2)
	seq2 = seq(from = 2,to = length(hr),by =2)
  min = as.numeric(hr[seq2]) 
  hr = as.numeric(hr[seq1]) * 60
  return((hr + min + 1))
}
checkdir = function(x)
{
  if(!file.exists(x))
  {
    dir.create(x)
  }
}

fetchGSIData = function(date)
{
	pathMain = '/home/admin/Dropbox/Second Gen/[KH-001S]'
	yr = substr(date,1,4)
	mon = substr(date,1,7)
	txtFileName = paste('[KH-001S] ',date,".txt",sep="")
	pathyr = paste(pathMain,yr,sep="/")
	gsiVal = NA
	if(file.exists(pathyr))
	{
		pathmon = paste(pathyr,mon,sep="/")
		if(file.exists(pathmon))
		{
			pathfile = paste(pathmon,txtFileName,sep="/")
			if(file.exists(pathfile))
			{
				dataread = read.table(pathfile,sep="\t",header = T)
				gsiVal = as.numeric(dataread[,13])
			}
		}
	}
	return(c(gsiVal))
}

secondGenData = function(filepath,writefilepath)
{
	 TIMESTAMPSALARM <<- NULL
	
	print(paste('IN 2G',filepath))
  dataread = try(read.table(filepath,header = T,sep = "\t",stringsAsFactors=F),silent=T)
	if(class(dataread) == "try-error")
	{
		date = Eac1 = DA = Eac2 = Gpy=Yld1 = Yld2 = PR1 = PR2 = totrowsmissing = NA 
		lasttime = lastread = NA
      df = data.frame(Date = date,
									DA = DA,
									Eac1=Eac1, 
									Eac2=Eac2,
									Downtime = totrowsmissing,
									Yld1=Yld1,
									Yld2=Yld2,
									PR1 = PR1,
									PR2 = PR2,
									GPy=Gpy,
								 	LastTime=lasttime,
									LastRead=lastread,
									stringsAsFactors = F)
		
		return()
	}

	dataread2 = dataread
	colnoUse = 39
	{
	idxpac = 15
	dataread = dataread2[complete.cases(as.numeric(dataread2[,colnoUse])),]
	lasttime=lastread=Eac2=NA
	if(nrow(dataread)>0)
	{
	lasttime = as.character(dataread[nrow(dataread),1])
	lastread = as.character(round(as.numeric(dataread[nrow(dataread),colnoUse])/1000,3))
  Eac2 = format(round((as.numeric(dataread[nrow(dataread),colnoUse]) - as.numeric(dataread[1,colnoUse]))/1000,2),nsmall=1)
	}
	print(paste("Eac-2",Eac2))
	Yld2 = round(as.numeric(Eac2)/INSTCAP,2)
	dataread = dataread2[complete.cases(as.numeric(dataread2[,idxpac])),]
	dataread = dataread[as.numeric(dataread[,idxpac]) > 0,]
	Eac1 = NA
	if(nrow(dataread) > 1)
	{
   	Eac1 = format(round(sum(as.numeric(dataread[,idxpac]))/60000,1),nsmall=2)
	}
	Yld1 = round(as.numeric(Eac1)/INSTCAP,2)
	}
	
	dataread = dataread2
	datareadcp = dataread2
  DA = format(round(nrow(dataread)/14.4,1),nsmall=1)
  tdx = timetomins(substr(dataread[,1],12,16))
  dataread2 = dataread[tdx > 480,]
  tdx = tdx[tdx > 480]
  dataread2 = dataread2[tdx < 1020,]
	dataread2 = dataread2[complete.cases(as.numeric(dataread2[,idxpac])),]
  missingfactor = 540 - nrow(dataread2)
  dataread2 = dataread2[as.numeric(dataread2[,idxpac]) < ltcutoff,]
	if(length(dataread2[,1]) > 0)
	{
		TIMESTAMPSALARM <<- as.character(dataread2[,1])
	}
  totrowsmissing = format(round((missingfactor + nrow(dataread2))/5.4,1),nsmall=1)
	date = NA
	if(nrow(dataread)>0)
		date = substr(as.character(dataread[1,1]),1,10)
	Gpy = fetchGSIData(date)
	PR1 = round(Yld1*100/Gpy,1)
	PR2 = round(Yld2*100/Gpy,1)
  df = data.frame(Date = date,
							DA = DA,
							Eac1=Eac1, 
							Eac2=Eac2,
							Downtime = totrowsmissing,
							Yld1=Yld1,
							Yld2=Yld2,
							PR1 = PR1,
							PR2 = PR2,
							GPy=Gpy,
						 	LastTime=lasttime,
							LastRead=lastread,
							stringsAsFactors = F)
	write.table(df,file = writefilepath,row.names = F,col.names = T,sep="\t",append = F)
  return(df)
}

thirdGenData = function(filepathm1,writefilepath)
{
  dataread1 =read.table(filepathm1,header = T,sep="\t",stringsAsFactors=F) #its correct dont change
  {
    if(file.exists(writefilepath))
    {
      write.table(dataread1,file = writefilepath,row.names = F,col.names = F,sep = "\t",append = T)
    }
    else
    {
      write.table(dataread1,file = writefilepath,row.names = F,col.names = T,sep = "\t",append = F)
    }
  }
}

