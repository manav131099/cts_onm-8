import requests, json
import requests.auth
import pandas as pd
import datetime
import os
import re
import time
import shutil
import pytz
import sys
import pyodbc
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.patches as mpatch
from matplotlib.dates import DateFormatter
import matplotlib.dates as mdates
from datetime import date
from datetime import timedelta


d0 = date(2020, 1, 1)
# d1 = date.today()
# yesterday = d1 - timedelta(days = 1)
# d1 = yesterday
d1 = sys.argv[1]
d1_date = date(int(d1[0:4]), int(d1[5:7]), int(d1[8:10]))
#
# date_year = int(file[15:19])
# date_month = int(file[20:22])
# date_day = int(file[23:25])

date_list = pd.date_range(start=d0,end=d1).tolist()
for i in range(0,len(date_list)):
    date_list[i] = str(date_list[i])[0:10]


irr_data_fail_safe = [0]*288
filter_list = [0]*147
for i in range(72,219):
    irr_data_fail_safe[i] = 30
    filter_list[i-72] = i

start_date = '2020-01-01'
last_date = str(d1)[0:10]
path = "/home/admin/Dropbox/Gen 1 Data/[IN-072L]"
years = os.listdir(path)


GA_list1 = [-1]*len(date_list)
DA_MFM1_List = [-1]*len(date_list)

GA_list2 = [-1]*len(date_list)
DA_MFM2_List = [-1]*len(date_list)

GA_list3 = [-1]*len(date_list)
DA_MFM3_List = [-1]*len(date_list)

DA_WMS_List = [-1]*len(date_list)

for year in years:
    year_mons = os.listdir(path+'/'+year)

    for year_mon in year_mons:
        devices = os.listdir(path+'/'+year+'/'+year_mon)
        for device in devices:
            if(device == 'MFM_1' or  device == 'MFM_2' or  device == 'MFM_3'):
                device_path = path+'/'+year+'/'+year_mon+'/'+device
                files = os.listdir(device_path)
                for file in files:
                    date_year = int(file[15:19])
                    date_month = int(file[20:22])
                    date_day = int(file[23:25])

                    end_date = date(date_year,date_month,date_day)
                    date_idx = (end_date-d0).days
                    if(end_date>d1_date):
                        continue
                    if(date_year == 2019):
                        continue
                    if(file[15:25] == str(date.today())):
                        continue

                    path_2g = "/home/admin/Dropbox/Gen 1 Data/[IN-072L]/"+year+'/'+year_mon+'/'+device+'/'+file
                    if(os.path.exists(path_2g) == False):
                        continue
                    else:
                        da_file = pd.read_csv(path_2g,delimiter='\t')
                        count_da_check = da_file['Hz_avg'].isna().sum()
                        hz_len = len(da_file['Hz_avg'])
                        count_da_check = da_file['Hz_avg'].isna().sum()
                        da = (288-count_da_check)*100/288
                        if(device == 'MFM_1'):
                            DA_MFM1_List[date_idx] = da
                            print(da)
                        elif(device == 'MFM_2'):
                            DA_MFM2_List[date_idx] = da
                        elif(device == 'MFM_3'):
                            DA_MFM3_List[date_idx] = da

                    # path_2g = "/home/admin/Dropbox/Second Gen/[IN-072L]/"+year+'/'+year_mon+'/'+device+'/'+file
                    # if(os.path.exists(path_2g) == True):
                    #     da_file = pd.read_csv(path_2g,delimiter='\t')
                    #     da = da_file['DA']
                    #     if(device == 'MFM_1'):
                    #         DA_MFM1_List[date_idx] = da
                    #     elif(device == 'MFM_2'):
                    #         DA_MFM2_List[date_idx] = da
                    #     elif(device == 'MFM_3'):
                    #         DA_MFM3_List[date_idx] = da

                    irridation_fail_safe = False
                    path_2g = "/home/admin/Dropbox/Gen 1 Data/[IN-061L]/"+year+'/'+year_mon+'/'+'WMS_1/'+'[IN-061L]-WMS1-'+file[15:25]+'.txt'
                    if(os.path.exists(path_2g) == True):
                        da_file = pd.read_csv(path_2g,delimiter='\t')
                        count_da_check = da_file['POAI_avg'].isna().sum()
                        da = (288-count_da_check)*100/288

                        DA_WMS_List[date_idx] = da
                        # da = da_file['DA'][0]
                        if(float(da)<90):
                            count_irr = 145
                            irridation_fail_safe = True
                    irr_list = list()
                    freq_list = list()
                    file_path = path+'/'+year+'/'+year_mon+'/'+device+'/'+file
                    if(os.path.exists(file_path) == False):
                        continue
                    dataread_MFM = pd.read_csv(file_path,delimiter='\t')
                    df_MFM = pd.DataFrame()
                    df_MFM['timestamps'] = dataread_MFM['ts']
                    df_MFM['freq_data'] = dataread_MFM['Hz_avg']


                    wms_path = "/home/admin/Dropbox/Gen 1 Data/[IN-061L]/"+year+'/'+year_mon+'/'+'WMS_1'+'/'+'[IN-061L]-WMS1-'+file[15:25]+'.txt'
                    if(os.path.exists(wms_path) == False):
                        continue
                    dataread_WMS = pd.read_csv(wms_path, delimiter='\t')
                    df_WMS = pd.DataFrame()
                    df_WMS['timestamps'] = dataread_WMS['ts']
                    df_WMS['irr_data'] = dataread_WMS['POAI_avg']
                    if(irridation_fail_safe == True):
                        df_WMS['irr_data'] = irr_data_fail_safe

                    df_t1 = df_WMS[df_WMS['irr_data']>=20]
                    df_WMS = df_WMS.merge(df_MFM, on='timestamps')
                    #73 to 219 as 30
                    df_t2 = df_WMS[((df_WMS['irr_data']>=20) & (df_WMS['freq_data']>=40))]

                    count_irr = len(df_t1)
                    count_freq = len(df_t2)

                    if(count_irr == 0):
                        ga=0
                        print(count_irr)
                    else:
                        ga = round((count_freq*100)/count_irr,2)
                        if(count_freq == 0):
                            ga = -1


                    end_date = date(date_year,date_month,date_day)
                    date_idx = (end_date-d0).days
                    if(device == 'MFM_1'):
                        GA_list1[date_idx] = ga
                    elif(device == 'MFM_2'):
                        GA_list2[date_idx] = ga
                    elif(device == 'MFM_3'):
                        GA_list3[date_idx] = ga

for i in range(0,len(date_list)-1):
    for j in range(0,len(date_list)-1):
        if(date_list[j]>date_list[j+1]):
            date = date_list[j]
            date_list[j] = date_list[j+1]
            date_list[j+1] = date

            x = GA_list1[j]
            GA_list1[j] = GA_list1[j+1]
            GA_list1[j+1] = x

            x = GA_list2[j]
            GA_list2[j] = GA_list2[j+1]
            GA_list2[j+1] = x

            x = GA_list3[j]
            GA_list3[j] = GA_list3[j+1]
            GA_list3[j+1] = x

ga_avg = 0

dates_datetime = [datetime.datetime.strptime(d,"%Y-%m-%d").date() for d in date_list]

df_mfm = pd.DataFrame()
df_mfm.insert(0,'Date',dates_datetime,True)
df_mfm.insert(1,'GA_MFM1',GA_list1,True)
df_mfm.insert(2,'DA_MFM_1',DA_MFM1_List,True)

# df_mfm.insert(3,'GA_MFM2',GA_list2,True)
# df_mfm.insert(4,'DA_MFM_2',DA_MFM2_List,True)
#
# df_mfm.insert(5,'GA_MFM3',GA_list3,True)
# df_mfm.insert(6,'DA_MFM_3',DA_MFM3_List,True)

df_mfm.insert(3,'DA_WMS',DA_WMS_List,True)

df_mfm = df_mfm[((df_mfm['GA_MFM1']>0))]
# print(df_mfm.head(50))
date_list = df_mfm['Date']

ga_avg1 = df_mfm['GA_MFM1'].sum()
ga_avg1 = ga_avg1/len(df_mfm['GA_MFM1'])

# ga_avg2 = df_mfm['GA_MFM2'].sum()
# ga_avg2 = ga_avg2/len(df_mfm['GA_MFM2'])

# ga_avg3 = df_mfm['GA_MFM3'].sum()
# ga_avg3 = ga_avg3/len(df_mfm['GA_MFM3'])

count_pos = len(df_mfm['GA_MFM1'])
# for i in range(0,len(df_mfm['GA'])-1):
#     if(df_mfm['GA'][i]<50):
#         print(df_mfm['Date'][i])
#         print(df_mfm['GA'][i])
#         print('\n')

dates_datetime = [datetime.datetime.strptime(str(d),"%Y-%m-%d").date() for d in date_list]

dates_graph = list(df_mfm['Date'])
# last_date = dates_graph[len(dates_graph)-1]
GA1_Graph = list(df_mfm['GA_MFM1'])
# GA2_Graph = list(df_mfm['GA_MFM2'])
# GA3_Graph = list(df_mfm['GA_MFM3'])

GA_Marking = GA1_Graph
dates = dates_graph
font = {'size' : 30}

# df_finalData = pd.DataFrame()
# df_finalData.insert(0,'Date',dates_datetime,True)
# df_finalData.insert(1,'GA',GA_Graph,True)
# df_finalData.insert(2,'DA_MFM',df_mfm['DA_MFM'],True)
# df_finalData.insert(3,'DA_WMS',df_mfm['DA_WMS'],True)

data_extract_path = '/home/admin/Graphs/Graph_Extract/IN-072/[IN-072] Graph '+str(last_date)+' - Grid Availability.txt'

df_mfm.to_csv(data_extract_path, sep = '\t', index=False, header=True)

path_final = '/home/admin/Graphs/Graph_Output/IN-072/[IN-072] Graph '+str(last_date)+' - Grid Availability.pdf'

plt.rcParams.update({'font.size': 30})
plt.rcParams['axes.facecolor'] = 'gainsboro'

fig = plt.figure(num=None, figsize=(50, 30))
ax = fig.add_subplot(111)


plt.plot(dates_graph,GA1_Graph,marker='o',color='red',linewidth=5, label='MFM_1')
# plt.plot(dates_graph,GA2_Graph,marker='o',color='green',linewidth=5, label='MFM_2')
# plt.plot(dates_graph,GA3_Graph,marker='o',color='blue',linewidth=5, label='MFM_3')


plt.xticks(dates_graph)
plt.ylim([0,105])

date_form = DateFormatter("%b-%y")
ax.xaxis.set_major_formatter(date_form)
ax.xaxis.set_major_locator(mdates.MonthLocator(interval=3))
ax.set(xlim=['2020-01-01',str(dates_datetime[len(dates_datetime)-1])])

ttl = ax.set_title(str(dates_graph[0])+' to '+str(last_date), fontdict={'fontsize': 55, 'fontweight': 'medium'})
ttl.set_position([.485, 1.02])
ttl_main=fig.suptitle('[IN-072L] Grid Availability',fontsize=70)

plt.legend()
legend = plt.legend(loc='best', ncol=1, prop={'size':50})

lifetime_ga_text = 'Lifetime GA\nMFM_1: '+str(round(ga_avg1,2)) #+ '\nMFM_2: '+str(round(ga_avg2,2)) + '\nMFM_3: '+str(round(ga_avg3,2))
no_of_points_txt = 'No. of Points: '+str(count_pos)+' days'
plt.text(0.15, 0.2,no_of_points_txt, fontsize=50, transform=plt.gcf().transFigure,weight='bold',color='white',bbox=dict(facecolor='black', alpha=0.8,pad=10.0))
plt.text(0.15, 0.25,lifetime_ga_text, fontsize=50, transform=plt.gcf().transFigure,weight='bold',color='white',bbox=dict(facecolor='black', alpha=0.8,pad=10.0))


for index,i in enumerate(GA_Marking):
    if(i<80):
        ln1=plt.scatter(dates[index], i, marker='D',color='green',s=300)

for i, label in enumerate(dates):
    if(GA_Marking[i]<80):
        ax.annotate(label, (label, GA_Marking[i]+.25),size=25,color='green')

ax.set_ylabel('Grid Availability [%]', fontsize=60)
ax.set_xlabel('Date', fontsize=60)
fig.savefig(path_final, bbox_inches='tight')
