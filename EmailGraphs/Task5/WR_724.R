library(MASS)	
library(jpeg)	
library(openair)	
library(stringr)	
tempList = list()	
myList = list()	
directionNames = c("N","NE","E","SE","S","SW","W","NW")	
yearFiles = list.files(path = "/home/admin/Dropbox/Cleantechsolar/1min/[724]",all.files = F, full.names = T)	
idx = match("_gsdata_",yearFiles)	
if(is.finite(idx)) {	
  yearFiles = yearFiles[-idx]  	
}	
numOfYears = length(yearFiles) - 1	
readData = function() {	
  numOfFiles = length(files)	
  for (x in 1:numOfFiles) {	
    #extract data from .txt file	
    data = read.table(files[x], header = T, sep = "\t")	
    
    #extract data from table	
    dataDate = substr(files[x], (nchar(files[x]) - 13), (nchar(files[x]) - 4))	
    dataTime = unlist(lapply(lapply(data$Tm, as.POSIXct), format, "%H:%M:%S"))	
    
    numOfRows = length(data$Rec_ID)	
    overallDAPercentage = 100 * (numOfRows / 1440)	
    
    windSpeed = data$AvgWindS	
    windDirection = data$AvgWindD	
    
    for (k in 1:length(data$Rec_ID)) {	
      #Takes one data point every 30min to plot in graph	
      if (k%%1 == 0) {	
        tempList <<- append(tempList,c(dataDate, dataTime[k], overallDAPercentage, windSpeed[k], windDirection[k]))	
      }	
    }	
  }	
}	
#filtering out to only extract data from Aug 2018 and beyond	
extractData = function() {	
  if (y == 2018) {	
    for (m in 8:12) {	
      print(paste0("This is data for ", month.name[m], " ", toString(y),"."))	
      if (m < 10) {	
        pathprobe = paste0("/home/admin/Dropbox/Cleantechsolar/1min/[724]/",toString(y),"/",toString(y),"-","0",toString(m))	
        if (file.exists(pathprobe)) {	
          files <<- list.files(path = pathprobe, all.files = F, full.names = T)	
          readData()	
          
          #adding info to list and resetting variables	
          myList <<- append(myList,tempList)	
          tempList <<- list()	
        }	
        else {	
          print(paste0("File ", pathprobe, " does not exist!"))	
        }	
      }	
      else {	
        pathprobe = paste0("/home/admin/Dropbox/Cleantechsolar/1min/[724]/",toString(y),"/",toString(y),"-",toString(m))	
        if (file.exists(pathprobe)) {	
          files <<- list.files(path = pathprobe, all.files = F, full.names = T)	
          readData()	
          
          #adding info to list and resetting variables	
          myList <<- append(myList,tempList)	
          tempList <<- list()	
        }	
        else {	
          print(paste0("File ", pathprobe, " does not exist!"))	
        }	
      }	
    }	
  }	
  else {	
    for (m in 1:12) {	
      print(paste0("This is data for ", month.name[m], " ", toString(y),"."))	
      if (m < 10) {	
        pathprobe = paste0("/home/admin/Dropbox/Cleantechsolar/1min/[724]/",toString(y),"/",toString(y),"-","0",toString(m))	
        if (file.exists(pathprobe)) {	
          files <<- list.files(path = pathprobe, all.files = F, full.names = T)	
          readData()	
          
          #adding info to list and resetting variables	
          myList <<- append(myList,tempList)	
          tempList <<- list()	
        }	
        else {	
          print(paste0("File ", pathprobe, " does not exist!"))	
        }	
      }	
      else {	
        pathprobe = paste0("/home/admin/Dropbox/Cleantechsolar/1min/[724]/",toString(y),"/",toString(y),"-",toString(m))	
        if (file.exists(pathprobe)) {	
          files <<- list.files(path = pathprobe, all.files = F, full.names = T)	
          readData()	
          
          #adding info to list and resetting variables	
          myList <<- append(myList,tempList)	
          tempList <<- list()	
        }	
        else {	
          print(paste0("File ", pathprobe, " does not exist!"))	
        }	
      }	
    }	
  }	
}	
#loop to begin extracting data	
for (y in 2018:(2018+numOfYears)) {	
  extractData()	
}	
#combining all data into a matrix	
numDataPoints = length(myList)/5	
usefulDataMatrix = matrix(nrow = (numDataPoints + 1), ncol = 5)	
usefulDataMatrix[1,] = c("Date", "Time", "Data Availability", "Wind Speed","Wind Direction")	
for(z in 1:numDataPoints) {	
  usefulDataMatrix[(z+1),] = c(unlist(myList[5*(z-1) + 1]), unlist(myList[5*(z-1) + 2]), unlist(myList[5*(z-1) + 3]), unlist(myList[5*(z-1) + 4]), unlist(myList[5*z]))	
}	
#save .txt file to the right directory	
setwd("/home/admin/Jason/cec intern/results/[724]")
write.matrix(usefulDataMatrix, "allData_724.txt", sep = "\t")	
pdf("Rplots.pdf",width=11,height=8)	
#plot monthly wind charts	
plotMonthlyWindCharts = function() {	
  #get list of rows which correspond to the start of a new month	
  startOfMonths = which(!duplicated(unlist(lapply(lapply(usefulDataMatrix[2:nrow(usefulDataMatrix),1],as.Date), format, "%m/%Y"))))	
  startOfMonths <<- append(startOfMonths, (dim(usefulDataMatrix)[1]))	
  
  #plot one graph for each month	
  for (i in 1:(length(startOfMonths) - 1)) {	
    #obtain Month and Year of data being plotted	
    monthNum = unlist(lapply(lapply(usefulDataMatrix[2:nrow(usefulDataMatrix),1],as.Date), format, "%m"))[startOfMonths[i]]	
    yearNum = as.numeric(unlist(lapply(lapply(usefulDataMatrix[2:nrow(usefulDataMatrix),1],as.Date), format, "%Y"))[startOfMonths[i]])	
    print(paste0("Data for ", monthNum,"/",yearNum))	
    
    #initiate PDF device	
    pdf(file=paste0("Wind Chart ", yearNum, "-", monthNum, ".pdf"), width = 11, height = 7)	
    
    #get list of Date, ws, and wd into data frame	
    Date = unlist(usefulDataMatrix[(startOfMonths[i] + 1):startOfMonths[i+1], 1])	
    ws = unlist(lapply(usefulDataMatrix[(startOfMonths[i] + 1):startOfMonths[i+1], 4], as.numeric))	
    wd = unlist(lapply(usefulDataMatrix[(startOfMonths[i] + 1):startOfMonths[i+1], 5], as.numeric))	
    df = data.frame(Date, ws, wd)	
    #countWindSpeed = length(df)
    #Obtain average wind speed	
    avgWindSpeed = mean(unlist(lapply(usefulDataMatrix[(startOfMonths[i] + 1):startOfMonths[i+1], 4], as.numeric)))	
    maxWindSpeed = max(unlist(lapply(usefulDataMatrix[(startOfMonths[i] + 1):startOfMonths[i+1], 4], as.numeric)))	
    countWindSpeed = length(unlist(lapply(usefulDataMatrix[(startOfMonths[i] + 1):startOfMonths[i+1], 4], as.numeric)))
    #Obtain number of readings for each direction	
    for (i in (startOfMonths[i]+1):startOfMonths[i+1]) {	
      directions = c(length(df$wd[df$wd >= 0 & df$wd < 22.5 | df$wd >= 337.5 & df$wd <= 360]), length(df$wd[df$wd >= 22.5 & df$wd < 67.5]), length(df$wd[df$wd >= 67.5 & df$wd < 112.5]), length(df$wd[df$wd >= 112.5 & df$wd < 157.5]), length(df$wd[df$wd >= 157.5 & df$wd < 202.5]), length(df$wd[df$wd >= 202.5 & df$wd < 247.5]), length(df$wd[df$wd >= 247.5 & df$wd < 292.5]), length(df$wd[df$wd >= 292.5 & df$wd < 337.5]))	
    }	
    
    #obtain predominant direction	
    predominantDirection = directionNames[match(max(directions),directions)]	
    
    #plot this data frame	
    polarFreq(df, ws.int = .5, pollutant = "ws", statistic = "frequency", cols="increment",  border.col ="transparent", offset = 5, gridline = 5, min.b = 1, auto.text = T, trans = T , ws.upper=10, main = paste0("[SG-724] Woodlands, SG - Met Station - ", month.name[as.numeric(monthNum)], " ", yearNum), key.position = "right", key.header = "Frequency", sub= paste("Max wind speed = ", format(round(maxWindSpeed, digits = 1), nsmall = 1)," m/s 	    ", sub= paste("Average wind speed = ", format(round(avgWindSpeed, digits = 1), nsmall = 1)," m/s         ","Number of points: ", countWindSpeed,"     Predominant Wind Direction: ",predominantDirection, sep="")),key.footer = "")	
    
    #turn off current device	
    dev.off()	
  }	
  
}	
plotMonthlyWindCharts()	
#plot overall wind chart	
plotOverallWindChart = function() {	
  #initiate PDF device	
  pdf(file=paste0("Overall Wind Chart - Lifetime",".pdf"), width = 11, height = 7)	
  
  #get list of Date, ws, and wd into data frame	
  Date = unlist(usefulDataMatrix[2:nrow(usefulDataMatrix), 1])	
  ws = unlist(lapply(usefulDataMatrix[2:nrow(usefulDataMatrix), 4], as.numeric))	
  wd = unlist(lapply(usefulDataMatrix[2:nrow(usefulDataMatrix), 5], as.numeric))	
  df = data.frame(Date, ws, wd)	
  
  #Obtain average wind speed	
  avgWindSpeed = mean(unlist(lapply(usefulDataMatrix[2:nrow(usefulDataMatrix), 4], as.numeric)))	
  maxWindSpeed = max(unlist(lapply(usefulDataMatrix[2:nrow(usefulDataMatrix), 4], as.numeric)))	
  countWindSpeed = length(unlist(lapply(usefulDataMatrix[2:nrow(usefulDataMatrix), 4], as.numeric)))	
  #Obtain number of readings for each direction	
  for (i in 2:nrow(usefulDataMatrix)) {	
    directions = c(length(df$wd[df$wd >= 0 & df$wd < 22.5 | df$wd >= 337.5 & df$wd <= 360]), length(df$wd[df$wd >= 22.5 & df$wd < 67.5]), length(df$wd[df$wd >= 67.5 & df$wd < 112.5]), length(df$wd[df$wd >= 112.5 & df$wd < 157.5]), length(df$wd[df$wd >= 157.5 & df$wd < 202.5]), length(df$wd[df$wd >= 202.5 & df$wd < 247.5]), length(df$wd[df$wd >= 247.5 & df$wd < 292.5]), length(df$wd[df$wd >= 292.5 & df$wd < 337.5]))	
  }	
  
  #obtain predominant direction	
  predominantDirection = directionNames[match(max(directions),directions)]	
  
  #plot this data frame	
  polarFreq(df, ws.int = .5, pollutant = "ws", statistic = "frequency", cols="increment",  border.col ="transparent", offset = 5, gridline = 5, min.b = 1, auto.text = T, trans = T , ws.upper=10, main = paste0("[SG-724] Woodlands, SG - Met Station - "), key.position = "right", key.header = "Frequency", sub= paste("Max wind speed = ", format(round(maxWindSpeed, digits = 1), nsmall = 1)," m/s 	    ",sub= paste("Average wind speed = ", format(round(avgWindSpeed, digits = 1), nsmall = 1)," m/s         ","Number of Points: ", countWindSpeed,"    Predominant Wind Direction: ",predominantDirection, sep="")),key.footer = "")	
  
  #turn off current device	
  dev.off()	
}	
plotOverallWindChart()	
dev.off()