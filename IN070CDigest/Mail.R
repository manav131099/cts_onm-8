rm(list=ls())
require('compiler')

enableJIT(3)
errHandle = file('/home/admin/Logs/LogsIN070CMail.txt',open='w',encoding='UTF-8')
sink(errHandle,type='message',append = T)
sink(errHandle,type='output',append = T)

source('/home/admin/CODE/IN070CDigest/summaryFunctions.R')
source('/home/admin/CODE/common/math.R')
source('/home/admin/CODE/Send_mail/sendmail.R')
RESETHISTORICAL=0
daysAlive = 0
reorderStnPaths = c(6,4,5,7,8,9,10,11)
MYLD = c()
source('/home/admin/CODE/MasterMail/timestamp.R')
require('mailR')
source('/home/admin/CODE/IN070CDigest/aggregateInfo.R')

METERNICKNAMES = c("ESS_3", "ESS_10", "ESS_2","INV-1","INV-2","INV-3","INV-4","INV-5")

METERACNAMES = c("ESS_3", "ESS_2", "ESS_10","Inverter_1","Inverter_2","Inverter_3",
                 "Inverter_4","Inverter_5")
checkdir = function(x)
{
  if(!file.exists(x))
  {
    dir.create(x)
  }
}

DOB2 = "2019-05-03"
DOB2=as.Date(DOB2)
sender = c('operations@cleantechsolar.com')
uname = 'shravan.karthik@cleantechsolar.com'
recipients = getRecipients("IN-070C","m")
pwd = 'CTS&*(789'
referenceDays = NA
extractDaysOnly = function(days)
{
	if(length(days!=0))
	{
	seq = seq(from=1,to=length(days)*6,by=6)
	days = unlist(strsplit(days,"-"))
	days = paste(days[seq+3],days[seq+4],days[seq+5],sep="-")
	return(days)
	}
}
analyseDays = function(days,ref)
{
  if(length(days) == length(ref))
    return(days)
  daysret = unlist(rep(NA,length(ref)))
  if(length(days) == 0)
    return(daysret)
  days2 = extractDaysOnly(days)
  idxmtch = match(days2,ref)
  idxmtch2 = idxmtch[complete.cases(idxmtch)]
  if(length(idxmtch) != length(idxmtch2))
  {
    print(".................Missmatch..................")
    print(days2)
    print("#########")
    print(ref)
    print("............................................")
    return(days)
  }
  if(is.finite(idxmtch))
  {
    daysret[idxmtch] = as.character(days)
    if(!(is.na(daysret[length(daysret)])))
      return(daysret)
  }	
  return(days)
}
performBackWalkCheck = function(day)
{
  path ="/home/admin/Dropbox/FlexiMC_Data/Gen1_Data/[IN-070C]"
  retval = 0
  yr = substr(day,1,4)
  yrmon = substr(day,1,7)
  pathprobe = paste(path,yr,yrmon,sep="/")
  stns = dir(pathprobe)
  print(stns)
  idxday = c()
  for(t in 1 : length(stns))
  {
    pathdays = paste(pathprobe,stns[t],sep="/")
    print(pathdays)
    days = dir(pathdays)
    dayMtch = days[grepl(day,days)]
    if(length(dayMtch))
    {
      idxday[t] = match(dayMtch,days)
    }
    print(paste("Match for",day,"is",idxday[t]))
  }
  idxmtch = match(NA,idxday[t])
  if(length(unique(idxday))==1 || length(idxmtch) < 5)
  {
    print('All days present ')
    retval = 1
  }
  return(retval)
}
sendMail= function(pathall)
{
  path = pathall[1]
  dataread = read.table(path,header = T,sep="\t")
  dataread1 = read.table(path,header = T,sep="\t")
  currday = as.character(dataread[1,1])
  Cur=as.Date(currday)
  filenams = c()
  body = ""
  body = paste(body,"Site Name: Apollo Tyres Phase 2",sep="")
  body = paste(body,"\n\nLocation: Chennai, India ")
  body = paste(body,"\n\nO&M Code: IN-070")
  body = paste(body,"\n\nSystem Size [kWp]:",INSTCAP)
  body = paste(body, "\n\nSystem Size Facing East: 4178.1 kWp (45.1% of total)")
  body = paste(body, "\n\nSystem Size Facing West: 4707.0 kWp (50.8% of total)")
  body = paste(body, "\n\nSystem Size Facing South: 99 kWp (1.1% of total)")
  body = paste(body, "\n\nSystem Size Facing North: 283.8 kWp (3.1% of total)")
  body = paste(body,"\n\nNumber of Energy Meters: 3")
  body = paste(body,"\n\nModule Brand / Model / Nos: JA Solar JAP72S01-(325-330W), Trina TSM-PE14A (325-330W)")
  body = paste(body,"\n\nInverter Brand / Model / Nos: ABB PV800-57B-1732 kW- 5 Nos")
  body = paste(body,"\n\nSite COD: 2019-05-03")
  body = paste(body,"\n\nSystem age [days]:",as.character((as.numeric(Cur-DOB2))))
  body = paste(body,"\n\nSystem age [years]:",as.character(round((as.numeric(Cur-DOB2))/365,1)))
  bodyac = body
  body = ""
  TOTALGENCALC = 0

    yr = substr(Cur,1,4)
    yrmon = substr(Cur,1,7)
    
    path_inv = "/home/admin/Dropbox/FlexiMC_Data/Gen1_Data/[IN-070C]"
    filename_mfm1 = paste("[IN-070C]-ESS_2-",Cur,".txt",sep = "")
    filename_mfm2 = paste("[IN-070C]-ESS_3-",Cur,".txt",sep = "")
    filename_mfm3 = paste("[IN-070C]-ESS_10-",Cur,".txt",sep = "")
    pathread_mfm1 = paste(path_inv, yr, yrmon, "ESS_2", filename_mfm1, sep = "/")
    dataread_mfm1 = read.table(pathread_mfm1, sep = "\t", header = T)
  pathread_mfm2 = paste(path_inv, yr, yrmon, "ESS_3", filename_mfm2, sep = "/")
  dataread_mfm2 = read.table(pathread_mfm2, sep = "\t", header = T)
  pathread_mfm3 = paste(path_inv, yr, yrmon, "ESS_10", filename_mfm3, sep = "/")
  dataread_mfm3 = read.table(pathread_mfm3, sep = "\t", header = T)
  
  imp_en1 = dataread_mfm1[65]
  imp_en1 = imp_en1[complete.cases(imp_en1), ]
  imp_en_value1 = imp_en1[1]
  imp_en_last1 = tail(imp_en1, n=1)
  imp_en_diff1 = imp_en_last1 - imp_en_value1
  
  imp_en2 = dataread_mfm2[65]
  imp_en2 = imp_en2[complete.cases(imp_en2), ]
  imp_en_value2 = imp_en2[1]
  imp_en_last2 = tail(imp_en2, n=1)
  imp_en_diff2 = imp_en_last2 - imp_en_value2
  
  imp_en3 = dataread_mfm3[65]
  imp_en3 = imp_en3[complete.cases(imp_en3), ]
  imp_en_value3 = imp_en3[1]
  imp_en_last3 = tail(imp_en3, n=1)
  imp_en_diff3 = imp_en_last3 - imp_en_value3
  
    filename = paste("[IN-015S] ",Cur,".txt",sep="")
    path = "/home/admin/Dropbox/Second Gen/[IN-015S]"
    path2 = "/home/admin/Dropbox/FlexiMC_Data/Gen1_Data/[IN-070C]"
    pathRead = paste(path,yr,yrmon,filename,sep="/")
filename3 = paste("[IN-070C]-I1-",Cur,".txt",sep = "")
filename4 = paste("[IN-070C]-I2-",Cur,".txt",sep = "")
filename5 = paste("[IN-070C]-I3-",Cur,".txt",sep = "")
filename6 = paste("[IN-070C]-I4-",Cur,".txt",sep = "")
filename7 = paste("[IN-070C]-I5-",Cur,".txt",sep = "")
pathread_inv1 = paste(path_inv, yr, yrmon, "Inverter_1", filename3, sep = "/")
dataread_inv1 = read.table(pathread_inv1, sep = "\t", header = T)
pathread_inv2 = paste(path_inv, yr, yrmon, "Inverter_2", filename4, sep = "/")
dataread_inv2 = read.table(pathread_inv2, sep = "\t", header = T)
pathread_inv3 = paste(path_inv, yr, yrmon, "Inverter_3", filename5, sep = "/")
dataread_inv3 = read.table(pathread_inv3, sep = "\t", header = T)
pathread_inv4 = paste(path_inv, yr, yrmon, "Inverter_4", filename6, sep = "/")
dataread_inv4 = read.table(pathread_inv4, sep = "\t", header = T)
pathread_inv5 = paste(path_inv, yr, yrmon, "Inverter_5", filename7, sep = "/")
dataread_inv5 = read.table(pathread_inv5, sep = "\t", header = T)
if(file.exists(pathread_inv1))
{
print("Printing dataread")
yield1 = dataread_inv1[33]
yield1 = yield1[complete.cases(yield1), ]
value1 = yield1[1]
last1 = tail(yield1, n=1)
diff1 = last1 - value1

yield1 = round(diff1/2002.7,2) #divide by installed capacity

MYLD = c(MYLD, yield1)
}
if(file.exists(pathread_inv2))
{
print("Printing dataread")
yield2 = dataread_inv2[33]
yield2 = yield2[complete.cases(yield2), ]
value2 = yield2[1]
last2 = tail(yield2, n=1)
diff2 = last2 - value2
yield2 = round(diff2/1904.5,2) #divide by installed capacity
print("Printing Yield-2")
print(yield2)
MYLD = c(MYLD, yield2)
}
if(file.exists(pathread_inv3))
{
print("Printing dataread")
yield3 = dataread_inv3[33]
yield3 = yield3[complete.cases(yield3), ]
value3 = yield3[1]
last3 = tail(yield3, n=1)
diff3 = last3 - value3
yield3 = round(diff3/1793,2) #divide by installed capacity
print("Printing Yield-3")
print(yield3)
MYLD = c(MYLD, yield3)
}
if(file.exists(pathread_inv4))
{
print("Printing dataread")
yield4 = dataread_inv4[33]
yield4 = yield4[complete.cases(yield4), ]
value4 = yield4[1]
last4 = tail(yield4, n=1)
diff4 = last4 - value4
yield4 = round(diff4/1610.4,2) #divide by installed capacity
print("Printing Yield-4")
print(yield4)
MYLD = c(MYLD, yield4)
}
if(file.exists(pathread_inv5))
{
print("Printing dataread")
yield5 = dataread_inv5[33]
yield5 = yield5[complete.cases(yield5), ]
value5 = yield5[1]
last5 = tail(yield5, n=1)
diff5 = last5 - value5
yield5 = round(diff5/1956.9,2) #divide by installed capacity
print("Printing Yield-4")
print(yield5)
MYLD = c(MYLD, yield5)
}
    GTIGreater20=NA
    GTI = DNI = NA
    if(file.exists(pathRead))
    {
      dataread = read.table(pathRead,sep="\t",header = T)

      if(nrow(dataread) > 0)
      {
        GTI1 = as.numeric(dataread[1,3])
        GTI2 = as.numeric(dataread[1,4])
        GTI3 = as.numeric(dataread[1,5])
        GTI4 = as.numeric(dataread[1,44])
        GTI5 = as.numeric(dataread[1,45])
        body = paste(body,"\n\n_____________________________________________\n",sep="")
        body = paste(body,"WMS")
        body  = paste(body,"\n_____________________________________________\n",sep="")
        body = paste(body,"\nDaily Irradiation Silicon Sensor [kWh/m2]: ",as.character(dataread[1,3]),sep="")
        body = paste(body,"\n\nDaily Irradiation Silicon Sensor, North [kWh/m2]: ",as.character(dataread[1,4]),sep="")
        body = paste(body,"\n\nDaily Irradiation Silicon Sensor, South [kWh/m2]: ",as.character(dataread[1,5]),sep="")
        body = paste(body,"\n\nDaily Irradiation Silicon Sensor, East [kWh/m2]: ",as.character(dataread[1,44]),sep="")
        body = paste(body,"\n\nDaily Irradiation Silicon Sensor, West [kWh/m2]: ",as.character(dataread[1,45]),sep="")
        body = paste(body,"\n\nWeighted Gmod for Site [kWh/m2]: ",round((dataread[1,44]*0.451)+(dataread[1,45]*0.508)+(dataread[1,4]*0.011)+(dataread[1,5]*0.031) ,2),sep="")
  
  
      }
    }
  

  acname = METERNICKNAMES
  noInverters = NOINVS
  globMtYld=c()
  idxglobMtYld=1
  InvReadings = unlist(rep(NA,noInverters))
  InvAvail = unlist(rep(NA,noInverters))

  for(t in 1 : length(pathall))
  {
    type = 0 
    meteridx = t
    if(grepl("WMS",pathall[t])) 
      type = 1
    else if(grepl("ESS",pathall[t]))
      type=1
    
    
    if( t > (1+NOMETERS))
    {
      splitpath = unlist(strsplit(pathall[t],"Inverter_"))
      if(length(splitpath)>1)
      {
        meteridx = unlist(strsplit(splitpath[2],"/"))
        meteridx = as.numeric(meteridx[1])
      }
    }
    path = pathall[t]
    dataread = read.table(path,header = T,sep="\t")
    currday = as.character(dataread[1,1])
    
    if(type==0)
    {
      filenams[t] = paste(currday,"-",METERNICKNAMES[meteridx+3],".txt",sep="")
    }
    else if(type==1 && grepl("WMS",pathall[t]))
    {
      filenams[t] = paste(currday,"-",'WMS',".txt",sep="")
    }
    else if(type==1 && grepl("ESS_2",pathall[t]))
    {
      filenams[t] = paste(currday,"-","ESS_2",".txt",sep="")
    }
    else if(type==1 && grepl("ESS_3",pathall[t]))
    {
      filenams[t] = paste(currday,"-","ESS_3",".txt",sep="") 
    }
    else if(type==1 && grepl("ESS_10",pathall[t]))
    {
      filenams[t] = paste(currday,"-","ESS_10",".txt",sep="") 
    }
    print("---------------------")
    print(path)
    print(filenams[t])
    print("---------------------")
    if(type == 1)
    {
      body = paste(body,"\n________________________________________________\n\n")
      body = paste(body,currday,acname[t])
      body = paste(body,"\n________________________________________________\n\n")
      body = paste(body,"DA [%]:",as.character(dataread[1,2]),"\n\n")
      {
        if(grepl("ESS",pathall[t]))
        {
          if(grepl("ESS_2",pathall[t])){
            body = paste(body,"System Size [kWp]:",INSTCAPM[1],"\n\n")
            #body = paste(body,"Import Energy Reading [kWh]:",round(imp_en_last1,2),"\n\n")
          }else if(grepl("ESS_3",pathall[t])){
            body = paste(body,"System Size [kWp]:",INSTCAPM[2],"\n\n")
            #body = paste(body,"Import Energy Reading [kWh]:",round(imp_en_last2,2),"\n\n")
          }else{
            body = paste(body,"System Size [kWp]:",INSTCAPM[3],"\n\n")
            #body = paste(body,"Import Energy Reading [kWh]:",round(imp_en_last3,2),"\n\n")
          }
          
          
          body = paste(body,"EAC method-1 (Pac) [kWh]:",as.character(format(round(dataread[1,3],1), nsmall = 1)),"\n\n")
          body = paste(body,"EAC method-2 (Eac) [kWh]:",as.character(format(round(dataread[1,4],1), nsmall = 1)),"\n\n")
          TOTALGENCALC = TOTALGENCALC + as.numeric(dataread[1,4])
          body = paste(body,"Yield-1 [kWh/kWp]:",as.character(format(round(dataread[1,5],2), nsmall = 2)),"\n\n")
          body = paste(body,"Yield-2 [kWh/kWp]:",as.character(format(round(dataread[1,6],2), nsmall = 2)),"\n\n")
          globMtYld[idxglobMtYld] = as.numeric(dataread[1,6])
          idxglobMtYld = idxglobMtYld + 1
          #body = paste(body,"GHI [kWh/m^2]:",as.character(format(round(dataread[1,3],2), nsmall = 2,)),"\n\n")
          if(file.exists(pathRead))
          {
          body = paste(body,"PR-1 [%]:",as.character(format(round(dataread[1,5]*100/GTI5,1), nsmall = 1)),"\n\n")
          body = paste(body,"PR-2 [%]:",as.character(format(round(dataread[1,6]*100/GTI5,1), nsmall = 1)),"\n\n")
          }
          body = paste(body,"Last recorded value [kWh]:",as.character(format(round(dataread[1,9],1), nsmall = 1)),"\n\n")
           if(grepl("ESS_2",pathall[t])){
            body = paste(body,"Import Energy Reading [kWh]:",round(imp_en_last1,2),"\n\n")
          }else if(grepl("ESS_3",pathall[t])){
            body = paste(body,"Import Energy Reading [kWh]:",round(imp_en_last2,2),"\n\n")
          }else{
            body = paste(body,"Import Energy Reading [kWh]:",round(imp_en_last3,2),"\n\n")
          }
          body = paste(body,"Last recorded time:",as.character(dataread[1,10]),"\n")
          
        }
        else
        {
          body = paste(body,"GHI [kWh/m^2]:",as.character(format(round(dataread[1,3],2), nsmall = 2,)),"\n\n")
          body = paste(body,"Tmod [C]:",as.character(format(round(dataread[1,5],1), nsmall = 1)),"\n\n")
          body = paste(body,"Tmod solar hours[C]:",as.character(format(round(dataread[1,7],1), nsmall = 1)))
          GTI = as.numeric(dataread[1,3])
        }
      }
      next
    }
    InvReadings[meteridx] = as.numeric(dataread1[1,5])
    InvAvail[meteridx]= as.numeric(dataread1[1,11])
    
    print(as.numeric(dataread1[1,11]))
    MYLD[(meteridx)] = as.numeric(dataread1[1,7])
    MYLD[(meteridx)] = as.numeric(dataread1[1,5])
  }
  body = paste(body,"\n\n________________________________________________\n\n")
  body = paste(body,currday,"Inverters")
  body = paste(body,"\n________________________________________________")
  MYLDCPY = MYLD
  print("Printing MYLD")
  print(MYLD)
  if(length(MYLD))
  {
    MYLD = MYLD[complete.cases(MYLD)]
  }
  addwarn = 0
  sddev = covar = NA
  
  if(length(MYLD))
  {
    addwarn = 1
    sddev = round(sdp(MYLD),1)
    meanyld = mean(MYLD)
    covar = round((sdp(MYLD)*100/meanyld),1)
  }
  MYLD = MYLDCPY
  for(t in 1 : noInverters)
  {
    	if (t == 1)
{
body = paste(body,"\n\n Yield INV-1: ",yield1,sep="")
}else if (t==2){
body = paste(body,"\n\n Yield INV-2: ",yield2,sep="")
}else if(t==3){
body = paste(body,"\n\n Yield INV-3: ",yield3,sep="")
}else if(t==4){
body = paste(body,"\n\n Yield INV-4: ",yield4,sep="")
}else if(t==5){
body = paste(body,"\n\n Yield INV-5: ",yield5,sep="")
    }
  }
  body = paste(body,"\n\nStdev/COV Yields: ",sddev," / ",covar,"%",sep="")
  # for(t in 1 : noInverters)
  #  {
  #   body = paste(body,"\n\n Inverter Availability ",acname[(t+1+NOMETERS)]," [%]: ",as.character(InvAvail[t]),sep="")
  #  }
  bodyac = paste(bodyac,"\n\nSystem Full Generation [kWh]:",format(round(TOTALGENCALC,1), nsmall = 1))
  bodyac = paste(bodyac,"\n\nSystem Full Yield [kWh/kWp]:",format(round(TOTALGENCALC/sum(INSTCAPM),1), nsmall = 1),"\n\n")

  if(file.exists(pathRead))
  {
  bodyac = paste(bodyac,"System Full PR [%]:",format(round(TOTALGENCALC*100/sum(INSTCAPM)/GTI4,1), nsmall = 1),"\n\n")
  }
  for(t in 1 : NOMETERS)
  {
    bodyac = paste(bodyac,"Yield",acname[(t)],"[kWh/kWp]:",as.character(format(round(globMtYld[t],2), nsmall = 2)),"\n\n")
  }
  sdm =round(sdp(globMtYld),3)
  covarm = round(sdm*100/mean(globMtYld),1)
  bodyac = paste(bodyac,"Stdev/COV Yields:",as.character(format(round(sdm,1), nsmall = 1)),"/",as.character(format(round(covarm,1), nsmall = 1)),"[%]")
  body = gsub("\n ","\n",body)
  body = paste(bodyac,body,sep="")
  firecond = lastMailDate(paste('/home/admin/Start/MasterMail/IN-070C_Mail.txt',sep=""))
  if (currday == firecond)
    return()
  send.mail(from = sender,
            to = recipients,
            subject = paste("Station [IN-070C] Digest",currday),
            body = body,
            smtp = list(host.name = "smtp.office365.com", port = 587, user.name = uname, passwd = pwd, tls = TRUE),
            authenticate = TRUE,
            send = TRUE,
            attach.files = pathall,
            file.names = filenams, # optional paramete
            debug = F)
  recordTimeMaster("IN-070C","Mail",currday)
}

path = "/home/admin/Dropbox/FlexiMC_Data/Gen1_Data/[IN-070C]"
path2G = '/home/admin/Dropbox/FlexiMC_Data/Second_Gen/[IN-070C]'
path3G = '/home/admin/Dropbox/FlexiMC_Data/Third_Gen/[IN-070C]'
path4G = '/home/admin/Dropbox/FlexiMC_Data/Fourth_Gen/[IN-070C]'

if(RESETHISTORICAL)
{
  command = paste("rm -rf",path2G)
  system(command)
  command = paste("rm -rf",path3G)
  system(command)
  command = paste("rm -rf",path4G)
  system(command)
}

checkdir(path2G)
checkdir(path3G)

years = dir(path)
stnnickName2 = "IN-070C"
lastdatemail = lastMailDate(paste('/home/admin/Start/MasterMail/',stnnickName2,'_Mail.txt',sep=""))
stopDate = paste("[",stnnickName2,"]-I5-",lastdatemail,".txt",sep="")
print(paste('StopDate is',stopDate))
ENDCALL=0
for(x in 1 : length(years))
{
  path2Gyr = paste(path2G,years[x],sep = "/")
  pathyr = paste(path,years[x],sep="/")
  checkdir(path2Gyr)
  months = dir(pathyr)
  for(y in 1 : length(months))
  {
    path2Gmon = paste(path2Gyr,months[y],sep = "/")
    pathmon = paste(pathyr,months[y],sep="/")
    checkdir(path2Gmon)
    stns = dir(pathmon)
    stns = stns[reorderStnPaths]
    a=c("ESS_2","ESS_3", "ESS_10")
    dunmun = 0
    for(t in 1 : length(stns))
    {
      type = 1
      if(stns[t] %in% a)
        type = 0
      if(stns[t] == "WMS")
        type = 2
      pathmon1 = paste(pathmon,stns[t],sep="/")
      days = dir(pathmon1)
      path2Gmon1 = paste(path2Gmon,stns[t],sep = "/")
      if(!file.exists(path2Gmon1))
      {	
        dir.create(path2Gmon1)
      }
      if(length(days)>0)
      {
        for(z in 1 : length(days))
        {
          if(ENDCALL == 1)
            break
          if((z==length(days)) && (y == length(months)) && (x ==length(years)))
            next
          print(days[z])
          pathfinal = paste(pathmon1,days[z],sep = "/")
          path2Gfinal = paste(path2Gmon1,days[z],sep="/")
          if(RESETHISTORICAL)
          {
            secondGenData(pathfinal,path2Gfinal,type)
          }
          if(!dunmun){
            daysAlive = daysAlive+1
          }
          if(days[z] == stopDate)
          {
            ENDCALL = 1
            print('Hit end call')
            next
          }
        }
      }
      dunmun = 1
      if(ENDCALL == 1)
        break
    }
    if(ENDCALL == 1)
      break
  }
  if(ENDCALL == 1)
    break
}

print('Backlog done')

prevx = x
prevy = y
prevz = z
repeats = 0
SENDMAILTRIGGER = 0
newlen=1
while(1)
{
  recipients = getRecipients("IN-070C","m")
  recordTimeMaster("IN-070C","Bot")
  years = dir(path)
  noyrs = length(years)
  path2Gfinalall = vector('list')
  for(x in prevx : noyrs)
  {
    pathyr = paste(path,years[x],sep="/")
    path2Gyr = paste(path2G,years[x],sep="/")
    checkdir(path2Gyr)
    mons = dir(pathyr)
    nomons = length(mons)
    startmn = prevy
    endmn = nomons
    if(startmn>endmn)
    {
      startmn = 1
      prevx = x-1
      prevz = 1
    }
    for(y in startmn:endmn)
    {
      pathmon = paste(pathyr,mons[y],sep="/")
      path2Gmon = paste(path2Gyr,mons[y],sep="/")
      checkdir(path2Gmon)
      stns = dir(pathmon)
      if(length(stns) < 7)
      {
        print('Station sync issue.. sleeping for an hour')
        Sys.sleep(3600) # Sync issue, meter data comes after MFM and WMS
        stns = dir(pathmon)
      }
      stns = stns[reorderStnPaths]
      a=c("ESS_2","ESS_3","ESS_10")
      for(t in 1 : length(stns))
      {
        newlen = (y-startmn+1) + (x-prevx)
        print(paste('Reset newlen to',newlen))
        type = 1
        if(grepl("ESS",stns[t]))
          type = 0
        else if(grepl("WMS",stns[t]))
          type = 2
        pathmon1 = paste(pathmon,stns[t],sep="/")
        days = dir(pathmon1)
        path2Gmon1 = paste(path2Gmon,stns[t],sep = "/")
        chkcopydays = days[grepl('Copy',days)]
        if(!file.exists(path2Gmon1))
        {
          dir.create(path2Gmon1)
        }
        if(length(chkcopydays) > 0)
        {
          print('Copy file found they are')
          print(chkcopydays)
          idxflse = match(chkcopydays,days)
          print(paste('idx matches are'),idxflse)
          for(innerin in 1 : length(idxflse))
          {
            command = paste("rm '",pathmon,"/",days[idxflse[innerin]],"'",sep="")
            print(paste('Calling command',command))
            system(command)
          }
          days = days[-idxflse]
        }
        days = days[complete.cases(days)]
        if(length(days)>0)
        {
        if(t==1){
          referenceDays <<- extractDaysOnly(days)
        }else{
          days = analyseDays(days,referenceDays)}
        }
        nodays = length(days)
        
        if(y > startmn)
        {
          z = prevz = 1
        }
        if(nodays > 0)
        {
          startz = prevz
          if(startz > nodays)
            startz = nodays
          for(z in startz : nodays)
          {
            if(t == 1)
            {
              path2Gfinalall[[newlen]] = vector('list')
            }
            if(is.na(days[z]))
              next
            pathdays = paste(pathmon1,days[z],sep = "/")
            path2Gfinal = paste(path2Gmon1,days[z],sep="/")
            secondGenData(pathdays,path2Gfinal,type)
            if((z == nodays) && (y == endmn) && (x == noyrs))
            {
              if(!repeats)
              {
                print('No new data')
                repeats = 1
              }
              next
            }
            if(is.na(days[z]))
            {
              print('Day was NA, do next in loop')
              next
            }
            SENDMAILTRIGGER <<- 1
            if(t == 1)
            {
              splitstr =unlist(strsplit(days[z],"-"))
              daysSend = paste(splitstr[4],splitstr[5],splitstr[6],sep="-")
              print(paste("Sending",daysSend))
              if(performBackWalkCheck(daysSend) == 0)
              {
                print("Sync issue, sleeping for an 1 hour")
                Sys.sleep(3600) # Sync issue -- meter data comes a little later than
                # MFM and WMS
              }
            }
            repeats = 0
            print(paste('New data, calculating digests',days[z]))
            path2Gfinalall[[newlen]][t] = paste(path2Gmon1,days[z],sep="/")
            newlen = newlen + 1
            print(paste('Incremented newlen by 1 to',newlen))
          }
        }
      }
    }
  }
  if(SENDMAILTRIGGER)
  {
    print('Sending mail')
    print(paste('newlen is ',newlen))
    for(lenPaths in 1 : (newlen - 1))
    {
      if(lenPaths < 1)
        next
      print(unlist(path2Gfinalall[[lenPaths]]))
      pathsend = unlist(path2Gfinalall[[lenPaths]])
      daysAlive = daysAlive+1
      sendMail(pathsend)
    }
    SENDMAILTRIGGER <<- 0
    newlen = 1
    print(paste('Reset newlen to',newlen))
    
  }
  
  prevx = x
  prevy = y
  prevz = z
  Sys.sleep(3600)
}
print(paste('Exited for some reason x y z values are'),x,y,z)
sink()
