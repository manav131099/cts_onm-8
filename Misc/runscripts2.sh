#!/bin/bash
RUNALL=1
x=""

if [ "$1" == "IN-018L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1Live.py "3818604" "[IN-018L]" "Asia/Kolkata" "Asia/Calcutta" > /home/admin/Logs/LogsIN018LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-018L_History"
	sleep 10
fi

if [ "$1" == "IN-021L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV2.py "3835525" "[IN-021L]" "Asia/Kolkata" "Asia/Calcutta" "2019-11-01"> /home/admin/Logs/LogsIN021LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-021L_History"
	sleep 10
fi

if [ "$1" == "IN-031L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1Live.py "3802193" "[IN-031L]" "Asia/Kolkata" "Asia/Calcutta" > /home/admin/Logs/LogsIN031LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-031L_History"
	sleep 10
fi

if [ "$1" == "IN-038L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1Live.py "3765443" "[IN-038L]" "Asia/Kolkata" "Asia/Calcutta" > /home/admin/Logs/LogsIN038LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-038L_History"
	sleep 10
fi

if [ "$1" == "IN-041L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV2.py "3835524" "[IN-041L]" "Asia/Kolkata" "Asia/Calcutta" "2019-11-01"> /home/admin/Logs/LogsIN041LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-041L_History"
	sleep 10
fi

if [ "$1" == "IN-042L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1Live.py "3777348" "[IN-042L]" "Asia/Kolkata" "Asia/Calcutta" > /home/admin/Logs/LogsIN042LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-042L_History"
	sleep 10
fi

if [ "$1" == "IN-043L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1Live.py "3777349" "[IN-043L]" "Asia/Kolkata" "Asia/Calcutta" > /home/admin/Logs/LogsIN043LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-043L_History"
	sleep 10
fi

if [ "$1" == "IN-045L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1Live.py "3794812" "[IN-045L]" "Asia/Kolkata" "Asia/Calcutta" > /home/admin/Logs/LogsIN045LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-045L_History"
	sleep 10
fi

if [ "$1" == "IN-047L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1Live.py "3794653" "[IN-047L]" "Asia/Kolkata" "Asia/Calcutta" > /home/admin/Logs/LogsIN047LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-047L_History"
	sleep 10
fi

if [ "$1" == "IN-048L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/IN048LDigest/Live.py  > /home/admin/Logs/LogsIN048LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-048L_History"
	sleep 10
fi

if [ "$1" == "IN-049L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1Live.py "3794253" "[IN-049L]" "Asia/Kolkata" "Asia/Calcutta" > /home/admin/Logs/LogsIN049LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-049L_History"
	sleep 10
fi

if [ "$1" == "IN-051L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV2.py "3787245" "[IN-051L]" "Asia/Kolkata" "Asia/Calcutta" "2019-07-01"> /home/admin/Logs/LogsIN051LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-051L_History"
	sleep 10
fi

if [ "$1" == "IN-052L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/IN052LDigest/Live.py > /home/admin/Logs/LogsIN052LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-052L_History"
	sleep 10
fi

if [ "$1" == "IN-053L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/IN053LDigest/Live.py > /home/admin/Logs/LogsIN053LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-053L_History"
	sleep 10
fi

if [ "$1" == "IN-055L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1Live.py "3795317" "[IN-055L]" "Asia/Kolkata" "Asia/Calcutta" > /home/admin/Logs/LogsIN055LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-055L_History"
	sleep 10
fi

if [ "$1" == "IN-056L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/IN056LDigest/Live.py > /home/admin/Logs/LogsIN056LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-056L_History"
	sleep 10
fi

if [ "$1" == "IN-058L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/IN058LDigest/Live.py > /home/admin/Logs/LogsIN058LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-058L_History"
	sleep 10
fi

if [ "$1" == "IN-059L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1Live.py "3800132" "[IN-059L]" "Asia/Kolkata" "Asia/Calcutta" > /home/admin/Logs/LogsIN059LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-059L_History"
	sleep 10
fi

if [ "$1" == "IN-060L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/IN060LDigest/Live.py > /home/admin/Logs/LogsIN060LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-060L_History"
	sleep 10
fi

if [ "$1" == "IN-061L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1Live.py "3800671" "[IN-061L]" "Asia/Kolkata" "Asia/Calcutta" > /home/admin/Logs/LogsIN061LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-061L_History"
	sleep 10
fi

if [ "$1" == "IN-062L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/IN062LDigest/Live.py > /home/admin/Logs/LogsIN062LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-062L_History"
	sleep 10
fi

if [ "$1" == "IN-063L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1Live.py "3802488" "[IN-063L]" "Asia/Kolkata" "Asia/Calcutta" > /home/admin/Logs/LogsIN063LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-063L_History"
	sleep 10
fi

if [ "$1" == "IN-064L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/IN064LDigest/Live.py > /home/admin/Logs/LogsIN064LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-064L_History"
	sleep 10
fi

if [ "$1" == "IN-065L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1Live.py "3814767" "[IN-065L]" "Asia/Kolkata" "Asia/Calcutta" > /home/admin/Logs/LogsIN065LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-065L_History"
	sleep 10
fi

if [ "$1" == "IN-066L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/IN066LDigest/Live.py > /home/admin/Logs/LogsIN066LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-066L_History"
	sleep 10
fi

if [ "$1" == "IN-067L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1Live.py "3804923" "[IN-067L]" "Asia/Kolkata" "Asia/Calcutta" > /home/admin/Logs/LogsIN067LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-067L_History"
	sleep 10
fi

if [ "$1" == "IN-068L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/IN068LDigest/Live.py > /home/admin/Logs/LogsIN068LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-068L_History"
	sleep 10
fi

if [ "$1" == "IN-069L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/IN069LDigest/Live.py > /home/admin/Logs/LogsIN069LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-069L_History"
	sleep 10
fi

if [ "$1" == "IN-071L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1Live.py "3804130" "[IN-071L]" "Asia/Kolkata" "Asia/Calcutta" > /home/admin/Logs/LogsIN071LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-071L_History"
	sleep 10
fi

if [ "$1" == "IN-072L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1Live.py "3805118" "[IN-072L]" "Asia/Kolkata" "Asia/Calcutta" > /home/admin/Logs/LogsIN072LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-072L_History"
	sleep 10
fi

if [ "$1" == "IN-073L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV3.py "3805120" "[IN-073L]" "Asia/Kolkata" "Asia/Calcutta" "2019-09-01" > /home/admin/Logs/LogsIN073LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-073L_History"
	sleep 10
fi

if [ "$1" == "IN-074L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1Live.py "3820177" "[IN-074L]" "Asia/Kolkata" "Asia/Calcutta" > /home/admin/Logs/LogsIN074LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-074L_History"
	sleep 10
fi

if [ "$1" == "IN-075L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/IN075LDigest/Live.py > /home/admin/Logs/LogsIN075LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-075L_History"
	sleep 10
fi

if [ "$1" == "IN-076L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1Live.py "3822707" "[IN-076L]" "Asia/Kolkata" "Asia/Calcutta" > /home/admin/Logs/LogsIN076LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-076L_History"
	sleep 10
fi

if [ "$1" == "IN-077L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1Live.py "3823815" "[IN-077L]" "Asia/Kolkata" "Asia/Calcutta" > /home/admin/Logs/LogsIN077LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-077L_History"
	sleep 10
fi

if [ "$1" == "IN-078L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1Live.py "3821964" "[IN-078L]" "Asia/Kolkata" "Asia/Calcutta" > /home/admin/Logs/LogsIN078LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-078L_History"
	sleep 10
fi

if [ "$1" == "IN-079L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV2.py "3829586" "[IN-079L]" "Asia/Kolkata" "Asia/Calcutta" "2019-09-01"> /home/admin/Logs/LogsIN079LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-079L_History"
	sleep 10
fi

if [ "$1" == "IN-080L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/IN080LDigest/Live.py  > /home/admin/Logs/LogsIN080LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-080L_History"
	sleep 10
fi

if [ "$1" == "IN-081L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV3.py "3836445" "[IN-081L]" "Asia/Kolkata" "Asia/Calcutta" "2019-12-01"> /home/admin/Logs/LogsIN081LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-081L_History"
	sleep 10
fi

if [ "$1" == "IN-082L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV3.py "3835833" "[IN-082L]" "Asia/Kolkata" "Asia/Calcutta" "2019-12-01"> /home/admin/Logs/LogsIN082LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-082L_History"
	sleep 10
fi

if [ "$1" == "IN-083L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV3.py "3839350" "[IN-083L]" "Asia/Kolkata" "Asia/Calcutta" "2019-12-01"> /home/admin/Logs/LogsIN083LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-083L_History"
	sleep 10
fi

if [ "$1" == "MY-005L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1Live.py "3819590" "[MY-005L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" > /home/admin/Logs/LogsMY005LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-005L_History"
	sleep 10
fi

if [ "$1" == "MY-007L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1Live.py "3827749" "[MY-007L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" > /home/admin/Logs/LogsMY007LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-007L_History"
	sleep 10
fi

if [ "$1" == "MY-008L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1Live.py "3824826" "[MY-008L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" > /home/admin/Logs/LogsMY008LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-008L_History"
	sleep 10
fi

if [ "$1" == "MY-009L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV4.py "3836443" "[MY-009L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2019-11-20"> /home/admin/Logs/LogsMY009LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-009L_History"
	sleep 10
fi

if [ "$1" == "MY-400L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV4.py "3845432" "[MY-400L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-01-01"> /home/admin/Logs/LogsMY400LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-400L_History"
	sleep 10
fi

if [ "$1" == "PH-001L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV2.py "3827787" "[PH-001L]" "Asia/Manila" "Asia/Manila" "2019-07-01"> /home/admin/Logs/LogsPH001LHistory.txt &
	y=`echo $!`
	x="$x\n$y --PH-001L_History"
	sleep 10
fi

if [ "$1" == "PH-002L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV2.py "3830382" "[PH-002L]" "Asia/Manila" "Asia/Manila" "2019-07-01"> /home/admin/Logs/LogsPH002LHistory.txt &
	y=`echo $!`
	x="$x\n$y --PH-002L_History"
	sleep 10
fi

if [ "$1" == "PH-003L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV2.py "3828091" "[PH-003L]" "Asia/Manila" "Asia/Manila" "2019-07-01"> /home/admin/Logs/LogsPH003LHistory.txt &
	y=`echo $!`
	x="$x\n$y --PH-003L_History"
	sleep 10
fi

if [ "$1" == "PH-004L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/PH004LDigest/Live.py > /home/admin/Logs/LogsPH004LHistory.txt &
	y=`echo $!`
	x="$x\n$y --PH-004L_History"
	sleep 10
fi

if [ "$1" == "PH-006L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV2.py "3828003" "[PH-006L]" "Asia/Manila" "Asia/Manila" "2019-07-01"> /home/admin/Logs/LogsPH006LHistory.txt &
	y=`echo $!`
	x="$x\n$y --PH-006L_History"
	sleep 10
fi

if [ "$1" == "PH-007L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV2.py "3826338" "[PH-007L]" "Asia/Manila" "Asia/Manila" "2019-07-01"> /home/admin/Logs/LogsPH007LHistory.txt &
	y=`echo $!`
	x="$x\n$y --PH-007L_History"
	sleep 10
fi

if [ "$1" == "SG-007L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1Live.py "3795604" "[SG-007L]" "Asia/Singapore" "Asia/Singapore" > /home/admin/Logs/LogsSG007LHistory.txt &
	y=`echo $!`
	x="$x\n$y --SG-007L_History"
	sleep 10
fi

if [ "$1" == "SG-008L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV3.py  "3836631" "[SG-008L]" "Asia/Singapore" "Asia/Singapore" "2019-12-01" > /home/admin/Logs/LogsSG008LHistory.txt &
	y=`echo $!`
	x="$x\n$y --SG-008L_History"
	sleep 10
fi

if [ "$1" == "TH-004L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV2.py "3835526" "[TH-004L]" "Asia/Bangkok" "Asia/Bangkok" "2019-11-01" > /home/admin/Logs/LogsTH004LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-004L_History"
	sleep 10
fi

if [ "$1" == "TH-005L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV2.py "3835213" "[TH-005L]" "Asia/Bangkok" "Asia/Bangkok" "2019-11-01"> /home/admin/Logs/LogsTH005LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-005L_History"
	sleep 10
fi

if [ "$1" == "TH-010L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV3.py "3839347" "[TH-010L]" "Asia/Bangkok" "Asia/Bangkok" "2019-12-01"> /home/admin/Logs/LogsTH010LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-010L_History"
	sleep 10
fi	

if [ "$1" == "TH-011L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV3.py "3839348" "[TH-011L]" "Asia/Bangkok" "Asia/Bangkok" "2019-12-01"> /home/admin/Logs/LogsTH011LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-011L_History"
	sleep 10
fi

if [ "$1" == "TH-012L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV3.py "3840208" "[TH-012L]" "Asia/Bangkok" "Asia/Bangkok" "2019-12-01"> /home/admin/Logs/LogsTH012LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-012L_History"
	sleep 10
fi

if [ "$1" == "TH-013L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV3.py "3839597" "[TH-013L]" "Asia/Bangkok" "Asia/Bangkok" "2019-12-01"> /home/admin/Logs/LogsTH013LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-013L_History"
	sleep 10
fi

if [ "$1" == "TH-014L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV3.py "3844885" "[TH-014L]" "Asia/Bangkok" "Asia/Bangkok" "2019-12-01"> /home/admin/Logs/LogsTH014LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-014L_History"
	sleep 10
fi

if [ "$1" == "TH-015L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV3.py "3840207" "[TH-015L]" "Asia/Bangkok" "Asia/Bangkok" "2019-12-01"> /home/admin/Logs/LogsTH015LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-015L_History"
	sleep 10
fi	

if [ "$1" == "TH-016L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV3.py "3840369" "[TH-016L]" "Asia/Bangkok" "Asia/Bangkok" "2019-12-01"> /home/admin/Logs/LogsTH016LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-016L_History"
	sleep 10
fi

if [ "$1" == "TH-017L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV3.py "3839502" "[TH-017L]" "Asia/Bangkok" "Asia/Bangkok" "2019-12-01"> /home/admin/Logs/LogsTH017LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-017L_History"
	sleep 10
fi	

if [ "$1" == "TH-018L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV3.py "3841478" "[TH-018L]" "Asia/Bangkok" "Asia/Bangkok" "2019-12-01"> /home/admin/Logs/LogsTH018LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-018L_History"
	sleep 10
fi	

if [ "$1" == "TH-019L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV3.py "3841479" "[TH-019L]" "Asia/Bangkok" "Asia/Bangkok" "2019-12-01"> /home/admin/Logs/LogsTH019LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-019L_History"
	sleep 10
fi	

if [ "$1" == "TH-020L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV3.py "3841476" "[TH-020L]" "Asia/Bangkok" "Asia/Bangkok" "2019-12-01"> /home/admin/Logs/LogsTH020LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-020L_History"
	sleep 10
fi	

if [ "$1" == "TH-021L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV3.py "3839596" "[TH-021L]" "Asia/Bangkok" "Asia/Bangkok" "2019-12-01"> /home/admin/Logs/LogsTH021LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-021L_History"
	sleep 10
fi	

if [ "$1" == "TH-022L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV3.py "3840166" "[TH-022L]" "Asia/Bangkok" "Asia/Bangkok" "2019-12-01"> /home/admin/Logs/LogsTH022LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-022L_History"
	sleep 10
fi	

if [ "$1" == "TH-023L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV3.py "3840169" "[TH-023L]" "Asia/Bangkok" "Asia/Bangkok" "2019-12-01"> /home/admin/Logs/LogsTH023LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-023L_History"
	sleep 10
fi

if [ "$1" == "TH-024L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV3.py "3845630" "[TH-024L]" "Asia/Bangkok" "Asia/Bangkok" "2019-12-01"> /home/admin/Logs/LogsTH024LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-024L_History"
	sleep 10
fi

if [ "$1" == "TH-025L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV3.py "3840406" "[TH-025L]" "Asia/Bangkok" "Asia/Bangkok" "2019-12-01"> /home/admin/Logs/LogsTH025LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-025L_History"
	sleep 10
fi

if [ "$1" == "TH-026L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV3.py "3840480" "[TH-026L]" "Asia/Bangkok" "Asia/Bangkok" "2019-12-01"> /home/admin/Logs/LogsTH026LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-026L_History"
	sleep 10
fi	

if [ "$1" == "TH-027L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV3.py "3840205" "[TH-027L]" "Asia/Bangkok" "Asia/Bangkok" "2019-12-01"> /home/admin/Logs/LogsTH027LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-027L_History"
	sleep 10
fi	

if [ "$1" == "TH-028L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV3.py "3844886" "[TH-028L]" "Asia/Bangkok" "Asia/Bangkok" "2019-12-01"> /home/admin/Logs/LogsTH028LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-028L_History"
	sleep 10
fi
if [ "$1" == "VN-002L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1Live.py "3818352" "[VN-002L]" "Asia/Saigon" "Asia/Saigon" > /home/admin/Logs/LogsVN002LHistory.txt &
	y=`echo $!`
	x="$x\n$y --VN-002L_History"
	sleep 10
fi

if [ "$1" == "VN-003L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1Live.py "3822066" "[VN-003L]" "Asia/Saigon" "Asia/Saigon" > /home/admin/Logs/LogsVN003LHistory.txt &
	y=`echo $!`
	x="$x\n$y --VN-003L_History"
	sleep 10
fi

#if [ "$1" == "IN-052L_History" ] || [ $RUNALL == 1 ] 
#then
#	Rscript /home/admin/CODE/IN052Digest/Live.R &
#	y=`echo $!`
#	x="$x\n$y --IN-052L_History"
#	sleep 10
#fi

if [ "$1" == "IN-031L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN031LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-031L_Mail"
	sleep 20 # extra sleep since all digests upto SG-005S are linked
fi

if [ "$1" == "IN-038L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN038LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-038L_Mail"
	sleep 20 # extra sleep since all digests upto SG-005S are linked
fi

if [ "$1" == "IN-045L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN045LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-045L_Mail"
	sleep 20 # extra sleep since all digests upto SG-005S are linked
fi

if [ "$1" == "IN-047L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN047LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-047L_Mail"
	sleep 20 # extra sleep since all digests upto SG-005S are linked
fi

if [ "$1" == "IN-048L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN048LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-048L_Mail"
	sleep 20 # extra sleep since all digests upto SG-005S are linked
fi

if [ "$1" == "IN-049L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN049LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-049L_Mail"
	sleep 20 # extra sleep since all digests upto SG-005S are linked
fi

if [ "$1" == "IN-051L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN051LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-051L_Mail"
	sleep 20 # extra sleep since all digests upto SG-005S are linked
fi


if [ "$1" == "IN-052L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN052LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-052L_Mail"
	sleep 20 # extra sleep since all digests upto SG-005S are linked
fi

if [ "$1" == "IN-055L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN055LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-055L_Mail"
	sleep 20 # extra sleep since all digests upto SG-005S are linked
fi

if [ "$1" == "IN-056L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN056LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-056L_Mail"
	sleep 20 # extra sleep since all digests upto SG-005S are linked
fi

if [ "$1" == "IN-059L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN059LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-059L_Mail"
	sleep 20 # extra sleep since all digests upto SG-005S are linked
fi

if [ "$1" == "IN-060L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN060LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-060L_Mail"
	sleep 20 # extra sleep since all digests upto SG-005S are linked
fi


if [ "$1" == "IN-061L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN061LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-061L_Mail"
	sleep 20 # extra sleep since all digests upto SG-005S are linked
fi

if [ "$1" == "IN-062L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN062LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-062L_Mail"
	sleep 20 # extra sleep since all digests upto SG-005S are linked
fi

if [ "$1" == "IN-063L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN063LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-063L_Mail"
	sleep 20 # extra sleep since all digests upto SG-005S are linked
fi

if [ "$1" == "IN-066L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN066LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-066L_Mail"
	sleep 20 # extra sleep since all digests upto SG-005S are linked
fi

if [ "$1" == "IN-067L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN067LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-067L_Mail"
	sleep 20 # extra sleep since all digests upto SG-005S are linked
fi

if [ "$1" == "IN-068L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN068LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-068L_Mail"
	sleep 20 # extra sleep since all digests upto SG-005S are linked
fi

if [ "$1" == "IN-071L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN071LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-071L_Mail"
	sleep 20 # extra sleep since all digests upto SG-005S are linked
fi

if [ "$1" == "IN-072L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN072LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-072L_Mail"
	sleep 20 # extra sleep since all digests upto SG-005S are linked
fi

if [ "$1" == "IN-073L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN073LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-073L_Mail"
	sleep 20 # extra sleep since all digests upto SG-005S are linked
fi

if [ "$1" == "IN-074L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN074LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-074L_Mail"
	sleep 20 # extra sleep since all digests upto SG-005S are linked
fi

if [ "$1" == "MY-008L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/MY008LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --MY-008L_Mail"
fi

if [ "$1" == "MY-009L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/MY009LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --MY-009L_Mail"
fi

if [ "$1" == "SG-003S_Live" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python3 /home/admin/CODE/716Live/Live.py > /home/admin/Logs/Logs716History.txt &
	y=`echo $!`
	x="$x\n$y --SG-003S_Live"
	sleep 10
fi

if [ "$1" == "SG-003S_Live_Azure_4g" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/716Live/Live_Azure_4g.py > /home/admin/Logs/Logs716AzureHistory.txt &
	y=`echo $!`
	x="$x\n$y --SG-003S_Live_Azure_4g"
	sleep 10
fi

if [ "$1" == "SG-005S_Live" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python3 /home/admin/CODE/725Live/Live.py > /home/admin/Logs/Logs725History.txt &
	y=`echo $!`
	x="$x\n$y --SG-005S_Live"
	sleep 10
fi

if [ "$1" == "SG-005S_Live_Azure_4g" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/725Live/Live_Azure_4g.py > /home/admin/Logs/Logs725AzureHistory.txt &
	y=`echo $!`
	x="$x\n$y --SG-005S_Live_Azure_4g"
	sleep 10
fi

if [ "$1" == "KH-008S_Live" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python3 /home/admin/CODE/728Live/Live.py > /home/admin/Logs/Logs728History.txt &
	y=`echo $!`
	x="$x\n$y --KH-008S_Live"
	sleep 10
fi

if [ "$1" == "KH-008S_Live_Azure_4g" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/728Live/Live_Azure_4g.py > /home/admin/Logs/Logs728AzureHistory.txt &
	y=`echo $!`
	x="$x\n$y --KH-008S_Live_Azure_4g"
	sleep 10
fi

if [ "$1" == "KH-003S_Live" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python3 /home/admin/CODE/715Live/Live.py > /home/admin/Logs/Logs715History.txt &
	y=`echo $!`
	x="$x\n$y --KH-003S_Live"
	sleep 10
fi

if [ "$1" == "KH-003S_Live_Azure_4g" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/715Live/Live_Azure_4g.py > /home/admin/Logs/Logs715AzureHistory.txt &
	y=`echo $!`
	x="$x\n$y --KH-003S_Live_Azure_4g"
	sleep 10
fi

if [ "$1" == "MY-006S_Live" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python3 /home/admin/CODE/729Live/Live.py > /home/admin/Logs/Logs729History.txt &
	y=`echo $!`
	x="$x\n$y --MY-006S_Live"
	sleep 10
fi

if [ "$1" == "MY-006S_Live_Azure_4g" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/729Live/Live_Azure_4g.py > /home/admin/Logs/Logs729AzureHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-006S_Live_Azure_4g"
	sleep 10
fi

if [ "$1" == "IN-015S_Live" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python3 /home/admin/CODE/712Live/Live.py > /home/admin/Logs/Logs712History.txt &
	y=`echo $!`
	x="$x\n$y --IN-015S_Live"
	sleep 10
fi

if [ "$1" == "IN-015S_Live_Azure_4g" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/712Live/Live_Azure_4g.py > /home/admin/Logs/Logs712AzureHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-015S_Live_Azure_4g"
	sleep 10
fi

if [ "$1" == "IN-036S_Live" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python3 /home/admin/CODE/718Live/Live.py > /home/admin/Logs/Logs718History.txt &
	y=`echo $!`
	x="$x\n$y --IN-036S_Live"
	sleep 10
fi

if [ "$1" == "IN-036S_Live_Azure_4g" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/718Live/Live_Azure_4g.py > /home/admin/Logs/Logs718AzureHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-036S_Live_Azure_4g"
	sleep 10
fi

if [ "$1" == "SerisSummary_Live_Azure_4g" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/DatabaseCreation/SERIS/Azure_Live_SerisSummary.py > /home/admin/Logs/LogsSerisSummaryAzureHistory.txt &
	y=`echo $!`
	x="$x\n$y --SerisSummary_Live_Azure_4g"
	sleep 10
fi

if [ "$1" == "SerisLifetimeMaster" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/Lifetime/SerisLifetimeMaster.py > /home/admin/Logs/LogsSerisLifetimeMaster.txt &
	y=`echo $!`
	x="$x\n$y --SerisLifetimeMaster"
	sleep 10
fi

if [ "$1" == "FlexiLifetimeMaster" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/Lifetime/FlexiLifetimeMaster.py > /home/admin/Logs/LogsFlexiLifetimeMaster.txt &
	y=`echo $!`
	x="$x\n$y --FlexiLifetimeMaster"
	sleep 10
fi

if [ "$1" == "SG-006_Lifetime" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/Lifetime/SG006Lifetime.py > /home/admin/Logs/LogsSG006LifetimeHistory.txt &
	y=`echo $!`
	x="$x\n$y --SG-006_Lifetime"
	sleep 10
fi

if [ "$1" == "Web_Interface" ] || [ $RUNALL == 1 ] 
then
	nohup python3  /home/admin/CODE/WebInterface/Interface.py > /home/admin/Logs/LogsWebInterface.txt 2>&1 &
	y=`echo $!`
	x="$x\n$y --Web_Interface"
	sleep 10
fi

if [ "$1" == "Invoice_View" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/Invoicing/View.py > /home/admin/Logs/LogsInvoiceHistory.txt &
	y=`echo $!`
	x="$x\n$y --Invoice_View"
	sleep 10
fi

if [ "$1" == "Tesco_Alarms" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/Tesco/tesco_alarms.py > /home/admin/Logs/LogsTescoAlarmsHistory.txt &
	y=`echo $!`
	x="$x\n$y --Tesco_Alarms"
	sleep 10
fi

if [ "$1" == "Tesco_Server_Push" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python3 /home/admin/CODE/Tesco/tesco_server_push.py > /home/admin/Logs/LogsTescoServerPushHistory.txt &
	y=`echo $!`
	x="$x\n$y --Tesco_Server_Push"
	sleep 10
fi
