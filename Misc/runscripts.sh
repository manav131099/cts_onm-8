#!/bin/bash
RUNALL=0
x=""
if [ $# == 0 ] 
then
	RUNALL=1
	echo "Running all scripts"
fi

RUNPYTHONONLY=0
RUNRONLY=0

if [ $# == 1 ]
	then
	if [ "$1" == "python-only" ]
	then
		RUNPYTHONONLY=1
		RUNALL=1
		echo "Running only python scripts"
	elif [ "$1" == "R-only" ]
	then
		RUNALL=1
		RUNRONLY=1
		echo "Running only R scripts"
	fi
fi

echo "Value of RUNALL $RUNALL"
echo "Value of RUNPYTHONONLY $RUNPYTHONONLY"
echo "Value of RUNRONLY $RUNRONLY"

if [ $RUNPYTHONONLY -eq 0 ] 
then
if [ "$1" == "GIS_Live" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/GIS/FTPCapture.R &
	x=`echo $!`
	x="$x --GIS_Live"
	if [ "$1" == "GIS_Live" ] 
	then
		x="\n$x"
	fi
	sleep 10
fi

if [ "$1" == "IN-711S_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/711Digest/MailDigest711.R &
	y=`echo $!`
	x="$x\n$y --IN-711S_Mail"
	sleep 10
fi

if [ "$1" == "KH-714S_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/714Digest/MailDigest714.R &
	y=`echo $!`
	x="$x\n$y --KH-714S_Mail"
	sleep 10
fi

if [ "$1" == "KH-003S_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/KH003NIDigest/MailDigestKH003.R &
	y=`echo $!`
	x="$x\n$y --KH-003S_Mail"
	sleep 10
fi

if [ "$1" == "KH-009L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/KH009LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --KH-009L_Mail"
	sleep 10
fi

if [ "$1" == "IN-015S_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN015Digest/MailDigestIN015.R &
	y=`echo $!`
	x="$x\n$y --IN-015S_Mail"
	sleep 10
fi

if [ "$1" == "IN-713S_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/713Digest/MailDigest713.R &
	y=`echo $!`
	x="$x\n$y --IN-713S_Mail"
	sleep 10
fi

if [ "$1" == "SG-003S_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/716Digest/MailDigest716.R &
	y=`echo $!`
	x="$x\n$y --SG-003S_Mail"
	sleep 10
fi

if [ "$1" == "SG-007L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SG007LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --SG-007L_Mail"
	sleep 10
fi

if [ "$1" == "SG-008L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SG008LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --SG-008L_Mail"
	sleep 10
fi


if [ "$1" == "PH-719S_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/719Digest/MailDigest719.R &
	y=`echo $!`
	x="$x\n$y --PH-719S_Mail"
	sleep 10
fi

if [ "$1" == "SG-004S_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SGDigest/SG004SDigest/MailDigest717.R &
	y=`echo $!`
	x="$x\n$y --SG-004S_Mail"
	sleep 10
fi

if [ "$1" == "PH-006S_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/PH006SDigest/MailDigest719.R &
	y=`echo $!`
	x="$x\n$y --PH-006S_Mail"
	sleep 10
fi

if [ "$1" == "IN-036S_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN036Digest/MailDigestIN036.R &
	y=`echo $!`
	x="$x\n$y --IN-036S_Mail"
	sleep 10
fi

if [ "$1" == "IN-003W_Gen_1" ] || [ $RUNALL == 1 ] 
then
	python3 /home/admin/CODE/IN003WDigest/Live.py  > /home/admin/Logs/LogsIN003WHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-003W_Gen_1"
	sleep 10 
fi

if [ "$1" == "IN-014L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV4.py "3850755" "[IN-014L]" "Asia/Kolkata" "Asia/Calcutta" "2020-06-10"> /home/admin/Logs/LogsIN014LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-014L_History"
	sleep 10
fi

if [ "$1" == "IN-018L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1Live.py "3818604" "[IN-018L]" "Asia/Kolkata" "Asia/Calcutta" > /home/admin/Logs/LogsIN018LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-018L_History"
	sleep 10
fi

if [ "$1" == "IN-021L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV2.py "3835525" "[IN-021L]" "Asia/Kolkata" "Asia/Calcutta" "2019-11-01"> /home/admin/Logs/LogsIN021LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-021L_History"
	sleep 10
fi

if [ "$1" == "IN-022L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV4.py "3858321" "[IN-022L]" "Asia/Kolkata" "Asia/Calcutta" "2020-06-10"> /home/admin/Logs/LogsIN022LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-022L_History"
	sleep 10
fi

if [ "$1" == "IN-023L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV4.py "3863937" "[IN-023L]" "Asia/Kolkata" "Asia/Calcutta" "2020-07-15"> /home/admin/Logs/LogsIN023LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-023L_History"
	sleep 10
fi

if [ "$1" == "IN-031L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1Live.py "3802193" "[IN-031L]" "Asia/Kolkata" "Asia/Calcutta" > /home/admin/Logs/LogsIN031LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-031L_History"
	sleep 10
fi

if [ "$1" == "IN-038L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1Live.py "3765443" "[IN-038L]" "Asia/Kolkata" "Asia/Calcutta" > /home/admin/Logs/LogsIN038LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-038L_History"
	sleep 10
fi

if [ "$1" == "IN-037L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV4.py "3860495" "[IN-037L]" "Asia/Kolkata" "Asia/Calcutta" "2020-05-28"> /home/admin/Logs/LogsIN037LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-037L_History"
	sleep 10
fi

if [ "$1" == "IN-039L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV4.py "3860862" "[IN-039L]" "Asia/Kolkata" "Asia/Calcutta" "2020-06-01"> /home/admin/Logs/LogsIN039LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-039L_History"
	sleep 10
fi


if [ "$1" == "IN-041L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV2.py "3835524" "[IN-041L]" "Asia/Kolkata" "Asia/Calcutta" "2019-11-01"> /home/admin/Logs/LogsIN041LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-041L_History"
	sleep 10
fi

if [ "$1" == "IN-042L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1Live.py "3777348" "[IN-042L]" "Asia/Kolkata" "Asia/Calcutta" > /home/admin/Logs/LogsIN042LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-042L_History"
	sleep 10
fi

if [ "$1" == "IN-043L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1Live.py "3777349" "[IN-043L]" "Asia/Kolkata" "Asia/Calcutta" > /home/admin/Logs/LogsIN043LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-043L_History"
	sleep 10
fi

if [ "$1" == "IN-045L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1Live.py "3794812" "[IN-045L]" "Asia/Kolkata" "Asia/Calcutta" > /home/admin/Logs/LogsIN045LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-045L_History"
	sleep 10
fi

if [ "$1" == "IN-047L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1Live.py "3794653" "[IN-047L]" "Asia/Kolkata" "Asia/Calcutta" > /home/admin/Logs/LogsIN047LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-047L_History"
	sleep 10
fi

if [ "$1" == "IN-048L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/IN048LDigest/Live.py  > /home/admin/Logs/LogsIN048LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-048L_History"
	sleep 10
fi

if [ "$1" == "IN-049L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1Live.py "3794253" "[IN-049L]" "Asia/Kolkata" "Asia/Calcutta" > /home/admin/Logs/LogsIN049LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-049L_History"
	sleep 10
fi

if [ "$1" == "IN-051L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV2.py "3787245" "[IN-051L]" "Asia/Kolkata" "Asia/Calcutta" "2019-07-01"> /home/admin/Logs/LogsIN051LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-051L_History"
	sleep 10
fi

if [ "$1" == "IN-052L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/IN052LDigest/Live.py > /home/admin/Logs/LogsIN052LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-052L_History"
	sleep 10
fi

if [ "$1" == "IN-053L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/IN053LDigest/Live.py > /home/admin/Logs/LogsIN053LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-053L_History"
	sleep 10
fi

if [ "$1" == "IN-055L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1Live.py "3795317" "[IN-055L]" "Asia/Kolkata" "Asia/Calcutta" > /home/admin/Logs/LogsIN055LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-055L_History"
	sleep 10
fi


if [ "$1" == "IN-056L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/IN056LDigest/Live.py > /home/admin/Logs/LogsIN056LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-056L_History"
	sleep 10
fi

if [ "$1" == "IN-058L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/IN058LDigest/Live.py > /home/admin/Logs/LogsIN058LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-058L_History"
	sleep 10
fi


if [ "$1" == "IN-058L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN058LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-058L_Mail"
	sleep 20 
fi


if [ "$1" == "IN-059L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1Live.py "3800132" "[IN-059L]" "Asia/Kolkata" "Asia/Calcutta" > /home/admin/Logs/LogsIN059LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-059L_History"
	sleep 10
fi

if [ "$1" == "IN-060L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/IN060LDigest/Live.py > /home/admin/Logs/LogsIN060LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-060L_History"
	sleep 10
fi

if [ "$1" == "IN-061L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1Live.py "3800671" "[IN-061L]" "Asia/Kolkata" "Asia/Calcutta" > /home/admin/Logs/LogsIN061LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-061L_History"
	sleep 10
fi

if [ "$1" == "IN-062L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/IN062LDigest/Live.py > /home/admin/Logs/LogsIN062LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-062L_History"
	sleep 10
fi

if [ "$1" == "IN-063L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1Live.py "3802488" "[IN-063L]" "Asia/Kolkata" "Asia/Calcutta" > /home/admin/Logs/LogsIN063LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-063L_History"
	sleep 10
fi

if [ "$1" == "IN-064L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/IN064LDigest/Live.py > /home/admin/Logs/LogsIN064LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-064L_History"
	sleep 10
fi

if [ "$1" == "IN-065L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1Live.py "3814767" "[IN-065L]" "Asia/Kolkata" "Asia/Calcutta" > /home/admin/Logs/LogsIN065LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-065L_History"
	sleep 10
fi

if [ "$1" == "IN-066L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/IN066LDigest/Live.py > /home/admin/Logs/LogsIN066LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-066L_History"
	sleep 10
fi

if [ "$1" == "IN-067L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1Live.py "3804923" "[IN-067L]" "Asia/Kolkata" "Asia/Calcutta" > /home/admin/Logs/LogsIN067LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-067L_History"
	sleep 10
fi

if [ "$1" == "IN-068L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/IN068LDigest/Live.py > /home/admin/Logs/LogsIN068LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-068L_History"
	sleep 10
fi

if [ "$1" == "IN-069L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/IN069LDigest/Live.py > /home/admin/Logs/LogsIN069LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-069L_History"
	sleep 10
fi

if [ "$1" == "IN-071L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1Live.py "3804130" "[IN-071L]" "Asia/Kolkata" "Asia/Calcutta" > /home/admin/Logs/LogsIN071LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-071L_History"
	sleep 10
fi

if [ "$1" == "IN-072L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1Live.py "3805118" "[IN-072L]" "Asia/Kolkata" "Asia/Calcutta" > /home/admin/Logs/LogsIN072LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-072L_History"
	sleep 10
fi

if [ "$1" == "IN-073L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV3.py "3805120" "[IN-073L]" "Asia/Kolkata" "Asia/Calcutta" "2019-09-01" > /home/admin/Logs/LogsIN073LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-073L_History"
	sleep 10
fi

if [ "$1" == "IN-074L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1Live.py "3820177" "[IN-074L]" "Asia/Kolkata" "Asia/Calcutta" > /home/admin/Logs/LogsIN074LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-074L_History"
	sleep 10
fi

if [ "$1" == "IN-075L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/IN075LDigest/Live.py > /home/admin/Logs/LogsIN075LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-075L_History"
	sleep 10
fi
 
if [ "$1" == "IN-076L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1Live.py "3822707" "[IN-076L]" "Asia/Kolkata" "Asia/Calcutta" > /home/admin/Logs/LogsIN076LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-076L_History"
	sleep 10
fi

if [ "$1" == "IN-077L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1Live.py "3823815" "[IN-077L]" "Asia/Kolkata" "Asia/Calcutta" > /home/admin/Logs/LogsIN077LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-077L_History"
	sleep 10
fi

if [ "$1" == "IN-078L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1Live.py "3821964" "[IN-078L]" "Asia/Kolkata" "Asia/Calcutta" > /home/admin/Logs/LogsIN078LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-078L_History"
	sleep 10
fi

if [ "$1" == "IN-079L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV3.py "3829586" "[IN-079L]" "Asia/Kolkata" "Asia/Calcutta" "2019-09-01"> /home/admin/Logs/LogsIN079LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-079L_History"
	sleep 10
fi

if [ "$1" == "IN-080L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/IN080LDigest/Live.py  > /home/admin/Logs/LogsIN080LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-080L_History"
	sleep 10
fi

if [ "$1" == "IN-081L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV3.py "3836445" "[IN-081L]" "Asia/Kolkata" "Asia/Calcutta" "2019-12-01"> /home/admin/Logs/LogsIN081LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-081L_History"
	sleep 10
fi

if [ "$1" == "IN-082L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV3.py "3835833" "[IN-082L]" "Asia/Kolkata" "Asia/Calcutta" "2019-12-01"> /home/admin/Logs/LogsIN082LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-082L_History"
	sleep 10
fi

if [ "$1" == "IN-083L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV3.py "3839350" "[IN-083L]" "Asia/Kolkata" "Asia/Calcutta" "2019-12-01"> /home/admin/Logs/LogsIN083LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-083L_History"
	sleep 10
fi

if [ "$1" == "IN-085L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV3.py "3849158" "[IN-085L]" "Asia/Kolkata" "Asia/Calcutta" "2020-02-10"> /home/admin/Logs/LogsIN085LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-085L_History"
	sleep 10
fi

if [ "$1" == "IN-086L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV3.py "3850328" "[IN-086L]" "Asia/Kolkata" "Asia/Calcutta" "2020-02-10"> /home/admin/Logs/LogsIN086LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-086L_History"
	sleep 10
fi

if [ "$1" == "IN-087L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV4.py "3850906" "[IN-087L]" "Asia/Kolkata" "Asia/Calcutta" "2020-03-01"> /home/admin/Logs/LogsIN087LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-087L_History"
	sleep 10
fi

if [ "$1" == "IN-088L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV4.py "3862554" "[IN-088L]" "Asia/Kolkata" "Asia/Calcutta" "2020-06-10"> /home/admin/Logs/LogsIN088LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-088L_History"
	sleep 10
fi

if [ "$1" == "IN-089L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV4.py "3851158" "[IN-089L]" "Asia/Kolkata" "Asia/Calcutta" "2020-07-01"> /home/admin/Logs/LogsIN089LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-089L_History"
	sleep 10
fi

if [ "$1" == "IN-301L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV5.py "3862993" "[IN-301L]" "Asia/Kolkata" "Asia/Calcutta" "2020-07-01"> /home/admin/Logs/LogsIN301LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-301L_History"
	sleep 10
fi

if [ "$1" == "KH-009L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV3.py "3848700" "[KH-009L]" "Asia/Phnom_Penh" "Asia/Phnom_Penh" "2020-02-01"> /home/admin/Logs/LogsKH009LHistory.txt &
	y=`echo $!`
	x="$x\n$y --KH-009L_History"
	sleep 10
fi

if [ "$1" == "MY-002L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV4.py "3862535" "[MY-002L]" "Asia/Kolkata" "Asia/Calcutta" "2020-06-15"> /home/admin/Logs/LogsMY002LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-002L_History"
	sleep 10
fi

if [ "$1" == "MY-005L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV2.py "3819590" "[MY-005L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2019-06-01"> /home/admin/Logs/LogsMY005LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-005L_History"
	sleep 10
fi	

if [ "$1" == "MY-007L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1Live.py "3827749" "[MY-007L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" > /home/admin/Logs/LogsMY007LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-007L_History"
	sleep 10
fi

if [ "$1" == "MY-008L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1Live.py "3824826" "[MY-008L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" > /home/admin/Logs/LogsMY008LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-008L_History"
	sleep 10
fi

if [ "$1" == "MY-009L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV3.py "3836443" "[MY-009L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2019-12-01"> /home/admin/Logs/LogsMY009LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-009L_History"
	sleep 10
fi

if [ "$1" == "MY-010L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV3.py "3849442" "[MY-010L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-02-01"> /home/admin/Logs/LogsMY010LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-010L_History"
	sleep 10
fi

if [ "$1" == "MY-401L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV3.py "3845432" "[MY-401L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-01-01"> /home/admin/Logs/LogsMY401LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-401L_History"
	sleep 10
fi

if [ "$1" == "MY-402L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV3.py "3845631" "[MY-402L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-01-01"> /home/admin/Logs/LogsMY402LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-402L_History"
	sleep 10
fi

if [ "$1" == "MY-403L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV3.py "3845864" "[MY-403L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-01-01"> /home/admin/Logs/LogsMY403LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-403L_History"
	sleep 10
fi

if [ "$1" == "MY-404L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV3.py "3846888" "[MY-404L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-02-01"> /home/admin/Logs/LogsMY404LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-404L_History"
	sleep 10
fi

if [ "$1" == "MY-405L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV3.py "3847585" "[MY-405L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-01-01"> /home/admin/Logs/LogsMY405LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-405L_History"
	sleep 10
fi

if [ "$1" == "MY-406L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV3.py "3848698" "[MY-406L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-02-01"> /home/admin/Logs/LogsMY406LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-406L_History"
	sleep 10
fi


if [ "$1" == "Shell_Alarm" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/Alarms/Shell_Alarm.py > /home/admin/Logs/LogsShellAlarm.txt &
	y=`echo $!`
	x="$x\n$y --Shell_Alarm"
	sleep 10
fi

if [ "$1" == "PH-001L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV2.py "3827787" "[PH-001L]" "Asia/Manila" "Asia/Manila" "2019-07-01"> /home/admin/Logs/LogsPH001LHistory.txt &
	y=`echo $!`
	x="$x\n$y --PH-001L_History"
	sleep 10
fi

if [ "$1" == "PH-002L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV2.py "3830382" "[PH-002L]" "Asia/Manila" "Asia/Manila" "2019-07-01"> /home/admin/Logs/LogsPH002LHistory.txt &
	y=`echo $!`
	x="$x\n$y --PH-002L_History"
	sleep 10
fi

if [ "$1" == "PH-003L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV2.py "3828091" "[PH-003L]" "Asia/Manila" "Asia/Manila" "2019-07-01"> /home/admin/Logs/LogsPH003LHistory.txt &
	y=`echo $!`
	x="$x\n$y --PH-003L_History"
	sleep 10
fi

if [ "$1" == "PH-004L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/PH004LDigest/Live.py > /home/admin/Logs/LogsPH004LHistory.txt &
	y=`echo $!`
	x="$x\n$y --PH-004L_History"
	sleep 10
fi

if [ "$1" == "PH-006L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1Live.py "3828003" "[PH-006L]" "Asia/Manila" "Asia/Manila" "2019-07-01"> /home/admin/Logs/LogsPH006LHistory.txt &
	y=`echo $!`
	x="$x\n$y --PH-006L_History"
	sleep 10
fi

if [ "$1" == "PH-007L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1Live.py "3826338" "[PH-007L]" "Asia/Manila" "Asia/Manila" "2019-07-01"> /home/admin/Logs/LogsPH007LHistory.txt &
	y=`echo $!`
	x="$x\n$y --PH-007L_History"
	sleep 10
fi

if [ "$1" == "SG-007L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1Live.py "3795604" "[SG-007L]" "Asia/Singapore" "Asia/Singapore" > /home/admin/Logs/LogsSG007LHistory.txt &
	y=`echo $!`
	x="$x\n$y --SG-007L_History"
	sleep 10
fi

if [ "$1" == "SG-008L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV3.py "3836631" "[SG-008L]" "Asia/Singapore" "Asia/Singapore" "2019-12-01" > /home/admin/Logs/LogsSG008LHistory.txt &
	y=`echo $!`
	x="$x\n$y --SG-008L_History"
	sleep 10
fi

if [ "$1" == "TH-002L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV4.py "3862423" "[TH-002L]" "Asia/Bangkok" "Asia/Bangkok" "2020-06-15"> /home/admin/Logs/LogsTH002LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-002L_History"
	sleep 10
fi

if [ "$1" == "TH-004L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV2.py "3835526" "[TH-004L]" "Asia/Bangkok" "Asia/Bangkok" "2019-11-01" > /home/admin/Logs/LogsTH004LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-004L_History"
	sleep 10
fi

if [ "$1" == "TH-005L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV2.py "3835213" "[TH-005L]" "Asia/Bangkok" "Asia/Bangkok" "2019-11-01"> /home/admin/Logs/LogsTH005LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-005L_History"
	sleep 10
fi

if [ "$1" == "TH-007L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV3.py "3846845" "[TH-007L]" "Asia/Bangkok" "Asia/Bangkok" "2020-02-01"> /home/admin/Logs/LogsTH007LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-007L_History"
	sleep 10
fi

if [ "$1" == "TH-010L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveTescoAzure.py "3839347" "[TH-010L]" "Asia/Bangkok" "Asia/Bangkok" "2019-12-01"> /home/admin/Logs/LogsTH010LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-010L_History"
	sleep 10
fi	

if [ "$1" == "TH-011L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveTescoAzure.py "3839348" "[TH-011L]" "Asia/Bangkok" "Asia/Bangkok" "2019-12-01"> /home/admin/Logs/LogsTH011LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-011L_History"
	sleep 10
fi

if [ "$1" == "TH-012L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveTescoAzure.py "3840208" "[TH-012L]" "Asia/Bangkok" "Asia/Bangkok" "2019-12-01"> /home/admin/Logs/LogsTH012LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-012L_History"
	sleep 10
fi

if [ "$1" == "TH-013L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveTescoAzure.py "3839597" "[TH-013L]" "Asia/Bangkok" "Asia/Bangkok" "2019-12-01"> /home/admin/Logs/LogsTH013LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-013L_History"
	sleep 10
fi

if [ "$1" == "TH-014L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveTescoAzure.py "3844885" "[TH-014L]" "Asia/Bangkok" "Asia/Bangkok" "2019-12-01"> /home/admin/Logs/LogsTH014LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-014L_History"
	sleep 10
fi

if [ "$1" == "TH-015L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveTescoAzure.py "3840207" "[TH-015L]" "Asia/Bangkok" "Asia/Bangkok" "2019-12-01"> /home/admin/Logs/LogsTH015LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-015L_History"
	sleep 10
fi	

if [ "$1" == "TH-016L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveTescoAzure.py "3840369" "[TH-016L]" "Asia/Bangkok" "Asia/Bangkok" "2019-12-01"> /home/admin/Logs/LogsTH016LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-016L_History"
	sleep 10
fi

if [ "$1" == "TH-017L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveTescoAzure.py "3839502" "[TH-017L]" "Asia/Bangkok" "Asia/Bangkok" "2019-12-01"> /home/admin/Logs/LogsTH017LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-017L_History"
	sleep 10
fi	

if [ "$1" == "TH-018L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveTescoAzure.py "3841478" "[TH-018L]" "Asia/Bangkok" "Asia/Bangkok" "2019-12-01"> /home/admin/Logs/LogsTH018LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-018L_History"
	sleep 10
fi	

if [ "$1" == "TH-019L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveTescoAzure.py "3841479" "[TH-019L]" "Asia/Bangkok" "Asia/Bangkok" "2019-12-01"> /home/admin/Logs/LogsTH019LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-019L_History"
	sleep 10
fi	

if [ "$1" == "TH-020L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveTescoAzure.py "3841476" "[TH-020L]" "Asia/Bangkok" "Asia/Bangkok" "2019-12-01"> /home/admin/Logs/LogsTH020LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-020L_History"
	sleep 10
fi	

if [ "$1" == "TH-021L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveTescoAzure.py "3839596" "[TH-021L]" "Asia/Bangkok" "Asia/Bangkok" "2019-12-01"> /home/admin/Logs/LogsTH021LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-021L_History"
	sleep 10
fi

if [ "$1" == "TH-022L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveTescoAzure.py "3840166" "[TH-022L]" "Asia/Bangkok" "Asia/Bangkok" "2019-12-01"> /home/admin/Logs/LogsTH022LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-022L_History"
	sleep 10
fi		

if [ "$1" == "TH-023L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveTescoAzure.py "3840169" "[TH-023L]" "Asia/Bangkok" "Asia/Bangkok" "2019-12-01"> /home/admin/Logs/LogsTH023LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-023L_History"
	sleep 10
fi	

if [ "$1" == "TH-024L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveTescoAzure.py "3845630" "[TH-024L]" "Asia/Bangkok" "Asia/Bangkok" "2019-12-01"> /home/admin/Logs/LogsTH024LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-024L_History"
	sleep 10
fi

if [ "$1" == "TH-025L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveTescoAzure.py "3840406" "[TH-025L]" "Asia/Bangkok" "Asia/Bangkok" "2019-12-01"> /home/admin/Logs/LogsTH025LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-025L_History"
	sleep 10
fi

if [ "$1" == "TH-026L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveTescoAzure.py "3840480" "[TH-026L]" "Asia/Bangkok" "Asia/Bangkok" "2019-12-01"> /home/admin/Logs/LogsTH026LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-026L_History"
	sleep 10
fi

if [ "$1" == "TH-027L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveTescoAzure.py "3840205" "[TH-027L]" "Asia/Bangkok" "Asia/Bangkok" "2019-12-01"> /home/admin/Logs/LogsTH027LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-027L_History"
	sleep 10
fi	

if [ "$1" == "TH-028L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveTescoAzure.py "3844886" "[TH-028L]" "Asia/Bangkok" "Asia/Bangkok" "2019-12-01"> /home/admin/Logs/LogsTH028LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-028L_History"
	sleep 10
fi

if [ "$1" == "TH-046L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV3.py "3857794" "[TH-046L]" "Asia/Bangkok" "Asia/Bangkok" "2020-02-25"> /home/admin/Logs/LogsTH046LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-046L_History"
	sleep 10
fi

if [ "$1" == "TH-047L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV3.py "3850754" "[TH-047L]" "Asia/Bangkok" "Asia/Bangkok" "2020-02-25"> /home/admin/Logs/LogsTH047LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-047L_History"
	sleep 10
fi

if [ "$1" == "TH-048L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV3.py "3850298" "[TH-048L]" "Asia/Bangkok" "Asia/Bangkok" "2020-02-25"> /home/admin/Logs/LogsTH048LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-048L_History"
	sleep 10
fi

if [ "$1" == "TH-049L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV3.py "3849157" "[TH-049L]" "Asia/Bangkok" "Asia/Bangkok" "2020-02-25"> /home/admin/Logs/LogsTH049LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-049L_History"
	sleep 10
fi	

if [ "$1" == "VN-002L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1Live.py "3818352" "[VN-002L]" "Asia/Saigon" "Asia/Saigon" > /home/admin/Logs/LogsVN002LHistory.txt &
	y=`echo $!`
	x="$x\n$y --VN-002L_History"
	sleep 10
fi

if [ "$1" == "VN-003L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1Live.py "3822066" "[VN-003L]" "Asia/Saigon" "Asia/Saigon" > /home/admin/Logs/LogsVN003LHistory.txt &
	y=`echo $!`
	x="$x\n$y --VN-003L_History"
	sleep 10
fi

if [ "$1" == "SG-003S_Live" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python3 /home/admin/CODE/716Live/Live.py > /home/admin/Logs/Logs716History.txt &
	y=`echo $!`
	x="$x\n$y --SG-003S_Live"
	sleep 10
fi

if [ "$1" == "SG-003S_Live_Azure_4g" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/716Live/Live_Azure_4g.py > /home/admin/Logs/Logs716AzureHistory.txt &
	y=`echo $!`
	x="$x\n$y --SG-003S_Live_Azure_4g"
	sleep 10
fi

if [ "$1" == "SG-004S_Live" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python3 /home/admin/CODE/717Live/Live.py > /home/admin/Logs/Logs717History.txt &
	y=`echo $!`
	x="$x\n$y --SG-004S_Live"
	sleep 10
fi

if [ "$1" == "SG-004S_Live_Azure_4g" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/717Live/Live_Azure_4g.py > /home/admin/Logs/Logs717AzureHistory.txt &
	y=`echo $!`
	x="$x\n$y --SG-004S_Live_Azure_4g"
	sleep 10
fi

if [ "$1" == "SG-005S_Live" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python3 /home/admin/CODE/725Live/Live.py > /home/admin/Logs/Logs725History.txt &
	y=`echo $!`
	x="$x\n$y --SG-005S_Live"
	sleep 10
fi

if [ "$1" == "SG-005S_Live_Azure_4g" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/725Live/Live_Azure_4g.py > /home/admin/Logs/Logs725AzureHistory.txt &
	y=`echo $!`
	x="$x\n$y --SG-005S_Live_Azure_4g"
	sleep 10
fi

if [ "$1" == "KH-001S_Live" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python3 /home/admin/CODE/714Live/Live.py > /home/admin/Logs/Logs714History.txt &
	y=`echo $!`
	x="$x\n$y --KH-001S_Live"
	sleep 10
fi

if [ "$1" == "KH-008S_Live" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python3 /home/admin/CODE/728Live/Live.py > /home/admin/Logs/Logs728History.txt &
	y=`echo $!`
	x="$x\n$y --KH-008S_Live"
	sleep 10
fi

if [ "$1" == "KH-008S_Live_Azure_4g" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/728Live/Live_Azure_4g.py > /home/admin/Logs/Logs728AzureHistory.txt &
	y=`echo $!`
	x="$x\n$y --KH-008S_Live_Azure_4g"
	sleep 10
fi

if [ "$1" == "KH-003S_Live" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python3 /home/admin/CODE/715Live/Live.py > /home/admin/Logs/Logs715History.txt &
	y=`echo $!`
	x="$x\n$y --KH-003S_Live"
	sleep 10
fi

if [ "$1" == "KH-003S_Live_Azure_4g" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/715Live/Live_Azure_4g.py > /home/admin/Logs/Logs715AzureHistory.txt &
	y=`echo $!`
	x="$x\n$y --KH-003S_Live_Azure_4g"
	sleep 10
fi

if [ "$1" == "MY-004S_Live" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python3 /home/admin/CODE/720Live/Live.py > /home/admin/Logs/Logs720History.txt &
	y=`echo $!`
	x="$x\n$y --MY-004S_Live"
	sleep 10
fi
if [ "$1" == "MY-006S_Live" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python3 /home/admin/CODE/729Live/Live.py > /home/admin/Logs/Logs729History.txt &
	y=`echo $!`
	x="$x\n$y --MY-006S_Live"
	sleep 10
fi

if [ "$1" == "MY-006S_Live_Azure_4g" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/729Live/Live_Azure_4g.py > /home/admin/Logs/Logs729AzureHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-006S_Live_Azure_4g"
	sleep 10
fi

if [ "$1" == "IN-015S_Live" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python3 /home/admin/CODE/712Live/Live.py > /home/admin/Logs/Logs712History.txt &
	y=`echo $!`
	x="$x\n$y --IN-015S_Live"
	sleep 10
fi

if [ "$1" == "IN-015S_Live_Azure_4g" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/712Live/Live_Azure_4g.py > /home/admin/Logs/Logs712AzureHistory.txt &	
	y=`echo $!`
	x="$x\n$y --IN-015S_Live_Azure_4g"
	sleep 10
fi

if [ "$1" == "IN-036S_Live" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python3 /home/admin/CODE/718Live/Live.py > /home/admin/Logs/Logs718History.txt &
	y=`echo $!`
	x="$x\n$y --IN-036S_Live"
	sleep 10
fi

if [ "$1" == "IN-036S_Live_Azure_4g" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/718Live/Live_Azure_4g.py > /home/admin/Logs/Logs718AzureHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-036S_Live_Azure_4g"
	sleep 10
fi

if [ "$1" == "VN-001S_Live" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python3 /home/admin/CODE/722Live/Live.py > /home/admin/Logs/Logs722History.txt &
	y=`echo $!`
	x="$x\n$y --VN-001S_Live"
	sleep 10
fi


if [ "$1" == "SerisLifetimeMaster" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/Lifetime/SerisLifetimeMaster.py > /home/admin/Logs/LogsSerisLifetimeMaster.txt &
	y=`echo $!`
	x="$x\n$y --SerisLifetimeMaster"
	sleep 20
fi

if [ "$1" == "LocusLifetimeMaster" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/Lifetime/LocusLifetimeMaster.py > /home/admin/Logs/LogsLocusLifetimeMaster.txt &
	y=`echo $!`
	x="$x\n$y --LocusLifetimeMaster"
	sleep 20
fi

if [ "$1" == "FlexiLifetimeMaster" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/Lifetime/FlexiLifetimeMaster.py > /home/admin/Logs/LogsFlexiLifetimeMaster.txt &
	y=`echo $!`
	x="$x\n$y --FlexiLifetimeMaster"
	sleep 20
fi

if [ "$1" == "Lifetime_Gen2" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/Lifetime/Lifetime_Gen2.py > /home/admin/Logs/LogsLifetimeGen_2.txt &
	y=`echo $!`
	x="$x\n$y --Lifetime_Gen2"
	sleep 10
fi


if [ "$1" == "Web_Interface" ] || [ $RUNALL == 1 ] 
then
	nohup python3  /home/admin/CODE/WebInterface/Interface_V2.py > /home/admin/Logs/LogsWebInterface.txt 2>&1 &
	y=`echo $!`
	x="$x\n$y --Web_Interface"
	sleep 10
fi

#Added as a Cronjob
#if [ "$1" == "Invoice_View" ] || [ $RUNALL == 1 ] 
#then
#	stdbuf -oL python /home/admin/CODE/Invoicing/View.py > /home/admin/Logs/LogsInvoiceHistory.txt &
#	y=`echo $!`
#	x="$x\n$y --Invoice_View"
#	sleep 10
#fi

if [ "$1" == "IN050_COV_Alarm" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python3 /home/admin/CODE/Alarms/IN050_COV_Alarm.py > /home/admin/Logs/LogsIN050Alarm.txt &
	y=`echo $!`
	x="$x\n$y --IN050_COV_Alarm"
	sleep 10
fi

if [ "$1" == "KH008_Curtailment_Alarm" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/Alarms/KH008_Curtailment_Alarm.py > /home/admin/Logs/LogsKH008CurtailmentAlarm.txt &
	y=`echo $!`
	x="$x\n$y --KH008_Curtailment_Alarm"
	sleep 10
fi

if [ "$1" == "Tesco_Alarms" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python3 /home/admin/CODE/Tesco/tesco_alarms.py > /home/admin/Logs/LogsTescoAlarmsHistory.txt &
	y=`echo $!`
	x="$x\n$y --Tesco_Alarms"
	sleep 10
fi

if [ "$1" == "Tesco_Server_Push" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python3 /home/admin/CODE/Tesco/tesco_server_push.py > /home/admin/Logs/LogsTescoServerPushHistory.txt &
	y=`echo $!`
	x="$x\n$y --Tesco_Server_Push"
	sleep 10
fi

if [ "$1" == "IN-721S_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/721Digest/MailDigest721.R &
	y=`echo $!`
	x="$x\n$y --IN-721S_Mail"
	sleep 10
fi

if [ "$1" == "SG-724S_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SGDigest/SG724Digest/MailDigest724.R &
	y=`echo $!`
	x="$x\n$y --SG-724S_Mail"
	sleep 20 
fi

if [ "$1" == "IN-003W_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN003WDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-003W_Mail"
	sleep 10 
fi

if [ "$1" == "IN-014L_Mail" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Digest_Generic.py "Mail" "2020-06-10"> /home/admin/Logs/LogsIN014LMail.txt &
	y=`echo $!`
	x="$x\n$y --IN-014L_Mail"
	sleep 10 
fi

if [ "$1" == "IN-018L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN018LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-018L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-021L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN021LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-021L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-022L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN022LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-022L_Mail"
	sleep 20 
fi
gi
if [ "$1" == "IN-031L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN031LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-031L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-037L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN037LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-037L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-038L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN038LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-038L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-039L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN039LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-039L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-041L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN041LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-041L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-042L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN042LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-042L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-043L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN043LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-043L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-045L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN045LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-045L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-047L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN047LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-047L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-048L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN048LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-048L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-049L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN049LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-049L_Mail"
	sleep 20 
fi


if [ "$1" == "IN-051L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN051LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-051L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-052L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN052LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-052L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-053L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN053LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-053L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-055L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN055LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-055L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-056L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN056LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-056L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-059L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN059LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-059L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-060L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN060LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-060L_Mail"
	sleep 20 
fi


if [ "$1" == "IN-061L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN061LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-061L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-062L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN062LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-062L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-063L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN063LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-063L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-064L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN064LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-064L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-065L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN065LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-065L_Mail"
	sleep 20 
fi


if [ "$1" == "IN-066L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN066LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-066L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-067L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN067LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-067L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-068L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN068LDigest/Client_Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-068L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-068L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN068LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-068L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-069L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN069LDigest/Client_Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-069L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-069L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN069LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-069L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-071L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN071LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-071L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-072L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN072LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-072L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-073L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN073LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-073L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-074L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN074LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-074L_Mail"
	sleep 20 
fi


if [ "$1" == "IN-075L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN075LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-075L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-080L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN080LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-080L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-076L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN076LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-076L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-077L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN077LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-077L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-078L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN078LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-078L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-079L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN079LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-079L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-081L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN081LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-081L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-082L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN082LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-082L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-083L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN083LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-083L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-085L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN085LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-085L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-086L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN086LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-086L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-087L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN087LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-087L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-088L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN088LDigest/Client_Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-088L_Mail"
	sleep 20 
fi


if [ "$1" == "IN-089L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN089LDigest/Client_Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-089L_Mail"
	sleep 20 
fi

if [ "$1" == "MY-008L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/MY008LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --MY-008L_Mail"
fi

if [ "$1" == "MY-009L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/MY009LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --MY-009L_Mail"
fi

if [ "$1" == "SG-725S_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SGDigest/SG725Digest/MailDigest725.R &
	y=`echo $!`
	x="$x\n$y --SG-725S_Mail"
	sleep 20 
fi

if [ "$1" == "VN-002L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/VN002LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --VN-002L_Mail"
	sleep 20 
fi

if [ "$1" == "VN-003L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/VN003LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --VN-003L_Mail"
	sleep 20 
fi

if [ "$1" == "SG-726S_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SGDigest/SG726Digest/MailDigest726.R &
	y=`echo $!`
	x="$x\n$y --SG-726S_Mail"
	sleep 20 
fi


if [ "$1" == "SG-727S_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SGDigest/SG727Digest/MailDigest727.R &
	y=`echo $!`
	x="$x\n$y --SG-727S_Mail"
	sleep 20 
fi

if [ "$1" == "SG-005S_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SGDigest/SG005SDigest/MailDigest724.R &
	y=`echo $!`
	x="$x\n$y --SG-005S_Mail"
	sleep 10
fi

if [ "$1" == "VN-001S_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/VN001SDigest/MailDigest722.R &
	y=`echo $!`
	x="$x\n$y --VN-001S_Mail"
	sleep 10
fi

if [ "$1" == "VN-722S_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/VN722Digest/MailDigest722.R &
	y=`echo $!`
	x="$x\n$y --VN-722S_Mail"
	sleep 10
fi

if [ "$1" == "SG-999S_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SGDigest/Customer/MailDigest724.R &
	y=`echo $!`
	x="$x\n$y --SG-999S_Mail"
	sleep 10
fi

if [ "$1" == "MY-004S_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/MY004SDigest/MailDigest720.R &
	y=`echo $!`
	x="$x\n$y --MY-004S_Mail"
	sleep 10
fi

if [ "$1" == "MY-720S_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/MY720Digest/MailDigest720.R &
	y=`echo $!`
	x="$x\n$y --MY-720S_Mail"
	sleep 10
fi

if [ "$1" == "MY-006S_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/MY006SDigest/MailDigest729.R &
	y=`echo $!`
	x="$x\n$y --MY-006S_Mail"
	sleep 10
fi

if [ "$1" == "KH-008S_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/KH008SDigest/MailDigestKH008.R &
	y=`echo $!`
	x="$x\n$y --KH-008S_Mail"
	sleep 10
fi

if [ "$1" == "IN-008X_History" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN008Digest/LalruCleanHistory.R &
	y=`echo $!`
	x="$x\n$y --IN-008X_History"
	sleep 10
fi

if [ "$1" == "IN-010X_History" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN010Digest/DelhiCleanHistory.R &
	y=`echo $!`
	x="$x\n$y --IN-010X_History"
	sleep 10
fi

if [ "$1" == "SG-001X_History" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SGDigest/SG001Digest/SG001CleanHistory.R &
	y=`echo $!`
	x="$x\n$y --SG-001X_History"
	sleep 10
fi

if [ "$1" == "SG-002X_History" ] || [ $RUNALL == 1 ]
then
	Rscript /home/admin/CODE/SGDigest/SG002Digest/SG002CleanHistory.R &
	y=`echo $!`
	x="$x\n$y --SG-002X_History"
	sleep 10
fi

if [ "$1" == "SG-004X_History" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SGDigest/SG004XDigest/SG004CleanHistory.R &
	y=`echo $!`
	x="$x\n$y --SG-004X_History"
	sleep 10
fi

if [ "$1" == "SG-006X_History" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SGDigest/SG006XDigest/SG006CleanHistory.R &
	y=`echo $!`
	x="$x\n$y --SG-006X_History"
	sleep 10
fi


if [ "$1" == "SG-003EF_History" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SGDigest/SG003EDigest/Final/SG003EFCleanHistory.R &
	y=`echo $!`
	x="$x\n$y --SG-003EF_History"
	sleep 10
fi

if [ "$1" == "SG-003EP_History" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SGDigest/SG003EDigest/Prelim/SG003EPCleanHistory.R &
	y=`echo $!`
	x="$x\n$y --SG-003EP_History"
	sleep 10
fi

if [ "$1" == "SG-007EP_History" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SGDigest/SG007EDigest/Prelim/SG007EPCleanHistory.R &
	y=`echo $!`
	x="$x\n$y --SG-007EP_History"
	sleep 10
fi

if [ "$1" == "SG-007EF_History" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SGDigest/SG007EDigest/Final/SG007EFCleanHistory.R &
	y=`echo $!`
	x="$x\n$y --SG-007EF_History"
	sleep 10
fi

if [ "$1" == "SG-008EF_History" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SGDigest/SG008EDigest/Final/SG008EFCleanHistory.R &
	y=`echo $!`
	x="$x\n$y --SG-008EF_History"
	sleep 10
fi

if [ "$1" == "SG-008EP_History" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SGDigest/SG008EDigest/Prelim/SG008EPCleanHistory.R &
	y=`echo $!`
	x="$x\n$y --SG-008EP_History"
	sleep 10
fi

if [ "$1" == "KH-002X_History" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/KH002Digest/KH002CleanHistory.R &
	y=`echo $!`
	x="$x\n$y --KH-002X_History"
	sleep 10
fi

if [ "$1" == "KH-003X_History" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/KH003Digest/KH003CleanHistory.R &
	y=`echo $!`
	x="$x\n$y --KH-003X_History"
	sleep 10
fi

if [ "$1" == "KH-004X_History" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/KH004Digest/KH004CleanHistory.R &
	y=`echo $!`
	x="$x\n$y --KH-004X_History"
	sleep 10
fi

if [ "$1" == "KH-005X_History" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/KH005XDigest/KH005CleanHistory.R &
	y=`echo $!`
	x="$x\n$y --KH-005X_History"
	sleep 10
fi

if [ "$1" == "KH-006X_History" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/KH006XDigest/KH006CleanHistory.R &
	y=`echo $!`
	x="$x\n$y --KH-006X_History"
	sleep 10
fi

if [ "$1" == "KH-007X_History" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/KH007XDigest/KH007CleanHistory.R &
	y=`echo $!`
	x="$x\n$y --KH-007X_History"
	sleep 10
fi

if [ "$1" == "KH-008X_History" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/KH008Digest/KH008CleanHistory.R &
	y=`echo $!`
	x="$x\n$y --KH-008X_History"
	sleep 10
fi


if [ "$1" == "TH-001X_History" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/TH001XDigest/TH001CleanHistory.R &
	y=`echo $!`
	x="$x\n$y --TH-001X_History"
	sleep 10
fi


if [ "$1" == "TH-003X_History" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/TH003Digest/TH003CleanHistory.R &
	y=`echo $!`
	x="$x\n$y --TH-003X_History"
	sleep 10
fi

if [ "$1" == "MY-001X_History" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/MY001Digest/MY001CleanHistory.R &
	y=`echo $!`
	x="$x\n$y --MY-001X_History"
	sleep 10
fi

if [ "$1" == "MY-003X_History" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/MY003Digest/MY003CleanHistory.R &
	y=`echo $!`
	x="$x\n$y --MY-003X_History"
	sleep 10
fi

if [ "$1" == "VN-001X_History" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/VN001XDigest/VN001CleanHistory.R &
	y=`echo $!`
	x="$x\n$y --VN-001X_History"
	sleep 10
fi

if [ "$1" == "IN-004C_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN004CDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-004C_Mail"
	sleep 10
fi

if [ "$1" == "IN-006C_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN006CDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-006C_Mail"
	sleep 10
fi

if [ "$1" == "IN-009C_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN009CDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-009C_Mail"
	sleep 10
fi

if [ "$1" == "IN-011C_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN011CDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-011C_Mail"
	sleep 10
fi

if [ "$1" == "IN-020C_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN020CDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-020C_Mail"
	sleep 10
fi

if [ "$1" == "IN-023C_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN023Digest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-023C_Mail"
	sleep 10
fi

if [ "$1" == "IN-022C_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN022Digest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-022C_Mail"
	sleep 10
fi

if [ "$1" == "IN-025C_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN025Digest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-025C_Mail"
	sleep 10
fi

if [ "$1" == "IN-027C_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN027CDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-027C_Mail"
	sleep 10
fi

if [ "$1" == "IN-026C_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN026CDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-026C_Mail"
	sleep 10
fi

if [ "$1" == "IN-026C_Mail" ] || [ $RUNALL == 1 ]
then
	Rscript /home/admin/CODE/IN026CDigest/Mail_new.R &
	y=echo $!
	x="$x\n$y --IN-026C_Mail"
	sleep 10
fi

if [ "$1" == "IN-028C_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN028CDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-028C_Mail"
	sleep 10
fi

if [ "$1" == "IN-029C_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN029CDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-029C_Mail"
	sleep 10
fi

if [ "$1" == "TH-001C_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/TH001CDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --TH-001C_Mail"
	sleep 10
fi

if [ "$1" == "IN-032C_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN032CDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-032C_Mail"
	sleep 10
fi

if [ "$1" == "IN-033C_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN033CDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-033C_Mail"
	sleep 10
fi

if [ "$1" == "IN-034C_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN034CDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-034C_Mail"
	sleep 10
fi

if [ "$1" == "IN-035C_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN035Digest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-035C_Mail"
	sleep 10
fi

if [ "$1" == "IN-036C_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN036CDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-036C_Mail"
	sleep 10
fi


if [ "$1" == "IN-039C_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN039CDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-039C_Mail"
	sleep 10
fi

if [ "$1" == "IN-040C_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN040CDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-040C_Mail"
	sleep 10
fi

if [ "$1" == "IN-012C_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN012CDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-012C_Mail"
	sleep 10
fi

if [ "$1" == "IN-005C_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN005CDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-005C_Mail"
	sleep 10
fi

if [ "$1" == "IN-007C_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN007CDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-007C_Mail"
	sleep 10
fi

if [ "$1" == "IN-013C_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN013CDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-013C_Mail"
	sleep 10
fi

if [ "$1" == "IN-015C_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN015CDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-015C_Mail"
	sleep 10
fi

if [ "$1" == "IN-016C_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN016CDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-016C_Mail"
	sleep 10
fi

if [ "$1" == "IN-017C_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN017CDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-017C_Mail"
	sleep 10
fi

if [ "$1" == "IN-019C_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN019CDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-019C_Mail"
	sleep 10
fi

if [ "$1" == "IN-019C_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN019CDigest/Mail_new.R &
	y=`echo $!`
	x="$x\n$y --IN-019C_Mail"
	sleep 10
fi

if [ "$1" == "IN-030C_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN030Digest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-030C_Mail"
	sleep 10
fi

if [ "$1" == "IN-044C_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN044CDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-044C_Mail"
	sleep 10
fi

if [ "$1" == "IN-044C_Mail" ] || [ $RUNALL == 1 ]
then
	Rscript /home/admin/CODE/IN044CDigest/Mail_new.R &
	y=echo $!
	x="$x\n$y --IN-044C_Mail"
	sleep 10
fi

if [ "$1" == "IN-046C_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN046CDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-046C_Mail"
	sleep 10
fi

if [ "$1" == "IN-050C_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN050CDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-050C_Mail"
	sleep 10
fi

if [ "$1" == "IN-050C_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN050CDigest/Mail_new.R &
	y=`echo $!`
	x="$x\n$y --IN-050C_Mail"
	sleep 10
fi

if [ "$1" == "IN-054C_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN054CDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-054C_Mail"
	sleep 10
fi

if [ "$1" == "IN-057C_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN057CDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-057C_Mail"
	sleep 10
fi

if [ "$1" == "IN-070C_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN070CDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-070C_Mail"
	sleep 10
fi

if [ "$1" == "MY-002L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/MY002LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --MY-002L_Mail"
	sleep 10
fi

if [ "$1" == "TH-002L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/TH002LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --TH-002L_Mail"
	sleep 10
fi

if [ "$1" == "MY-002L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/MY002LDigest/Client_Mail.R &
	y=`echo $!`
	x="$x\n$y --MY-002L_Mail"
	sleep 10
fi

if [ "$1" == "IN-301L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN301LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-301L_Mail"
	sleep 10
fi

if [ "$1" == "IN-008X_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN008Digest/MailDigestLalru.R &
	y=`echo $!`
	x="$x\n$y --IN-008X_Mail"
	sleep 10
fi

if [ "$1" == "IN-010X_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN010Digest/MailDigestDelhi.R &
	y=`echo $!`
	x="$x\n$y --IN-010X_Mail"
	sleep 10
fi

if [ "$1" == "KH-001X_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/KH001Digest/MailDigestKH001.R &
	y=`echo $!`
	x="$x\n$y --KH-001X_Mail"
	sleep 10
fi

if [ "$1" == "KH-002X_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/KH002Digest/MailDigestKH002.R &
	y=`echo $!`
	x="$x\n$y --KH-002X_Mail"
	sleep 10
fi

if [ "$1" == "SG-001X_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SGDigest/SG001Digest/MailDigestSG001.R &
	y=`echo $!`
	x="$x\n$y --SG-001X_Mail"
	sleep 10
fi

if [ "$1" == "SG-002X_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SGDigest/SG002Digest/MailDigestSG002.R &
	y=`echo $!`
	x="$x\n$y --SG-002X_Mail"
	sleep 10
fi

if [ "$1" == "SG-003EP_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SGDigest/SG003EDigest/Prelim/MailDigestSG003EP.R &
	y=`echo $!`
	x="$x\n$y --SG-003EP_Mail"
	sleep 10
fi

if [ "$1" == "SG-003EF_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SGDigest/SG003EDigest/Final/MailDigestSG003EF.R &
	y=`echo $!`
	x="$x\n$y --SG-003EF_Mail"
	sleep 10
fi

if [ "$1" == "SG-007EP_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SGDigest/SG007EDigest/Prelim/MailDigestSG007EP.R &
	y=`echo $!`
	x="$x\n$y --SG-007EP_Mail"
	sleep 10
fi

if [ "$1" == "SG-007EF_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SGDigest/SG007EDigest/Final/MailDigestSG007EF.R &
	y=`echo $!`
	x="$x\n$y --SG-007EF_Mail"
	sleep 10
fi

if [ "$1" == "SG-008EP_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SGDigest/SG008EDigest/Prelim/MailDigestSG008EP.R &
	y=`echo $!`
	x="$x\n$y --SG-008EP_Mail"
	sleep 10
fi

if [ "$1" == "SG-008EF_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SGDigest/SG008EDigest/Final/MailDigestSG008EF.R &
	y=`echo $!`
	x="$x\n$y --SG-008EF_Mail"
	sleep 10
fi

if [ "$1" == "SG-004X_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SGDigest/SG004XDigest/MailDigestSG004.R &
	y=`echo $!`
	x="$x\n$y --SG-004X_Mail"
	sleep 10
fi

if [ "$1" == "SG-006X_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SGDigest/SG006XDigest/MailDigestSG006.R &
	y=`echo $!`
	x="$x\n$y --SG-006X_Mail"
	sleep 10
fi


if [ "$1" == "IN-018X_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN018Digest/MailDigestIN018.R &
	y=`echo $!`
	x="$x\n$y --IN-018X_Mail"
	sleep 10
fi

if [ "$1" == "SG-006_Lifetime" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/Lifetime/SG006Lifetime.py > /home/admin/Logs/LogsSG006LifetimeHistory.txt &
	y=`echo $!`
	x="$x\n$y --SG-006_Lifetime"
	sleep 10
fi

if [ "$1" == "KH-003X_Mail" ] || [ $RUNALL == 1 ]
then
	Rscript /home/admin/CODE/KH003Digest/MailDigestKH003.R &
	y=`echo $!`
	x="$x\n$y --KH-003X_Mail"
	sleep 10
fi

if [ "$1" == "KH-004X_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/KH004Digest/MailDigestKH004.R &
	y=`echo $!`
	x="$x\n$y --KH-004X_Mail"
	sleep 10
fi

if [ "$1" == "KH-005X_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/KH005XDigest/MailDigestKH005.R &
	y=`echo $!`
	x="$x\n$y --KH-005X_Mail"
	sleep 10
fi

if [ "$1" == "KH-006X_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/KH006XDigest/MailDigestKH006.R &
	y=`echo $!`
	x="$x\n$y --KH-006X_Mail"
	sleep 10
fi

if [ "$1" == "KH-007X_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/KH007XDigest/MailDigestKH007.R &
	y=`echo $!`
	x="$x\n$y --KH-007X_Mail"
	sleep 10
fi

if [ "$1" == "KH-008X_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/KH008Digest/MailDigestKH008.R &
	y=`echo $!`
	x="$x\n$y --KH-008X_Mail"
	sleep 10
fi

if [ "$1" == "TH-001X_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/TH001XDigest/MailDigestTH001.R &
	y=`echo $!`
	x="$x\n$y --TH-001X_Mail"
	sleep 10
fi

if [ "$1" == "TH-003X_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/TH003Digest/MailDigestTH003.R &
	y=`echo $!`
	x="$x\n$y --TH-003X_Mail"
	sleep 10
fi

if [ "$1" == "MY-001X_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/MY001Digest/MailDigestMY001.R &
	y=`echo $!`
	x="$x\n$y --MY-001X_Mail"
	sleep 10
fi

if [ "$1" == "MY-003X_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/MY003Digest/MailDigestMY003.R &
	y=`echo $!`
	x="$x\n$y --MY-003X_Mail"
	sleep 10
fi


if [ "$1" == "TH-004L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/TH004LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --TH-004L_Mail"
fi


if [ "$1" == "TH-005L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/TH005LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --TH-005L_Mail"
fi

if [ "$1" == "TH-007L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/TH007LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --TH-007L_Mail"
fi

if [ "$1" == "TH-010L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/TH010LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --TH-010L_Mail"
fi

if [ "$1" == "TH-011L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/TH011LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --TH-011L_Mail"
fi


if [ "$1" == "TH-012L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/TH012LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --TH-012L_Mail"
fi


if [ "$1" == "TH-013L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/TH013LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --TH-013L_Mail"
fi

if [ "$1" == "TH-014L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/TH014LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --TH-014L_Mail"
fi

if [ "$1" == "TH-015L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/TH015LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --TH-015L_Mail"
fi

if [ "$1" == "TH-016L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/TH016LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --TH-016L_Mail"
fi

if [ "$1" == "TH-017L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/TH017LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --TH-017L_Mail"
fi

if [ "$1" == "TH-018L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/TH018LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --TH-018L_Mail"
fi

if [ "$1" == "TH-019L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/TH019LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --TH-019L_Mail"
fi

if [ "$1" == "TH-020L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/TH020LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --TH-020L_Mail"
fi

if [ "$1" == "TH-021L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/TH021LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --TH-021L_Mail"
fi

if [ "$1" == "TH-022L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/TH022LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --TH-022L_Mail"
fi

if [ "$1" == "TH-023L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/TH023LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --TH-023L_Mail"
fi

if [ "$1" == "TH-024L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/TH024LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --TH-024L_Mail"
fi

if [ "$1" == "TH-025L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/TH025LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --TH-025L_Mail"
fi

if [ "$1" == "TH-026L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/TH026LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --TH-026L_Mail"
fi

if [ "$1" == "TH-027L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/TH027LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --TH-027L_Mail"
fi

if [ "$1" == "TH-028L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/TH028LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --TH-028L_Mail"
fi

if [ "$1" == "TH-046L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/TH046LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --TH-046L_Mail"
fi

if [ "$1" == "TH-047L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/TH047LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --TH-047L_Mail"
fi

if [ "$1" == "TH-048L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/TH048LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --TH-048L_Mail"
fi

if [ "$1" == "TH-049L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/TH049LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --TH-049L_Mail"
fi

if [ "$1" == "MY-005L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/MY005LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --MY-005L_Mail"
fi

if [ "$1" == "MY-007L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/MY007LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --MY-007L_Mail"
fi

if [ "$1" == "MY-010L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/MY010LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --MY-010L_Mail"
fi

if [ "$1" == "MY-401L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/MY401LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --MY-401L_Mail"
fi


if [ "$1" == "MY-402L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/MY402LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --MY-402L_Mail"
fi


if [ "$1" == "MY-403L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/MY403LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --MY-403L_Mail"
fi


if [ "$1" == "MY-404L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/MY404LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --MY-404L_Mail"
fi

if [ "$1" == "MY-405L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/MY405LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --MY-405L_Mail"
fi

if [ "$1" == "MY-406L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/MY406LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --MY-406L_Mail"
fi

if [ "$1" == "VN-001X_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/VN001XDigest/MailDigestVN001.R &
	y=`echo $!`
	x="$x\n$y --VN-001X_Mail"
	sleep 10
fi

if [ "$1" == "MasterMail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/MasterMail/MasterMail.R &
	y=`echo $!`
	x="$x\n$y --MasterMail"
	sleep 10
fi

if [ "$1" == "SERIS3G_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SERIS_3G/SERIS_3G.R &
	y=`echo $!`
	x="$x\n$y --SERIS3G"
	sleep 10
fi

if [ "$1" == "GraphMail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/EmailGraphs/sendGraphs.R &
	y=`echo $!`
	x="$x\n$y --GraphMail"
	sleep 10
fi

if [ "$1" == "PHMarketSite" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/AllSites/PH/PH_Digest.R &
	y=`echo $!`
	x="$x\n$y --PHMarketSite"
	sleep 10
fi

if [ "$1" == "SG-Central" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/Centralization/SG-Central.R &
	y=`echo $!`
	x="$x\n$y --SGCental"
	sleep 10
fi

fi ## RUNPYTHONONLY


if [ $RUNRONLY -eq 0 ]
then

if [ "$1" == "IN-004C_DBRaw" ] || [ $RUNALL == 1 ] 
then
	python3 /home/admin/CODE/DatabaseCreation/IN004C/IN-004C_raw_live.py > /home/admin/Logs/LogsIN004C_Raw.txt &
	y=`echo $!`
	x="$x\n$y --IN-004C_DBRaw"
	sleep 60
fi

if [ "$1" == "IN-005C_DBRaw" ] || [ $RUNALL == 1 ] 
then
	python3 /home/admin/CODE/DatabaseCreation/IN005C/IN-005C_raw_live.py > /home/admin/Logs/LogsIN005C_Raw.txt &
	y=`echo $!`
	x="$x\n$y --IN-005C_DBRaw"
	sleep 60
fi

if [ "$1" == "IN-006C_DBRaw" ] || [ $RUNALL == 1 ] 
then
	python3 /home/admin/CODE/DatabaseCreation/IN006C/IN-006C_raw_live.py > /home/admin/Logs/LogsIN006C_Raw.txt &
	y=`echo $!`
	x="$x\n$y --IN-006C_DBRaw"
	sleep 60
fi

if [ "$1" == "IN-007C_DBRaw" ] || [ $RUNALL == 1 ] 
then
	python3 /home/admin/CODE/DatabaseCreation/IN007C/IN-007C_raw_live.py > /home/admin/Logs/LogsIN007C_Raw.txt &
	y=`echo $!`
	x="$x\n$y --IN-007C_DBRaw"
	sleep 60
fi

if [ "$1" == "IN-009C_DBRaw" ] || [ $RUNALL == 1 ] 
then
	python3 /home/admin/CODE/DatabaseCreation/IN009C/IN-009C_raw_live.py > /home/admin/Logs/LogsIN009C_Raw.txt &
	y=`echo $!`
	x="$x\n$y --IN-009C_DBRaw"
	sleep 60
fi

if [ "$1" == "IN-011C_DBRaw" ] || [ $RUNALL == 1 ] 
then
	python3 /home/admin/CODE/DatabaseCreation/IN011C/IN-011C_raw_live.py > /home/admin/Logs/LogsIN011C_Raw.txt &
	y=`echo $!`
	x="$x\n$y --IN-011C_DBRaw"
	sleep 60
fi

if [ "$1" == "IN-012C_DBRaw" ] || [ $RUNALL == 1 ] 
then
	python3 /home/admin/CODE/DatabaseCreation/IN012C/IN-012C_raw_live.py > /home/admin/Logs/LogsIN012C_Raw.txt &
	y=`echo $!`
	x="$x\n$y --IN-012C_DBRaw"
	sleep 60
fi

if [ "$1" == "IN-013C_DBRaw" ] || [ $RUNALL == 1 ] 
then
	python3 /home/admin/CODE/DatabaseCreation/IN013C/IN-013C_raw_live.py > /home/admin/Logs/LogsIN013C_Raw.txt &
	y=`echo $!`
	x="$x\n$y --IN-013C_DBRaw"
	sleep 60
fi


if [ "$1" == "IN-015C_DBRaw" ] || [ $RUNALL == 1 ] 
then
	python3 /home/admin/CODE/DatabaseCreation/IN015C/IN-015C_raw_live.py > /home/admin/Logs/LogsIN015C_Raw.txt &
	y=`echo $!`
	x="$x\n$y --IN-015C_DBRaw"
	sleep 60
fi

if [ "$1" == "IN-016C_DBRaw" ] || [ $RUNALL == 1 ] 
then
	python3 /home/admin/CODE/DatabaseCreation/IN016C/IN-016C_raw_live.py > /home/admin/Logs/LogsIN016C_Raw.txt &
	y=`echo $!`
	x="$x\n$y --IN-016C_DBRaw"
	sleep 60
fi

if [ "$1" == "IN-017C_DBRaw" ] || [ $RUNALL == 1 ] 
then
	python3 /home/admin/CODE/DatabaseCreation/IN017C/IN-017C_raw_live.py > /home/admin/Logs/LogsIN017C_Raw.txt &
	y=`echo $!`
	x="$x\n$y --IN-017C_DBRaw"
	sleep 60
fi

if [ "$1" == "IN-019C_DBRaw" ] || [ $RUNALL == 1 ] 
then
	python3 /home/admin/CODE/DatabaseCreation/IN019C/IN-019C_raw_live.py > /home/admin/Logs/LogsIN019C_Raw.txt &
	y=`echo $!`
	x="$x\n$y --IN-019C_DBRaw"
	sleep 60
fi

if [ "$1" == "IN-020C_DBRaw" ] || [ $RUNALL == 1 ] 
then
	python3 /home/admin/CODE/DatabaseCreation/IN020C/IN-020C_raw_live.py > /home/admin/Logs/LogsIN020C_Raw.txt &
	y=`echo $!`
	x="$x\n$y --IN-020C_DBRaw"
	sleep 60
fi

if [ "$1" == "IN-023C_DBRaw" ] || [ $RUNALL == 1 ] 
then
	python3 /home/admin/CODE/DatabaseCreation/IN023C/IN-023C_raw_live.py > /home/admin/Logs/LogsIN023C_Raw.txt &
	y=`echo $!`
	x="$x\n$y --IN-023C_DBRaw"
	sleep 60
fi

if [ "$1" == "IN-025C_DBRaw" ] || [ $RUNALL == 1 ] 
then
	python3 /home/admin/CODE/DatabaseCreation/IN025C/IN-025C_raw_live.py > /home/admin/Logs/LogsIN025C_Raw.txt &
	y=`echo $!`
	x="$x\n$y --IN-025C_DBRaw"
	sleep 60
fi

if [ "$1" == "IN-026C_DBRaw" ] || [ $RUNALL == 1 ] 
then
	python3 /home/admin/CODE/DatabaseCreation/IN026C/IN-026C_raw_live.py > /home/admin/Logs/LogsIN026C_Raw.txt &
	y=`echo $!`
	x="$x\n$y --IN-026C_DBRaw"
	sleep 60
fi

if [ "$1" == "IN-027C_DBRaw" ] || [ $RUNALL == 1 ] 
then
	python3 /home/admin/CODE/DatabaseCreation/IN027C/IN-027C_raw_live.py > /home/admin/Logs/LogsIN027C_Raw.txt &
	y=`echo $!`
	x="$x\n$y --IN-027C_DBRaw"
	sleep 60
fi

if [ "$1" == "IN-028C_DBRaw" ] || [ $RUNALL == 1 ] 
then
	python3 /home/admin/CODE/DatabaseCreation/IN028C/IN-028C_raw_live.py > /home/admin/Logs/LogsIN028C_Raw.txt &
	y=`echo $!`
	x="$x\n$y --IN-028C_DBRaw"
	sleep 60
fi

if [ "$1" == "IN-029C_DBRaw" ] || [ $RUNALL == 1 ] 
then
	python3 /home/admin/CODE/DatabaseCreation/IN029C/IN-029C_raw_live.py > /home/admin/Logs/LogsIN029C_Raw.txt &
	y=`echo $!`
	x="$x\n$y --IN-029C_DBRaw"
	sleep 60
fi

if [ "$1" == "IN-030C_DBRaw" ] || [ $RUNALL == 1 ] 
then
	python3 /home/admin/CODE/DatabaseCreation/IN030C/IN-030C_raw_live.py > /home/admin/Logs/LogsIN030C_Raw.txt &
	y=`echo $!`
	x="$x\n$y --IN-030C_DBRaw"
	sleep 60
fi

if [ "$1" == "IN-032C_DBRaw" ] || [ $RUNALL == 1 ] 
then
	python3 /home/admin/CODE/DatabaseCreation/IN032C/IN-032C_raw_live.py > /home/admin/Logs/LogsIN032C_Raw.txt &
	y=`echo $!`
	x="$x\n$y --IN-032C_DBRaw"
	sleep 60
fi

if [ "$1" == "IN-033C_DBRaw" ] || [ $RUNALL == 1 ] 
then
	python3 /home/admin/CODE/DatabaseCreation/IN033C/IN-033C_raw_live.py > /home/admin/Logs/LogsIN033C_Raw.txt &
	y=`echo $!`
	x="$x\n$y --IN-033C_DBRaw"
	sleep 60
fi

if [ "$1" == "IN-034C_DBRaw" ] || [ $RUNALL == 1 ] 
then
	python3 /home/admin/CODE/DatabaseCreation/IN034C/IN-034C_raw_live.py > /home/admin/Logs/LogsIN034C_Raw.txt &
	y=`echo $!`
	x="$x\n$y --IN-034C_DBRaw"
	sleep 60
fi

if [ "$1" == "IN-035C_DBRaw" ] || [ $RUNALL == 1 ] 
then
	python3 /home/admin/CODE/DatabaseCreation/IN035C/IN-035C_raw_live.py > /home/admin/Logs/LogsIN035C_Raw.txt &
	y=`echo $!`
	x="$x\n$y --IN-035C_DBRaw"
	sleep 60
fi

if [ "$1" == "IN-036C_DBRaw" ] || [ $RUNALL == 1 ] 
then
	python3 /home/admin/CODE/DatabaseCreation/IN036C/IN-036C_raw_live.py > /home/admin/Logs/LogsIN036C_Raw.txt &
	y=`echo $!`
	x="$x\n$y --IN-036C_DBRaw"
	sleep 60
fi

if [ "$1" == "IN-037C_DBRaw" ] || [ $RUNALL == 1 ] 
then
	python3 /home/admin/CODE/DatabaseCreation/IN037C/IN-037C_raw_live.py > /home/admin/Logs/LogsIN037C_Raw.txt &
	y=`echo $!`
	x="$x\n$y --IN-037C_DBRaw"
	sleep 60
fi

if [ "$1" == "IN-039C_DBRaw" ] || [ $RUNALL == 1 ] 
then
	python3 /home/admin/CODE/DatabaseCreation/IN039C/IN-039C_raw_live.py > /home/admin/Logs/LogsIN039C_Raw.txt &
	y=`echo $!`
	x="$x\n$y --IN-039C_DBRaw"
	sleep 60
fi

if [ "$1" == "IN-040C_DBRaw" ] || [ $RUNALL == 1 ] 
then
	python3 /home/admin/CODE/DatabaseCreation/IN040C/IN-040C_raw_live.py > /home/admin/Logs/LogsIN040C_Raw.txt &
	y=`echo $!`
	x="$x\n$y --IN-040C_DBRaw"
	sleep 60
fi

if [ "$1" == "IN-041C_DBRaw" ] || [ $RUNALL == 1 ] 
then
	python3 /home/admin/CODE/DatabaseCreation/IN041C/IN-041C_raw_live.py > /home/admin/Logs/LogsIN041C_Raw.txt &
	y=`echo $!`
	x="$x\n$y --IN-041C_DBRaw"
	sleep 60
fi

if [ "$1" == "IN-044C_DBRaw" ] || [ $RUNALL == 1 ] 
then
	python3 /home/admin/CODE/DatabaseCreation/IN044C/IN-044C_raw_live.py > /home/admin/Logs/LogsIN044C_Raw.txt &
	y=`echo $!`
	x="$x\n$y --IN-044C_DBRaw"
	sleep 60
fi

if [ "$1" == "IN-046C_DBRaw" ] || [ $RUNALL == 1 ] 
then
	python3 /home/admin/CODE/DatabaseCreation/IN046C/IN-046C_raw_live.py > /home/admin/Logs/LogsIN046C_Raw.txt &
	y=`echo $!`
	x="$x\n$y --IN-046C_DBRaw"
	sleep 60
fi

if [ "$1" == "IN-050C1_DBRaw" ] || [ $RUNALL == 1 ] 
then
	python3 /home/admin/CODE/DatabaseCreation/IN050C/IN-050C1_raw_live.py > /home/admin/Logs/LogsIN050C1_Raw.txt &
	y=`echo $!`
	x="$x\n$y --IN-050C1_DBRaw"
	sleep 60
fi

if [ "$1" == "IN-050C2_DBRaw" ] || [ $RUNALL == 1 ] 
then
	python3 /home/admin/CODE/DatabaseCreation/IN050C/IN-050C2_raw_live.py > /home/admin/Logs/LogsIN050C2_Raw.txt &
	y=`echo $!`
	x="$x\n$y --IN-050C2_DBRaw"
	sleep 60
fi

if [ "$1" == "IN-050C3_DBRaw" ] || [ $RUNALL == 1 ] 
then
	python3 /home/admin/CODE/DatabaseCreation/IN050C/IN-050C3_raw_live.py > /home/admin/Logs/LogsIN050C3_Raw.txt &
	y=`echo $!`
	x="$x\n$y --IN-050C3_DBRaw"
	sleep 60
fi

if [ "$1" == "IN-050C4_DBRaw" ] || [ $RUNALL == 1 ] 
then
	python3 /home/admin/CODE/DatabaseCreation/IN050C/IN-050C4_raw_live.py > /home/admin/Logs/LogsIN050C4_Raw.txt &
	y=`echo $!`
	x="$x\n$y --IN-050C4_DBRaw"
	sleep 60
fi

if [ "$1" == "IN-050C5_DBRaw" ] || [ $RUNALL == 1 ] 
then
	python3 /home/admin/CODE/DatabaseCreation/IN050C/IN-050C5_raw_live.py > /home/admin/Logs/LogsIN050C5_Raw.txt &
	y=`echo $!`
	x="$x\n$y --IN-050C5_DBRaw"
	sleep 60
fi

if [ "$1" == "IN-050C6_DBRaw" ] || [ $RUNALL == 1 ] 
then
	python3 /home/admin/CODE/DatabaseCreation/IN050C/IN-050C6_raw_live.py > /home/admin/Logs/LogsIN050C6_Raw.txt &
	y=`echo $!`
	x="$x\n$y --IN-050C6_DBRaw"
	sleep 60
fi

if [ "$1" == "IN-050C7_DBRaw" ] || [ $RUNALL == 1 ] 
then
	python3 /home/admin/CODE/DatabaseCreation/IN050C/IN-050C7_raw_live.py > /home/admin/Logs/LogsIN050C7_Raw.txt &
	y=`echo $!`
	x="$x\n$y --IN-050C7_DBRaw"
	sleep 60
fi

if [ "$1" == "IN-050C8_DBRaw" ] || [ $RUNALL == 1 ] 
then
	python3 /home/admin/CODE/DatabaseCreation/IN050C/IN-050C8_raw_live.py > /home/admin/Logs/LogsIN050C8_Raw.txt &
	y=`echo $!`
	x="$x\n$y --IN-050C8_DBRaw"
	sleep 60
fi

if [ "$1" == "IN-050C9_DBRaw" ] || [ $RUNALL == 1 ] 
then
	python3 /home/admin/CODE/DatabaseCreation/IN050C/IN-050C9_raw_live.py > /home/admin/Logs/LogsIN050C9_Raw.txt &
	y=`echo $!`
	x="$x\n$y --IN-050C9_DBRaw"
	sleep 60
fi

if [ "$1" == "IN-050C10_DBRaw" ] || [ $RUNALL == 1 ] 
then
	python3 /home/admin/CODE/DatabaseCreation/IN050C/IN-050C10_raw_live.py > /home/admin/Logs/LogsIN050C10_Raw.txt &
	y=`echo $!`
	x="$x\n$y --IN-050C10_DBRaw"
	sleep 60
fi

if [ "$1" == "IN-050C11_DBRaw" ] || [ $RUNALL == 1 ] 
then
	python3 /home/admin/CODE/DatabaseCreation/IN050C/IN-050C11_raw_live.py > /home/admin/Logs/LogsIN050C11_Raw.txt &
	y=`echo $!`
	x="$x\n$y --IN-050C11_DBRaw"
	sleep 60
fi

if [ "$1" == "IN-054C_DBRaw" ] || [ $RUNALL == 1 ] 
then
	python3 /home/admin/CODE/DatabaseCreation/IN054C/IN-054C_raw_live.py > /home/admin/Logs/LogsIN054C_Raw.txt &
	y=`echo $!`
	x="$x\n$y --IN-054C_DBRaw"
	sleep 60
fi

if [ "$1" == "IN-057C_DBRaw" ] || [ $RUNALL == 1 ] 
then
	python3 /home/admin/CODE/DatabaseCreation/IN057C/IN-057C_raw_live.py > /home/admin/Logs/LogsIN057C_Raw.txt &
	y=`echo $!`
	x="$x\n$y --IN-057C_DBRaw"
	sleep 60
fi

if [ "$1" == "IN-070C_DBRaw" ] || [ $RUNALL == 1 ] 
then
	python3 /home/admin/CODE/DatabaseCreation/IN070C/IN-070C_raw_live.py > /home/admin/Logs/LogsIN070C_Raw.txt &
	y=`echo $!`
	x="$x\n$y --IN-070C_DBRaw"
	sleep 60
fi

if [ "$1" == "TH-001C_DBRaw" ] || [ $RUNALL == 1 ] 
then
	python3 /home/admin/CODE/DatabaseCreation/TH001C/TH-001C_raw_live.py > /home/admin/Logs/LogsTH001C_Raw.txt &
	y=`echo $!`
	x="$x\n$y --TH-001C_DBRaw"
	sleep 60
fi

if [ "$1" == "IN-004C_DBGen1" ] || [ $RUNALL == 1 ] 
then
	python3 /home/admin/CODE/DatabaseCreation/IN004C/IN-004C_gen1_live.py > /home/admin/Logs/LogsIN004C_Gen1.txt &
	y=`echo $!`
	x="$x\n$y --IN-004C_DBGen1"
	sleep 10
fi

if [ "$1" == "IN-005C_DBGen1" ] || [ $RUNALL == 1 ] 
then
	python3 /home/admin/CODE/DatabaseCreation/IN005C/IN-005C_gen1_live.py > /home/admin/Logs/LogsIN005C_Gen1.txt &
	y=`echo $!`
	x="$x\n$y --IN-005C_DBGen1"
	sleep 10
fi

if [ "$1" == "IN-006C_DBGen1" ] || [ $RUNALL == 1 ] 
then
	python3 /home/admin/CODE/DatabaseCreation/IN006C/IN-006C_gen1_live.py > /home/admin/Logs/LogsIN006C_Gen1.txt &
	y=`echo $!`
	x="$x\n$y --IN-006C_DBGen1"
	sleep 10
fi

if [ "$1" == "IN-007C_DBGen1" ] || [ $RUNALL == 1 ] 
then
	python3 /home/admin/CODE/DatabaseCreation/IN007C/IN-007C_gen1_live.py > /home/admin/Logs/LogsIN007C_Gen1.txt &
	y=`echo $!`
	x="$x\n$y --IN-007C_DBGen1"
	sleep 10
fi

if [ "$1" == "IN-009C_DBGen1" ] || [ $RUNALL == 1 ] 
then
	python3 /home/admin/CODE/DatabaseCreation/IN009C/IN-009C_gen1_live.py > /home/admin/Logs/LogsIN009C_Gen1.txt &
	y=`echo $!`
	x="$x\n$y --IN-009C_DBGen1"
	sleep 10
fi

if [ "$1" == "IN-011C_DBGen1" ] || [ $RUNALL == 1 ] 
then
	python3 /home/admin/CODE/DatabaseCreation/IN011C/IN-011C_gen1_live.py > /home/admin/Logs/LogsIN011C_Gen1.txt &
	y=`echo $!`
	x="$x\n$y --IN-011C_DBGen1"
	sleep 10
fi

if [ "$1" == "IN-012C_DBGen1" ] || [ $RUNALL == 1 ] 
then
	python3 /home/admin/CODE/DatabaseCreation/IN012C/IN-012C_gen1_live.py > /home/admin/Logs/LogsIN012C_Gen1.txt &
	y=`echo $!`
	x="$x\n$y --IN-012C_DBGen1"
	sleep 10
fi

if [ "$1" == "IN-013C_DBGen1" ] || [ $RUNALL == 1 ] 
then
	python3 /home/admin/CODE/DatabaseCreation/IN013C/IN-013C_gen1_live.py > /home/admin/Logs/LogsIN013C_Gen1.txt &
	y=`echo $!`
	x="$x\n$y --IN-013C_DBGen1"
	sleep 10
fi


if [ "$1" == "IN-015C_DBGen1" ] || [ $RUNALL == 1 ] 
then
	python3 /home/admin/CODE/DatabaseCreation/IN015C/IN-015C_gen1_live.py > /home/admin/Logs/LogsIN015C_Gen1.txt &
	y=`echo $!`
	x="$x\n$y --IN-015C_DBGen1"
	sleep 10
fi

if [ "$1" == "IN-016C_DBGen1" ] || [ $RUNALL == 1 ] 
then
	python3 /home/admin/CODE/DatabaseCreation/IN016C/IN-016C_gen1_live.py > /home/admin/Logs/LogsIN016C_Gen1.txt &
	y=`echo $!`
	x="$x\n$y --IN-016C_DBGen1"
	sleep 10
fi

if [ "$1" == "IN-017C_DBGen1" ] || [ $RUNALL == 1 ] 
then
	python3 /home/admin/CODE/DatabaseCreation/IN017C/IN-017C_gen1_live.py > /home/admin/Logs/LogsIN017C_Gen1.txt &
	y=`echo $!`
	x="$x\n$y --IN-017C_DBGen1"
	sleep 10
fi

if [ "$1" == "IN-019C_DBGen1" ] || [ $RUNALL == 1 ] 
then
	python3 /home/admin/CODE/DatabaseCreation/IN019C/IN-019C_gen1_live.py > /home/admin/Logs/LogsIN019C_Gen1.txt &
	y=`echo $!`
	x="$x\n$y --IN-019C_DBGen1"
	sleep 10
fi

if [ "$1" == "IN-020C_DBGen1" ] || [ $RUNALL == 1 ] 
then
	python3 /home/admin/CODE/DatabaseCreation/IN020C/IN-020C_gen1_live.py > /home/admin/Logs/LogsIN020C_Gen1.txt &
	y=`echo $!`
	x="$x\n$y --IN-020C_DBGen1"
	sleep 10
fi


if [ "$1" == "IN-023C_DBGen1" ] || [ $RUNALL == 1 ] 
then
	python3 /home/admin/CODE/DatabaseCreation/IN023C/IN-023C_gen1_live.py > /home/admin/Logs/LogsIN023C_Gen1.txt &
	y=`echo $!`
	x="$x\n$y --IN-023C_DBGen1"
	sleep 10
fi

if [ "$1" == "IN-025C_DBGen1" ] || [ $RUNALL == 1 ] 
then
	python3 /home/admin/CODE/DatabaseCreation/IN025C/IN-025C_gen1_live.py > /home/admin/Logs/LogsIN025C_Gen1.txt &
	y=`echo $!`
	x="$x\n$y --IN-025C_DBGen1"
	sleep 10
fi

if [ "$1" == "IN-026C_DBGen1" ] || [ $RUNALL == 1 ] 
then
	python3 /home/admin/CODE/DatabaseCreation/IN026C/IN-026C_gen1_live.py > /home/admin/Logs/LogsIN026C_Gen1.txt &
	y=`echo $!`
	x="$x\n$y --IN-026C_DBGen1"
	sleep 10
fi

if [ "$1" == "IN-027C_DBGen1" ] || [ $RUNALL == 1 ] 
then
	python3 /home/admin/CODE/DatabaseCreation/IN027C/IN-027C_gen1_live.py > /home/admin/Logs/LogsIN027C_Gen1.txt &
	y=`echo $!`
	x="$x\n$y --IN-027C_DBGen1"
	sleep 10
fi

if [ "$1" == "IN-028C_DBGen1" ] || [ $RUNALL == 1 ] 
then
	python3 /home/admin/CODE/DatabaseCreation/IN028C/IN-028C_gen1_live.py > /home/admin/Logs/LogsIN028C_Gen1.txt &
	y=`echo $!`
	x="$x\n$y --IN-028C_DBGen1"
	sleep 10
fi

if [ "$1" == "IN-029C_DBGen1" ] || [ $RUNALL == 1 ] 
then
	python3 /home/admin/CODE/DatabaseCreation/IN029C/IN-029C_gen1_live.py > /home/admin/Logs/LogsIN029C_Gen1.txt &
	y=`echo $!`
	x="$x\n$y --IN-029C_DBGen1"
	sleep 10
fi

if [ "$1" == "IN-030C_DBGen1" ] || [ $RUNALL == 1 ] 
then
	python3 /home/admin/CODE/DatabaseCreation/IN030C/IN-030C_gen1_live.py > /home/admin/Logs/LogsIN030C_Gen1.txt &
	y=`echo $!`
	x="$x\n$y --IN-030C_DBGen1"
	sleep 10
fi

if [ "$1" == "IN-032C_DBGen1" ] || [ $RUNALL == 1 ] 
then
	python3 /home/admin/CODE/DatabaseCreation/IN032C/IN-032C_gen1_live.py > /home/admin/Logs/LogsIN032C_Gen1.txt &
	y=`echo $!`
	x="$x\n$y --IN-032C_DBGen1"
	sleep 10
fi

if [ "$1" == "IN-033C_DBGen1" ] || [ $RUNALL == 1 ] 
then
	python3 /home/admin/CODE/DatabaseCreation/IN033C/IN-033C_gen1_live.py > /home/admin/Logs/LogsIN033C_Gen1.txt &
	y=`echo $!`
	x="$x\n$y --IN-033C_DBGen1"
	sleep 10
fi

if [ "$1" == "IN-034C_DBGen1" ] || [ $RUNALL == 1 ] 
then
	python3 /home/admin/CODE/DatabaseCreation/IN034C/IN-034C_gen1_live.py > /home/admin/Logs/LogsIN034C_Gen1.txt &
	y=`echo $!`
	x="$x\n$y --IN-034C_DBGen1"
	sleep 10
fi

if [ "$1" == "IN-035C_DBGen1" ] || [ $RUNALL == 1 ] 
then
	python3 /home/admin/CODE/DatabaseCreation/IN035C/IN-035C_gen1_live.py > /home/admin/Logs/LogsIN035C_Gen1.txt &
	y=`echo $!`
	x="$x\n$y --IN-035C_DBGen1"
	sleep 10
fi

if [ "$1" == "IN-036C_DBGen1" ] || [ $RUNALL == 1 ] 
then
	python3 /home/admin/CODE/DatabaseCreation/IN036C/IN-036C_gen1_live.py > /home/admin/Logs/LogsIN036C_Gen1.txt &
	y=`echo $!`
	x="$x\n$y --IN-036C_DBGen1"
	sleep 10
fi

if [ "$1" == "IN-037C_DBGen1" ] || [ $RUNALL == 1 ] 
then
	python3 /home/admin/CODE/DatabaseCreation/IN037C/IN-037C_gen1_live.py > /home/admin/Logs/LogsIN037C_Gen1.txt &
	y=`echo $!`
	x="$x\n$y --IN-037C_DBGen1"
	sleep 10
fi

if [ "$1" == "IN-039C_DBGen1" ] || [ $RUNALL == 1 ] 
then
	python3 /home/admin/CODE/DatabaseCreation/IN039C/IN-039C_gen1_live.py > /home/admin/Logs/LogsIN039C_Gen1.txt &
	y=`echo $!`
	x="$x\n$y --IN-039C_DBGen1"
	sleep 10
fi

if [ "$1" == "IN-040C_DBGen1" ] || [ $RUNALL == 1 ] 
then
	python3 /home/admin/CODE/DatabaseCreation/IN040C/IN-040C_gen1_live.py > /home/admin/Logs/LogsIN040C_Gen1.txt &
	y=`echo $!`
	x="$x\n$y --IN-040C_DBGen1"
	sleep 10
fi

if [ "$1" == "IN-041C_DBGen1" ] || [ $RUNALL == 1 ] 
then
	python3 /home/admin/CODE/DatabaseCreation/IN041C/IN-041C_gen1_live.py > /home/admin/Logs/LogsIN041C_Gen1.txt &
	y=`echo $!`
	x="$x\n$y --IN-041C_DBGen1"
	sleep 10
fi

if [ "$1" == "IN-044C_DBGen1" ] || [ $RUNALL == 1 ] 
then
	python3 /home/admin/CODE/DatabaseCreation/IN044C/IN-044C_gen1_live.py > /home/admin/Logs/LogsIN044C_Gen1.txt &
	y=`echo $!`
	x="$x\n$y --IN-044C_DBGen1"
	sleep 10
fi

if [ "$1" == "IN-046C_DBGen1" ] || [ $RUNALL == 1 ] 
then
	python3 /home/admin/CODE/DatabaseCreation/IN046C/IN-046C_gen1_live.py > /home/admin/Logs/LogsIN046C_Gen1.txt &
	y=`echo $!`
	x="$x\n$y --IN-046C_DBGen1"
	sleep 10
fi

if [ "$1" == "IN-050C1_DBGen1" ] || [ $RUNALL == 1 ] 
then
	python3 /home/admin/CODE/DatabaseCreation/IN050C/IN-050C1_gen1_live.py > /home/admin/Logs/LogsIN050C1_Gen1.txt &
	y=`echo $!`
	x="$x\n$y --IN-050C1_DBGen1"
	sleep 10
fi

if [ "$1" == "IN-050C2_DBGen1" ] || [ $RUNALL == 1 ] 
then
	python3 /home/admin/CODE/DatabaseCreation/IN050C/IN-050C2_gen1_live.py > /home/admin/Logs/LogsIN050C2_Gen1.txt &
	y=`echo $!`
	x="$x\n$y --IN-050C2_DBGen1"
	sleep 10
fi

if [ "$1" == "IN-050C3_DBGen1" ] || [ $RUNALL == 1 ] 
then
	python3 /home/admin/CODE/DatabaseCreation/IN050C/IN-050C3_gen1_live.py > /home/admin/Logs/LogsIN050C3_Gen1.txt &
	y=`echo $!`
	x="$x\n$y --IN-050C3_DBGen1"
	sleep 10
fi

if [ "$1" == "IN-050C4_DBGen1" ] || [ $RUNALL == 1 ] 
then
	python3 /home/admin/CODE/DatabaseCreation/IN050C/IN-050C4_gen1_live.py > /home/admin/Logs/LogsIN050C4_Gen1.txt &
	y=`echo $!`
	x="$x\n$y --IN-050C4_DBGen1"
	sleep 10
fi

if [ "$1" == "IN-050C5_DBGen1" ] || [ $RUNALL == 1 ] 
then
	python3 /home/admin/CODE/DatabaseCreation/IN050C/IN-050C5_gen1_live.py > /home/admin/Logs/LogsIN050C5_Gen1.txt &
	y=`echo $!`
	x="$x\n$y --IN-050C5_DBGen1"
	sleep 10
fi

if [ "$1" == "IN-050C6_DBGen1" ] || [ $RUNALL == 1 ] 
then
	python3 /home/admin/CODE/DatabaseCreation/IN050C/IN-050C6_gen1_live.py > /home/admin/Logs/LogsIN050C6_Gen1.txt &
	y=`echo $!`
	x="$x\n$y --IN-050C6_DBGen1"
	sleep 10
fi

if [ "$1" == "IN-050C7_DBGen1" ] || [ $RUNALL == 1 ] 
then
	python3 /home/admin/CODE/DatabaseCreation/IN050C/IN-050C7_gen1_live.py > /home/admin/Logs/LogsIN050C7_Gen1.txt &
	y=`echo $!`
	x="$x\n$y --IN-050C7_DBGen1"
	sleep 10
fi

if [ "$1" == "IN-050C8_DBGen1" ] || [ $RUNALL == 1 ] 
then
	python3 /home/admin/CODE/DatabaseCreation/IN050C/IN-050C8_gen1_live.py > /home/admin/Logs/LogsIN050C8_Gen1.txt &
	y=`echo $!`
	x="$x\n$y --IN-050C8_DBGen1"
	sleep 10
fi

if [ "$1" == "IN-050C9_DBGen1" ] || [ $RUNALL == 1 ] 
then
	python3 /home/admin/CODE/DatabaseCreation/IN050C/IN-050C9_gen1_live.py > /home/admin/Logs/LogsIN050C9_Gen1.txt &
	y=`echo $!`
	x="$x\n$y --IN-050C9_DBGen1"
	sleep 10
fi

if [ "$1" == "IN-050C10_DBGen1" ] || [ $RUNALL == 1 ] 
then
	python3 /home/admin/CODE/DatabaseCreation/IN050C/IN-050C10_gen1_live.py > /home/admin/Logs/LogsIN050C10_Gen1.txt &
	y=`echo $!`
	x="$x\n$y --IN-050C10_DBGen1"
	sleep 10
fi

if [ "$1" == "IN-050C11_DBGen1" ] || [ $RUNALL == 1 ] 
then
	python3 /home/admin/CODE/DatabaseCreation/IN050C/IN-050C11_gen1_live.py > /home/admin/Logs/LogsIN050C11_Gen1.txt &
	y=`echo $!`
	x="$x\n$y --IN-050C11_DBGen1"
	sleep 10
fi

if [ "$1" == "IN-054C_DBGen1" ] || [ $RUNALL == 1 ] 
then
	python3 /home/admin/CODE/DatabaseCreation/IN054C/IN-054C_gen1_live.py > /home/admin/Logs/LogsIN054C_Gen1.txt &
	y=`echo $!`
	x="$x\n$y --IN-054C_DBGen1"
	sleep 10
fi

if [ "$1" == "IN-057C_DBGen1" ] || [ $RUNALL == 1 ] 
then
	python3 /home/admin/CODE/DatabaseCreation/IN057C/IN-057C_gen1_live.py > /home/admin/Logs/LogsIN057C_Gen1.txt &
	y=`echo $!`
	x="$x\n$y --IN-057C_DBGen1"
	sleep 10
fi

if [ "$1" == "IN-070C_DBGen1" ] || [ $RUNALL == 1 ] 
then
	python3 /home/admin/CODE/DatabaseCreation/IN070C/IN-070C_gen1_live.py > /home/admin/Logs/LogsIN070C_Gen1.txt &
	y=`echo $!`
	x="$x\n$y --IN-070C_DBGen1"
	sleep 10
fi

#if [ "$1" == "IN070_Powertrip_Alarm" ] || [ $RUNALL == 1 ] 
#then
#	stdbuf -oL python /home/admin/CODE/Alarms/IN070_Powertrip_Alarm.py > /home/admin/Logs/LogsIN070Alarm.txt &
#	y=`echo $!`
#	x="$x\n$y --IN070_Powertrip_Alarm"
#	sleep 10
#fi

if [ "$1" == "TH-001C_DBGen1" ] || [ $RUNALL == 1 ] 
then
	python3 /home/admin/CODE/DatabaseCreation/TH001C/TH-001C_gen1_live.py > /home/admin/Logs/LogsTH001C_Gen1.txt &
	y=`echo $!`
	x="$x\n$y --TH-001C_DBGen1"
	sleep 10
fi

if [ "$1" == "IN-023C_AzureAll" ] || [ $RUNALL == 1 ] 
then
	python2 /home/admin/CODE/DatabaseCreation/IN023C/IN-023C_azure_all.py > /home/admin/Logs/LogsIN023C_Azure_All.txt &
	y=`echo $!`
	x="$x\n$y --IN-023C_AzureAll"
	sleep 10
fi


if [ "$1" == "MY-001X_Azure4G" ] || [ $RUNALL == 1 ] 
then
	python2 /home/admin/CODE/DatabaseCreation/MY001X/MY-001X_azure_4g.py > /home/admin/Logs/LogsMY001X_Azure_4G.txt &
	y=`echo $!`
	x="$x\n$y --MY-001X_Azure4G"
	sleep 10
fi

if [ "$1" == "MY-003X_Azure4G" ] || [ $RUNALL == 1 ] 
then
	python2 /home/admin/CODE/DatabaseCreation/MY003X/MY-003X_azure_4g.py > /home/admin/Logs/LogsMY003X_Azure_4G.txt &
	y=`echo $!`
	x="$x\n$y --MY-003X_Azure4G"
	sleep 10
fi

if [ "$1" == "SG-003E_AzureAll" ] || [ $RUNALL == 1 ] 
then
	python2 /home/admin/CODE/DatabaseCreation/SG003E/SG-003E_azure_all.py > /home/admin/Logs/LogsSG003E_Azure_All.txt &
	y=`echo $!`
	x="$x\n$y --SG-003E_AzureAll"
	sleep 10
fi

if [ "$1" == "SG-007E_AzureAll" ] || [ $RUNALL == 1 ] 
then
	python2 /home/admin/CODE/DatabaseCreation/SG007E/SG-007E_azure_all.py > /home/admin/Logs/LogsSG007E_Azure_All.txt &
	y=`echo $!`
	x="$x\n$y --SG-007E_AzureAll"
	sleep 10
fi

if [ "$1" == "GIS_MASTER_ALL" ] || [ $RUNALL == 1 ] 
then
	python2 /home/admin/CODE/DatabaseCreation/GIS_Master_All/gis_master_all.py > /home/admin/Logs/LogsGISMasterAll.txt &	
	y=`echo $!`
	x="$x\n$y --GIS_MASTER_ALL"
	sleep 10
fi

if [ "$1" == "GIS_SUM_AHME" ] || [ $RUNALL == 1 ] 
then
	python2 /home/admin/CODE/DatabaseCreation/GIS_SUM/gis_sum_ahmedabad.py > /home/admin/Logs/LogsGIS_sum_ahme.txt &
	y=`echo $!`
	x="$x\n$y --GIS_SUM_AHME"
	sleep 10
fi

if [ "$1" == "GIS_SUM_NEWD" ] || [ $RUNALL == 1 ] 
then
	python2 /home/admin/CODE/DatabaseCreation/GIS_SUM/gis_sum_newd.py > /home/admin/Logs/LogsGIS_sum_newd.txt &
	y=`echo $!`
	x="$x\n$y --GIS_SUM_NEWD"
	sleep 10
fi

if [ "$1" == "Station_All_Alarm" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python3 /home/admin/CODE/Alarms/Station_All_Alarm.py > /home/admin/Logs/LogsStationAllAlarm.txt &
	y=`echo $!`
	x="$x\n$y --Station_All_Alarm"
	sleep 10
fi

if [ "$1" == "KH003_Merge_Live" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/Azure/KH003_Merge_Live.py > /home/admin/Logs/LogsKH003MergeLive.txt &
	y=`echo $!`
	x="$x\n$y --KH003_Merge_Live"
	sleep 10
fi

if [ "$1" == "Seris_Alarm" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/Alarms/Seris_Alarm.py > /home/admin/Logs/LogsSeris_Alarm.txt &
	y=`echo $!`
	x="$x\n$y --Seris_Alarm"
	sleep 10
fi

if [ "$1" == "Bot_Status" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/Alarms/Bot_Status.py > /home/admin/Logs/LogsBotStatus.txt &
	y=`echo $!`
	x="$x\n$y --Bot_Status"
	sleep 10
fi

fi # RunRONLY

if [ $RUNPYTHONONLY -eq 0 ] 
then
if [ "$1" == "FaultAlert" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/Misc/FaultAlert.R &
	y=`echo $!`
	x="$x\n$y --FaultAlert"
	sleep 10
fi

if [ "$1" == "MemoryProfile" ] || [ $RUNALL == 1 ] 
then
	sh /home/admin/Usage.sh &
	sleep 1
fi
fi #RUNPYTHONONLY

if [ $# == 0 ] 
then 
	echo -e "$x" > "/home/admin/psIds.txt"
else 
	y1="$(echo -e "${x}" | sed -e ':a;N;$!ba;s/\n/ /g')"
	y2="$(echo -e "${y1}" | sed -e 's/^[[:space:]]*//')"
	sed -i "/$1/c$y2" "/home/admin/psIds.txt"
	#echo -e "$x" >> "/home/admin/psIds.txt"
fi

