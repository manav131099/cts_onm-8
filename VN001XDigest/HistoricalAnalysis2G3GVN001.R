system('rm -R "/home/admin/Dropbox/Second Gen/[VN-001X]"')
system('rm -R "/home/admin/Dropbox/Third Gen/[VN-001X]"')

require('mailR')
require('Hmisc')

source('/home/admin/CODE/VN001XDigest/VN001MailDigestFunctions.R')
source('/home/admin/CODE/Misc/calcBackLog.R')
path = '/home/admin/Dropbox/Gen 1 Data/[VN-001X]'
writepath2G = '/home/admin/Dropbox/Second Gen/[VN-001X]'
checkdir(writepath2G)

writepath3G = '/home/admin/Dropbox/Third Gen/[VN-001X]'
checkdir(writepath3G)
DAYSALIVE = 0
years = dir(path)
stnnickName2 = "VN-001X"
lastdatemail = lastMailDate(paste('/home/admin/Start/MasterMail/',stnnickName2,'_Mail.txt',sep=""))
stopDate = paste("[VN-001X] ",lastdatemail,".txt",sep="")
ENDCALL=0

for(x in 1 : length(years))
{
  pathyrs = paste(path,years[x],sep="/")
  writepath2Gyr = paste(writepath2G,years[x],sep="/")
  checkdir(writepath2Gyr)
  writepath3Gyr = paste(writepath3G,years[x],sep="/")
  checkdir(writepath3Gyr)
  months = dir(pathyrs)
  for(y in 1 : length(months))
  {
    pathmonths = paste(pathyrs,months[y],sep="/")
    writepath2Gmon = paste(writepath2Gyr,months[y],sep="/")
    checkdir(writepath2Gmon)
    writepath3Gfinal = paste(writepath3Gyr,"/[VN-001X] ",months[y],".txt",sep="")
    pathdays = pathmonths
    writepath2Gdays = writepath2Gmon
    checkdir(writepath2Gdays)
    days = dir(pathdays)
    temp = unlist(strsplit(days[1]," "))
    temp = as.Date(substr(temp[2],1,10),"%Y-%m-%d")
     for(t in 1 : length(days))
      {
        if(ENDCALL==1)
					break

				{
          if(x == 1 && y == 1 && t == 1)
          {
            DOB = unlist(strsplit(days[t]," "))
            DOB = substr(DOB[2],1,10)
            DOB = as.Date(DOB,"%Y-%m-%d")
          }
          else if(x == length(years) && y == length(months) && t == length(days))
          {
            next
          }
        }
        DAYSALIVE = DAYSALIVE + 1
        writepath2Gfinal = paste(writepath2Gdays,"/",days[t],sep="")
        readpath = paste(pathdays,days[t],sep="/")
				TOTSOLAR <<- 0
				METERCALLED <<- 1 #correct dont change
        df1 = secondGenData(readpath,writepath2Gfinal)
				print(days[t])
        thirdGenData(writepath2Gfinal,writepath3Gfinal)
				print(writepath3Gfinal)
				if(days[t] == stopDate)
				{
					ENDCALL = 1
				}
      }
			if(ENDCALL == 1)
				break
    }
			if(ENDCALL == 1)
				break
}
print('Historical Analysis Done')
