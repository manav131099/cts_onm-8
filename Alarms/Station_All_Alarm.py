import requests, json
import requests.auth
import pandas as pd
import datetime
import os
import re 
import time
import shutil
import pytz
import math
import sys
import numpy as np
import pyodbc
import pymsteams
import logging

tz = pytz.timezone('Asia/Calcutta')

server = 'cleantechsolar.database.windows.net'
database = 'Cleantech Meter Readings'
username = 'RohanKN'
password = 'R@h@nKN1'
driver= '{ODBC Driver 13 for SQL Server}'
path='/home/admin/Dropbox/Second Gen/'
#logging.basicConfig(filename='/home/pranav/Station_Alerts.txt')
date=str(datetime.datetime.now(tz).date()+datetime.timedelta(days=-1))

def create_template(date,stn,energy='NULL',yld='NULL',pr='NULL',ga='NULL',pa='NULL',cov_mfm='NULL',cov_inv='NULL',pr_alarm='NULL',mfm_cov_alarm='NULL',inv_cov_alarm='NULL',status='NULL',ghi='NULL',da='NULL'):
    df_template = pd.DataFrame({'Date':[date],'O&M_Code':[stn],'Energy':[energy],'Yield':[yld],'GHI':[ghi],'PR':[pr],'DA':[da],'GA':[ga],'PA':[pa],'CoV_MFM':[cov_mfm],'CoV_INV':[cov_inv],'PR_Alarm':[pr_alarm],'MFM_CoV_Alarm':[mfm_cov_alarm],'INV_CoV_Alarm':[inv_cov_alarm],'Status':[status]},columns =['Date','O&M_Code','Energy','Yield','GHI','PR','DA','GA','PA','CoV_MFM','CoV_INV','PR_Alarm','MFM_CoV_Alarm','INV_CoV_Alarm','Status'])
    return df_template

#MY-010,TH-047,TH-049
#IN-024
#SG-006
connStr = pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+ password)
SQL_Query = pd.read_sql_query(
'''  SELECT  * FROM [dbo].[Station_Limits] ''', connStr)
df = pd.DataFrame(SQL_Query, columns=['O&M_Code','Provider','MFM_limit','INV_limit','PR_limit','Webhook'])

connStr.close()
df_locus=df.loc[df['Provider'].str.strip()=='Locus',['MFM_limit','INV_limit','PR_limit','Webhook']]
df_flexi=df.loc[df['Provider'].str.strip()=='Flexi',['MFM_limit','INV_limit','PR_limit','Webhook']]
df_seris=df.loc[df['Provider'].str.strip()=='Seris',['MFM_limit','INV_limit','PR_limit','Webhook']]
df_ebx=df.loc[df['Provider'].str.strip()=='Ebx',['MFM_limit','INV_limit','PR_limit','Webhook']]

seris_stns=['IN-015', 'KH-001', 'KH-003', 'KH-008', 'MY-004', 'MY-006', 'SG-003', 'SG-004', 'SG-005', 'VN-001']
seris_capacities=[[1814.4], [63.0], [2560.0], [495.95, 2249.98, 2854.8, 1397.5, 2835.32], [414.64], [1615.6, 1579.5, 858.2], [839.52, 782.28, 165.36], [384.0, 460.8], [794.97, 1042.8, 819.0, 410.8, 486.0], [422.5]]
seris_mfm_limit=df_seris['MFM_limit'].values.tolist()
seris_inv_limit=df_seris['INV_limit'].values.tolist()
seris_pr_limit=df_seris['PR_limit'].values.tolist() 
seris_w=df_seris['Webhook'].str.strip().values.tolist()
print(seris_w)

#i=='IN-084' or i=='IN-085'or i=='IN-087' or i=='KH-009' or i=='IN-077' or i=='IN-078' or i=='IN-079'
#locus_stns=['IN-018', 'IN-021', 'IN-031', 'IN-038', 'IN-041', 'IN-042', 'IN-043', 'IN-045', 'IN-047', 'IN-048', 'IN-049', 'IN-051', 'IN-052', 'IN-053', 'IN-055', 'IN-056', 'IN-058', 'IN-059', 'IN-060', 'IN-061', 'IN-062', 'IN-063', 'IN-064', 'IN-065', 'IN-066', 'IN-067', 'IN-068', 'IN-069', 'IN-071', 'IN-072', 'IN-073', 'IN-074', 'IN-075', 'IN-076', 'IN-077', 'IN-078', 'IN-079', 'IN-080', 'IN-081', 'IN-082', 'IN-083', 'IN-084', 'IN-085', 'IN-087', 'KH-009', 'MY-007', 'MY-008', 'MY-009', 'MY-401', 'MY-402', 'MY-403', 'MY-404', 'MY-405', 'MY-406', 'SG-007', 'SG-008', 'TH-004', 'TH-005', 'TH-007', 'TH-010', 'TH-011', 'TH-012', 'TH-013', 'TH-014', 'TH-015', 'TH-016', 'TH-017', 'TH-018', 'TH-019', 'TH-020', 'TH-021', 'TH-022', 'TH-023', 'TH-024', 'TH-025', 'TH-026', 'TH-027', 'TH-028', 'VN-002', 'VN-003']
#locus_capacities=[[100.8], [910.0], [318.0, 445.0], [547.2], [130.0, 760.5], [345.0], [448.0], [133.92], [180.01], [41.54, 120.9, 449.5], [213.84], [370.5, 325.0, 302.58], [505.6, 241.92], [8615.0], [377.0], [446.16, 503.25], [224.4, 43.56, 66.0], [476.52], [650.0, 390.0], [337.025], [442.2, 501.6, 264.0, 442.2], [756.36], [66.99, 127.71, 66.0, 403.59, 515.79], [4745.4], [834.6], [374.22], [995.15], [1203.84, 8534.0, 4098.6], [522.7], [203.4], [650.1], [65.0, 65.0], [552.5], [1254.0], [451.44, 451.44], [451.44, 376.2], [97.5, 59.8], [1214.4], [409.2], [811.53], [92.365, 627.0], [1632.3], [447.42], [1001.0], [356.4], [345.8], [396.5, 1417.0], [913.56], [70.875], [70.875], [58.32], [70.875], [64.8], [70.875], [167.7], [646.1], [310.2], [916.5], [993.6], [994.5], [546.0], [994.5], [344.5], [994.5], [994.5], [702.0], [923.0], [994.5], [916.5], [994.5], [923.0], [994.5], [624.0], [650.0, 214.5], [546.0], [793.0], [624.0], [546.0], [546.0], [546.0]]
locus_stns=['IN-018', 'IN-021', 'IN-031', 'IN-038', 'IN-041', 'IN-042', 'IN-043', 'IN-045', 'IN-047', 'IN-048', 'IN-049', 'IN-051', 'IN-052', 'IN-053', 'IN-055', 'IN-056', 'IN-058', 'IN-059', 'IN-060', 'IN-061', 'IN-062', 'IN-063', 'IN-064', 'IN-065', 'IN-066', 'IN-067', 'IN-068', 'IN-069', 'IN-071', 'IN-072', 'IN-073', 'IN-074', 'IN-075', 'IN-076', 'IN-078','IN-080', 'IN-081', 'IN-082', 'IN-083','IN-086','IN-087','IN-088','IN-089','IN-301','KH-009','MY-002','MY-007', 'MY-008', 'MY-009', 'MY-401', 'MY-402', 'MY-403', 'MY-404', 'MY-405', 'MY-406', 'SG-007', 'SG-008', 'TH-004', 'TH-005', 'TH-007', 'TH-010', 'TH-011', 'TH-012', 'TH-013', 'TH-014', 'TH-015', 'TH-016', 'TH-017', 'TH-018', 'TH-019', 'TH-020', 'TH-021', 'TH-022', 'TH-023', 'TH-024', 'TH-025', 'TH-026', 'TH-027', 'TH-028', 'VN-002', 'VN-003']
locus_capacities=[[100.8], [910.0], [318.0, 445.0], [547.2], [130.0, 760.5], [345.0], [448.0], [133.92], [180.01], [41.54, 120.9, 449.5], [213.84], [370.5, 325.0, 302.58], [505.6, 241.92], [8615.0], [377.0], [446.16, 503.25], [224.4, 43.56, 66.0], [476.52], [650.0, 390.0], [337.025], [442.2, 501.6, 264.0, 442.2], [756.36], [66.99, 127.71, 66.0, 403.59, 515.79], [4745.4], [834.6], [374.22], [995.15], [1203.84, 8534.0, 4098.6], [522.7], [203.4], [650.1], [65.0, 65.0], [552.5], [1254.0], [451.44,376.2] ,[1214.4], [409.2], [811.53], [92.365, 627.0],[258.06] ,[1001],[322.08],[60.06, 60.06, 60.06, 120.12, 149.82],[31501.27],[356.4],[316.8, 103.95],[345.8],[396.5, 1417.0], [913.56], [70.875], [70.875], [58.32], [70.875], [64.8], [70.875], [167.7], [646.1], [310.2], [916.5], [993.6], [994.5], [546.0], [994.5], [344.5], [994.5], [994.5], [702.0], [923.0], [994.5], [916.5], [994.5], [923.0], [994.5], [624.0], [650.0, 214.5], [546.0], [793.0], [624.0], [546.0], [546.0], [546.0]]
locus_mfm_limit=df_locus['MFM_limit'].values.tolist()
locus_inv_limit=df_locus['INV_limit'].values.tolist()
locus_pr_limit=df_locus['PR_limit'].values.tolist()
locus_w=df_locus['Webhook'].str.strip().values.tolist()
print(len(locus_stns))
print(len(locus_capacities))
print(len(locus_mfm_limit))
print(len(locus_inv_limit))
print(len(locus_pr_limit))
print(len(locus_w))


#IN-050 [806.4, 292.5, 282.1, 299.0, 156.0, 448.5, 172.8, 672.0, 345.6, 998.4, 780.8, 1024.0, 1003.2, 889.6, 513.5, 1105.0, 598.0, 454.4, 245.2, 455.0, 312.0, 132.0, 332.8, 633.6, 1256.0, 754.0]
#flexi_stns=['IN-004', 'IN-005', 'IN-006', 'IN-007', 'IN-009', 'IN-011', 'IN-012', 'IN-013', 'IN-014', 'IN-016', 'IN-017', 'IN-019', 'IN-020', 'IN-022', 'IN-023', 'IN-025', 'IN-026', 'IN-027', 'IN-028', 'IN-029', 'IN-030', 'IN-032', 'IN-033', 'IN-034', 'IN-035', 'IN-037', 'IN-039', 'IN-040', 'IN-044', 'IN-046', 'IN-050', 'IN-054', 'IN-057', 'IN-070', 'MY-002', 'TH-001']
#flexi_capacities=[[187.5, 109.75], [218.4], [93.94, 57.645, 29.89, 87.84, 36.6, 180.56], [150.0], [128.52], [156.0, 218.4, 374.4], [283.5, 283.5, 532.98], [252.0, 220.5, 157.5, 126.0, 64.26], [226.8, 378.0, 453.6], [340.2, 359.1, 66.15, 66.15], [239.4], [750.48], [1004.88], [614.4, 614.4, 384.0, 307.2], [1144.0], [1215.5], [364.8, 313.6, 291.2, 142.72, 268.16], [346.56], [281.6], [200.0], [474.5], [652.8], [44.8, 185.6], [230.4], [3510.0], [305.92, 110.72], [280.8], [365.0], [102.4, 102.4], [192.0, 89.6], [806.4, 292.5, 282.1, 299.0, 156.0, 448.5, 172.8, 672.0, 345.6, 998.4, 780.8, 1024.0, 1003.2, 889.6, 513.5, 1105.0, 598.0, 454.4, 245.2, 455.0, 312.0, 132.0, 332.8, 633.6, 1256.0, 754.0], [508.2], [119.79, 154.44], [3920.0, 1904.5, 3593.0], [316.8, 103.95], [1404.0, 1404.0]]
flexi_stns=['IN-004', 'IN-005', 'IN-007', 'IN-009', 'IN-011', 'IN-012', 'IN-013', 'IN-014', 'IN-016', 'IN-017', 'IN-019', 'IN-020', 'IN-022', 'IN-023', 'IN-025', 'IN-026', 'IN-027', 'IN-028', 'IN-029', 'IN-030', 'IN-032', 'IN-033', 'IN-034', 'IN-035', 'IN-036','IN-037', 'IN-040', 'IN-044', 'IN-046', 'IN-054', 'IN-057', 'IN-070', 'TH-001']
flexi_capacities=[[187.5, 109.75], [218.4], [150.0], [128.52], [156.0, 218.4, 374.4], [283.5, 283.5, 532.98], [252.0, 220.5, 157.5, 126.0, 64.26], [226.8, 378.0, 453.6], [340.2, 359.1, 66.15, 66.15], [239.4], [750.48], [1004.88], [614.4, 614.4, 384.0, 307.2], [1144.0], [1215.5], [364.8, 313.6, 291.2, 142.72, 268.16], [346.56], [281.6], [200.0], [474.5], [652.8], [44.8, 185.6], [230.4], [3510.0],[2255.5, 2255.5],[305.92, 110.72], [365.0], [102.4, 102.4], [192.0, 89.6], [508.2], [119.79, 154.44], [3920.0, 1904.5, 3593.0], [1404.0, 1404.0]]
flexi_mfm_limit=df_flexi['MFM_limit'].values.tolist()
flexi_inv_limit=df_flexi['INV_limit'].values.tolist()
flexi_pr_limit=df_flexi['PR_limit'].values.tolist()
flexi_w=df_flexi['Webhook'].str.strip().values.tolist()

ebx_stns=['IN-008', 'IN-010', 'KH-002', 'KH-004', 'KH-005', 'KH-006', 'KH-007', 'MY-001', 'MY-003', 'SG-001', 'SG-002','SG-006', 'TH-002', 'TH-003']
ebx_capacities=[[291.5, 218.625], [106.0], [63.0], [181.44, 362.88], [858.0], [500.5, 500.5], [53.76], [313.5], [236.94, 307.56, 62.7], [44.785], [38.16],[163.8,100.8,176.4,50.4,504] ,[994.5], [533.0, 461.5]]
ebx_mfm_limit=df_ebx['MFM_limit'].values.tolist()
ebx_pr_limit=df_ebx['PR_limit'].values.tolist() 
ebx_inv_limit=df_ebx['INV_limit'].values.tolist()
ebx_w=df_ebx['Webhook'].str.strip().values.tolist()

stns=[seris_stns,locus_stns,flexi_stns,ebx_stns]
capacities=[seris_capacities,locus_capacities,flexi_capacities,ebx_capacities]
mfm_limit=[seris_mfm_limit,locus_mfm_limit,flexi_mfm_limit,ebx_mfm_limit]
inv_limit=[seris_inv_limit,locus_inv_limit,flexi_inv_limit,ebx_inv_limit]
pr_limit=[seris_pr_limit,locus_pr_limit,flexi_pr_limit,ebx_pr_limit]
webhook=[seris_w,locus_w,flexi_w,ebx_w]

#stns=['IN-002', 'IN-003', 'IN-004', 'IN-005', 'IN-006', 'IN-007', 'IN-008', 'IN-009', 'IN-010', 'IN-011', 'IN-012', 'IN-013', 'IN-014', 'IN-015', 'IN-016', 'IN-017', 'IN-018', 'IN-019', 'IN-020', 'IN-021', 'IN-022', 'IN-023', 'IN-025', 'IN-026', 'IN-027', 'IN-028', 'IN-029', 'IN-030', 'IN-031', 'IN-032', 'IN-033', 'IN-034', 'IN-035', 'IN-036', 'IN-037', 'IN-038', 'IN-039', 'IN-040', 'IN-041', 'IN-042', 
#'IN-043', 'IN-044', 'IN-045', 'IN-046', 'IN-047', 'IN-048', 'IN-049', 'IN-050', 'IN-051', 'IN-052', 'IN-053', 'IN-054', 'IN-055', 'IN-056', 'IN-057', 'IN-058', 'IN-059', 'IN-060', 'IN-061', 'IN-062', 'IN-063', 'IN-064', 'IN-065', 'IN-066', 'IN-067', 'IN-068', 'IN-069', 'IN-070', 'IN-071', 'IN-072', 'IN-073', 'IN-074', 'IN-075', 'IN-076', 'IN-077', 'IN-078', 'IN-079', 'IN-080', 'IN-081', 'IN-082', 'IN-083', 
#'IN-084', 'IN-085', 'IN-086', 'IN-087', 'KH-001', 'KH-002', 'KH-003', 'KH-004', 'KH-005', 'KH-006', 'KH-007', 'KH-008', 'KH-009', 'MY-001', 'MY-002', 'MY-003', 'MY-004', 'MY-005', 'MY-006', 'MY-007', 'MY-008', 'MY-009', 'MY-401', 'MY-402', 'MY-403', 'MY-404', 'MY-405', 'MY-406', 'PH-001', 'PH-002', 'PH-003', 'PH-004', 'PH-005', 'PH-006', 'PH-007', 'SG-001', 'SG-002', 'SG-003', 'SG-004', 'SG-005', 'SG-007',
# 'SG-008', 'TH-001', 'TH-002', 'TH-003', 'TH-004', 'TH-005', 'TH-007', 'TH-010', 'TH-011', 'TH-012', 'TH-013', 'TH-014', 'TH-015', 'TH-016', 'TH-017', 'TH-018', 'TH-019', 'TH-020', 'TH-021', 'TH-022', 'TH-023', 'TH-024', 'TH-025', 'TH-026', 'TH-027', 'TH-028', 'TH-048', 'VN-001', 'VN-002', 'VN-003']

#capacities=[[138.32, 45.76, 66.82], [227.24], [187.5, 109.75], [218.4], [93.94, 57.645, 29.89, 87.84, 36.6, 180.56], [150.0], [291.5, 218.625], [128.52], [106.0], [156.0, 218.4, 374.4], [283.5, 283.5, 532.98], [252.0, 220.5, 157.5, 126.0, 64.26], [226.8, 378.0, 453.6], [1814.4, nan], [340.2, 359.1, 66.15, 66.15], [239.4], [100.8], [750.48], [1004.88], [910.0], [614.4, 614.4, 384.0, 307.2], [1144.0], [1215.5], [364.8, 313.6, 291.2, 142.72, 268.16], [346.56], [281.6], [200.0], [474.5], [318.0, 445.0], [652.8], [44.8, 185.6], [230.4], [3510.0], [2255.5, 2255.5], [305.92, 110.72], [547.2], [280.8], [365.0], [130.0, 760.5], [345.0], [448.0], [102.4, 102.4], [133.92], [192.0, 89.6], [180.01], [41.54, 120.9, 449.5], [213.84], [806.4, 292.5, 282.1, 299.0, 156.0, 448.5, 172.8, 672.0, 345.6, 998.4, 780.8, 1024.0, 1003.2, 889.6, 513.5, 1105.0, 598.0, 454.4, 245.2, 455.0, 312.0, 132.0, 332.8, 633.6, 1256.0, 754.0], [370.5, 325.0, 302.58], [505.6, 241.92], [8615.0], [508.2], [377.0], [446.16, 503.25], [119.79, 154.44], [224.4, 43.56, 66.0], [476.52], [650.0, 390.0], [337.025], [442.2, 501.6, 264.0, 442.2], [756.36], [66.99, 127.71, 66.0, 403.59, 515.79], [4745.4], [834.6], [374.22], [995.15], [1203.84, 8534.0, 0.0, 4098.6], [3920.0, 1904.5, 3593.0, 0.0], [522.7], [203.4], [650.1], [65.0, 65.0], [552.5], [1254.0], [451.44, 451.44], [451.44, 376.2], [97.5, 59.8], [1214.4], [409.2], [811.53], [92.365, 627.0], [1632.3], [447.42], [258.06], [1001.0], [63.0], [63.0], [2560.0], [181.44, 362.88], [858.0], [500.5, 500.5], [53.76], [495.95, 2249.98, 2854.8, 1397.5, 2835.32], [356.4], [313.5], [316.8, 103.95], [236.94, 307.56, 62.7], [414.64], [558.72], [1615.6, 1579.5, 858.2], [345.8], [396.5, 1417.0], [913.56], [70.875], [70.875], [58.32], [70.875], [64.8], [70.875], [381.6], [503.5], [190.8], [320.0], [256.0], [300.8], [294.4], [44.785], [38.16], [839.52, 782.28, 165.36], [384.0, 460.8], [794.97, 1042.8, 819.0, 410.8, 486.0, 0.0, 0.0], [167.7], [646.1, 58.5], [1404.0, 1404.0], [994.5], [533.0, 461.5], [310.2], [916.5], [993.6], [994.5], [546.0], [994.5], [344.5], [994.5], [994.5], [702.0], [923.0], [994.5], [916.5], [994.5], [923.0], [994.5], [624.0], [650.0, 214.5], [546.0], [793.0], [624.0], [546.0], [318.5], [422.5], [546.0], [546.0]]

def seris_calc(stn,df,date):
    if(stn=='IN-015'):
        vals=df[['SolEnergyDelHVSide','HvYld','PRLV','Gsi','DA']].values.tolist()[0]
        return create_template(date,stn,energy=vals[0],yld=vals[1],pr=vals[2],ghi=vals[3],da=vals[4])
    elif(stn=='IN-036'):
        vals=df[['siClean','Eac2Str','Eac2Ctr','grid_avl','CoV','DA']].values.tolist()[0]
        Eac=vals[1]+vals[2]
        Yld=round(float((vals[1]+vals[2])/4511),2)
        PR=round(float(Yld/vals[0])*100,1)
        vals=[Eac]+[Yld]+[PR]+[vals[3],'NULL',vals[4],vals[5],vals[0]]
        if(os.path.exists('/home/admin/Dropbox/FlexiMC_Data/Fourth_Gen/[IN-036C]/[IN-036C]-lifetime.txt')):
            df=pd.read_csv('/home/admin/Dropbox/FlexiMC_Data/Fourth_Gen/[IN-036C]/[IN-036C]-lifetime.txt',sep='\t')
            flexi_df=locus_flexi_calc('IN-036',df,date,[2255.5, 2255.5],"Flexi")
            print(flexi_df)
            return create_template(date,stn,energy=vals[0],yld=vals[1],pr=vals[2],cov_mfm=vals[5],ghi=vals[7],da=vals[6],cov_inv=flexi_df['CoV_INV'][0])
        else:
            return create_template(date,stn,energy=vals[0],yld=vals[1],pr=vals[2],cov_mfm=vals[5],ghi=vals[7],da=vals[6])
    elif(stn=='KH-001'):
        vals=df[['Eac2SystGen','DailySpecificYield2','PR2','DA']].values.tolist()[0]
        return create_template(date,stn,energy=vals[0],yld=vals[1],pr=vals[2],da=vals[3])
    elif(stn=='KH-003'):
        vals=df[['EacCokeMeth2','Yld2Coke','PR2Coke','Irradiance','DA']].values.tolist()[0]
        return create_template(date,stn,energy=vals[0],yld=vals[1],pr=vals[2],ghi=vals[3],da=vals[4])
    elif(stn=='KH-008'):#Cov
        vals=df[['Eac2Full','Yld2Full','PR2Full','Gsi02','DA']].values.tolist()[0]
        cov_vals=df[['Yld2Pond','Yld2R02','Yld2R03','Yld2R06','Yld2R11']].values.tolist()[0]
        cov=round(np.nanstd(cov_vals)*100/np.nanmean(cov_vals),1)
        return create_template(date,stn,energy=vals[0],yld=vals[1],pr=vals[2],cov_mfm=cov,ghi=vals[3],da=vals[4])
    elif(stn=='MY-004'):#Cov
        vals=df[['Eac21','FullSiteYld2','FullSitePR2Si','GSi','DA']].values.tolist()[0]
        return create_template(date,stn,energy=vals[0],yld=vals[1],pr=vals[2],ghi=vals[3],da=vals[4])
    elif(stn=='MY-006'):#Cov
        vals=df[['FillSite2','FullSiteYld2','FullSitePR2','GSi','DA']].values.tolist()[0]
        caps=[1615.6, 1579.5, 858.2]
        eac_vals=df[['Eac21','Eac22','Eac23']].values.tolist()[0]
        ylds=[]
        for index,i in enumerate(eac_vals):
            ylds.append(i/caps[index])
        cov=round(np.nanstd(ylds)*100/np.nanmean(ylds),1)
        return create_template(date,stn,energy=vals[0],yld=vals[1],pr=vals[2],cov_mfm=cov,ghi=vals[3],da=vals[4])
    elif(stn=='SG-003'):
        vals=df[['FullSiteProd','CovYlds','Gsi00','W_G','W_P','Gsi00','DA']].values.tolist()[0]
        yld=float(vals[0]/1621.8)
        pr=float(yld/vals[2])*100
        return create_template(date,stn,energy=vals[0],yld=yld,pr=pr,cov_mfm=vals[1],ga=vals[3],pa=vals[4],ghi=vals[5],da=vals[6])
    elif(stn=='SG-004'):
        vals=df[['FullSiteProd','FullSiteYld','Gsi00','CovYlds','DA']].values.tolist()[0]
        PR=round(float(vals[1]/vals[2])*100,1)
        return create_template(date,stn,energy=vals[0],yld=vals[1],pr=PR,cov_mfm=vals[3],ghi=vals[2],da=vals[4])
    elif(stn=='SG-005'):
        vals=df[['FullSite2','FullSiteYld2','FullSitePRSi2','CovYld2','GSi','DA']].values.tolist()[0]
        return create_template(date,stn,energy=vals[0],yld=vals[1],pr=vals[2],cov_mfm=vals[3],ghi=vals[4],da=vals[5])
    elif(stn=='VN-001'):
        if(os.path.exists(path+'[VN-001X]/'+date[0:4]+'/'+date[0:7]+'/[VN-001X] '+date+'.txt')):
            df_cov=pd.read_csv(path+'[VN-001X]/'+date[0:4]+'/'+date[0:7]+'/[VN-001X] '+date+'.txt',sep='\t')
            cov=df_cov['CovYld'].values[0]
            vals=df[['FullSite','FullSiteYld2','FullSitePRSi2','GSi','DA']].values.tolist()[0]
            return create_template(date,stn,energy=vals[0],yld=vals[1],pr=vals[2],cov_mfm=0,cov_inv=cov,ghi=vals[3],da=vals[4])

def ebx_calc(stn,date,capacities):
    if(stn=='IN-008'):
        if(os.path.exists(path+'[IN-008X]/'+date[0:4]+'/'+date[0:7]+'/Meter 1/[IN-008X-M1] '+date+'.txt') and os.path.exists(path+'[IN-008X]/'+date[0:4]+'/'+date[0:7]+'/Meter 2/[IN-008X-M2] '+date+'.txt')):
            df_m1=pd.read_csv(path+'[IN-008X]/'+date[0:4]+'/'+date[0:7]+'/Meter 1/[IN-008X-M1] '+date+'.txt',sep='\t')
            df_m2=pd.read_csv(path+'[IN-008X]/'+date[0:4]+'/'+date[0:7]+'/Meter 2/[IN-008X-M2] '+date+'.txt',sep='\t')
            vals_m1=df_m1[['Eac2']].values.tolist()[0]
            vals_m2=df_m2[['Eac2']].values.tolist()[0]
            eac=vals_m1[0]+vals_m2[0]
            yld=float(eac)/sum(capacities)
            return create_template(date,stn,energy=eac,yld=yld)
    elif(stn=='IN-010'):
        if(os.path.exists(path+'[IN-010X]/'+date[0:4]+'/'+date[0:7]+'/[IN-010X] '+date+'.txt')):
            df_m1=pd.read_csv(path+'[IN-010X]/'+date[0:4]+'/'+date[0:7]+'/[IN-010X] '+date+'.txt',sep='\t')
            vals_m1=df[['Eac2','DailySpecYield2']].values.tolist()[0]
            return create_template(date,stn,energy=vals_m1[0],yld=vals_m1[1])
    elif(stn=='KH-002'):
        if(os.path.exists(path+'[KH-002X]/'+date[0:4]+'/'+date[0:7]+'/Solar/[KH-002X-M1] '+date+'.txt')):
            df_m1=pd.read_csv(path+'[KH-002X]/'+date[0:4]+'/'+date[0:7]+'/Solar/[KH-002X-M1] '+date+'.txt',sep='\t')
            vals=df_m1[['LoadConsumed','DailySpecYield1','PR']].values.tolist()[0]
        return create_template(date,stn,energy=vals[0],yld=vals[1],pr=vals[2])
    elif(stn=='KH-004'):
        if(os.path.exists(path+'[KH-004X]/'+date[0:4]+'/'+date[0:7]+'/MSB-01 Solar/[KH-004X-M3] '+date+'.txt') and os.path.exists(path+'[KH-004X]/'+date[0:4]+'/'+date[0:7]+'/MSB-02 Solar/[KH-004X-M6] '+date+'.txt')):
            df_m1=pd.read_csv(path+'[KH-004X]/'+date[0:4]+'/'+date[0:7]+'/MSB-01 Solar/[KH-004X-M3] '+date+'.txt',sep='\t')
            df_m2=pd.read_csv(path+'[KH-004X]/'+date[0:4]+'/'+date[0:7]+'/MSB-02 Solar/[KH-004X-M6] '+date+'.txt',sep='\t')
            vals_m1=df_m1[['SolarPowerMeth2','PR2']].values.tolist()[0]
            vals_m2=df_m2[['SolarPowerMeth2','PR2']].values.tolist()[0]
            eac=vals_m1[0]+vals_m2[0]
            yld=float(eac)/sum(capacities)
            pr=(vals_m1[1]*capacities[0]+vals_m2[1]*capacities[1])/(capacities[0]+capacities[1])
            return create_template(date,stn,energy=eac,yld=yld,pr=pr)
    elif(stn=='KH-005'):#Cov
        if(os.path.exists(path+'[KH-005X]/'+date[0:4]+'/'+date[0:7]+'/SCS/[KH-005X-M3] '+date+'.txt')):
            df_m1=pd.read_csv(path+'[KH-005X]/'+date[0:4]+'/'+date[0:7]+'/SCS/[KH-005X-M3] '+date+'.txt',sep='\t')
            vals=df_m1[['SolEac2','Yld','PR2']].values.tolist()[0]
            return create_template(date,stn,energy=vals[0],yld=vals[1],pr=vals[2])
    elif(stn=='KH-006'):#Cov
        if(os.path.exists(path+'[KH-006X]/'+date[0:4]+'/'+date[0:7]+'/MSB-01-Solar/[KH-006X-M2] '+date+'.txt') and os.path.exists(path+'[KH-006X]/'+date[0:4]+'/'+date[0:7]+'/MSB-02-Solar/[KH-006X-M4] '+date+'.txt')):
            df_m1=pd.read_csv(path+'[KH-006X]/'+date[0:4]+'/'+date[0:7]+'/MSB-01-Solar/[KH-006X-M2] '+date+'.txt',sep='\t')
            df_m2=pd.read_csv(path+'[KH-006X]/'+date[0:4]+'/'+date[0:7]+'/MSB-02-Solar/[KH-006X-M4] '+date+'.txt',sep='\t')
            vals_m1=df_m1[['SolarPowerMeth2','PR2']].values.tolist()[0]
            vals_m2=df_m2[['SolarPowerMeth2','PR2']].values.tolist()[0]
            eac=vals_m1[0]+vals_m2[0]
            yld=float(eac)/sum(capacities)
            pr=(vals_m1[1]*capacities[0]+vals_m2[1]*capacities[1])/(capacities[0]+capacities[1])
            return create_template(date,stn,energy=eac,yld=yld,pr=pr)
    elif(stn=='KH-007'):#Cov
        if(os.path.exists(path+'[KH-007X]/'+date[0:4]+'/'+date[0:7]+'/[KH-007X] '+date+'.txt')):
            df_m1=pd.read_csv(path+'[KH-007X]/'+date[0:4]+'/'+date[0:7]+'/[KH-007X] '+date+'.txt',sep='\t')
            vals=df_m1[['Eac2','Yld2','PR2']].values.tolist()[0]
            return create_template(date,stn,energy=vals[0],yld=vals[1],pr=vals[2])
    elif(stn=='MY-001'):
        if(os.path.exists(path+'[MY-001X]/'+date[0:4]+'/'+date[0:7]+'/[MY-001X] '+date+'.txt')):
            df_m1=pd.read_csv(path+'[MY-001X]/'+date[0:4]+'/'+date[0:7]+'/[MY-001X] '+date+'.txt',sep='\t')
            vals=df_m1[['Eac2','DailySpecYield2','PR2']].values.tolist()[0]
            return create_template(date,stn,energy=vals[0],yld=vals[1],pr=vals[2])
    elif(stn=='MY-003'):
        if(os.path.exists(path+'[MY-003X]/'+date[0:4]+'/'+date[0:7]+'/GF-1/[MY-003X-M1] '+date+'.txt') and os.path.exists(path+'[MY-003X]/'+date[0:4]+'/'+date[0:7]+'/GF-2/[MY-003X-M2] '+date+'.txt') and os.path.exists(path+'[MY-003X]/'+date[0:4]+'/'+date[0:7]+'/GF-3/[MY-003X-M3] '+date+'.txt')):
            df_m1=pd.read_csv(path+'[MY-003X]/'+date[0:4]+'/'+date[0:7]+'/GF-1/[MY-003X-M1] '+date+'.txt',sep='\t')
            df_m2=pd.read_csv(path+'[MY-003X]/'+date[0:4]+'/'+date[0:7]+'/GF-1/[MY-003X-M2] '+date+'.txt',sep='\t')
            df_m3=pd.read_csv(path+'[MY-003X]/'+date[0:4]+'/'+date[0:7]+'/GF-3/[MY-003X-M3] '+date+'.txt',sep='\t')
            vals_m1=df_m1[['SolarPowerMeth2','PR2']].values.tolist()[0]
            vals_m2=df_m2[['SolarPowerMeth2','PR2']].values.tolist()[0]
            vals_m3=df_m3[['SolarPowerMeth2','PR2']].values.tolist()[0]
            eac=vals_m1[0]+vals_m2[0]+vals_m3[0]
            yld=float(eac)/sum(capacities)
            pr=(vals_m1[1]*capacities[0]+vals_m2[1]*capacities[1]+vals_m3[1]*capacities[2])/sum(capacities)
            return create_template(date,stn,energy=eac,yld=yld,pr=pr)
    elif(stn=='SG-001'):
        if(os.path.exists(path+'[SG-001X]/'+date[0:4]+'/'+date[0:7]+'/[SG-001X] '+date+'.txt')):
            df_m1=pd.read_csv(path+'[SG-001X]/'+date[0:4]+'/'+date[0:7]+'/[SG-001X] '+date+'.txt',sep='\t')
            vals=df_m1[['Eac2','DSPY2','PR2']].values.tolist()[0]
            return create_template(date,stn,energy=vals[0],yld=vals[1],pr=vals[2])
    elif(stn=='SG-002'):
        if(os.path.exists(path+'[SG-002X]/'+date[0:4]+'/'+date[0:7]+'/[SG-002X] '+date+'.txt')):
            df_m1=pd.read_csv(path+'[SG-002X]/'+date[0:4]+'/'+date[0:7]+'/[SG-002X] '+date+'.txt',sep='\t')
            vals=df_m1[['Eac2','DailySpecYield2','PR2']].values.tolist()[0]
            return create_template(date,stn,energy=vals[0],yld=vals[1],pr=vals[2])
    elif(stn=='SG-006'):
        if(os.path.exists(path+'[SG-006X]/'+date[0:4]+'/'+date[0:7]+'/Admin/[SG-006X-M1] '+date+'.txt') and os.path.exists(path+'[SG-006X]/'+date[0:4]+'/'+date[0:7]+'/Canteen/[SG-006X-M2] '+date+'.txt') and os.path.exists(path+'[SG-006X]/'+date[0:4]+'/'+date[0:7]+'/EM/[SG-006X-M3] '+date+'.txt') and os.path.exists(path+'[SG-006X]/'+date[0:4]+'/'+date[0:7]+'/Mill/[SG-006X-M4] '+date+'.txt') and os.path.exists(path+'[SG-006X]/'+date[0:4]+'/'+date[0:7]+'/Warehouse/[SG-006X-M5] '+date+'.txt')):
            df_m1=pd.read_csv(path+'[SG-006X]/'+date[0:4]+'/'+date[0:7]+'/Admin/[SG-006X-M1] '+date+'.txt',sep='\t')
            df_m2=pd.read_csv(path+'[SG-006X]/'+date[0:4]+'/'+date[0:7]+'/Canteen/[SG-006X-M2] '+date+'.txt',sep='\t')
            df_m3=pd.read_csv(path+'[SG-006X]/'+date[0:4]+'/'+date[0:7]+'/EM/[SG-006X-M3] '+date+'.txt',sep='\t')
            df_m4=pd.read_csv(path+'[SG-006X]/'+date[0:4]+'/'+date[0:7]+'/Mill/[SG-006X-M4] '+date+'.txt',sep='\t')
            df_m5=pd.read_csv(path+'[SG-006X]/'+date[0:4]+'/'+date[0:7]+'/Warehouse/[SG-006X-M5] '+date+'.txt',sep='\t')
            vals_m1=df_m1[['SolarPowerMeth2','PR2','Yield2']].values.tolist()[0]
            vals_m2=df_m2[['SolarPowerMeth2','PR2','Yield2']].values.tolist()[0]
            vals_m3=df_m3[['SolarPowerMeth2','PR2','Yield2']].values.tolist()[0]
            vals_m4=df_m4[['SolarPowerMeth2','PR2','Yield2']].values.tolist()[0]
            vals_m5=df_m5[['SolarPowerMeth2','PR2','Yield2']].values.tolist()[0]
            eac=vals_m1[0]+vals_m2[0]+vals_m3[0]+vals_m4[0]+vals_m5[0]
            yld=float(eac)/sum(capacities)
            ylds=[vals_m1[2],vals_m2[2],vals_m3[2],vals_m4[2],vals_m5[2]]
            pr=(vals_m1[1]*capacities[0]+vals_m2[1]*capacities[1]+vals_m3[1]*capacities[2]+vals_m4[1]*capacities[3]+vals_m5[1]*capacities[4])/sum(capacities)
            if(pr>85):
                pr=85
            cov=round(np.nanstd(ylds)*100/np.nanmean(ylds),1)
            print(ylds,cov)
            return create_template(date,stn,energy=eac,yld=yld,pr=pr,cov_mfm=cov)
    elif(stn=='TH-002'):
        if(os.path.exists(path+'[TH-002X]/'+date[0:4]+'/'+date[0:7]+'/Invoice/[TH-002X-M2] '+date+'.txt')):
            df_m1=pd.read_csv(path+'[TH-002X]/'+date[0:4]+'/'+date[0:7]+'/Invoice/[TH-002X-M2] '+date+'.txt',sep='\t')
            vals=df_m1[['SolEac2','Yld2','PR2']].values.tolist()[0]
            return create_template(date,stn,energy=vals[0],yld=vals[1],pr=vals[2])
    elif(stn=='TH-003'):
        if(os.path.exists(path+'[TH-003X]/'+date[0:4]+'/'+date[0:7]+'/MDB-01 Invoice/[TH-003X-M2] '+date+'.txt') and os.path.exists(path+'[TH-003X]/'+date[0:4]+'/'+date[0:7]+'/MDB-02 Invoice/[TH-003X-M4] '+date+'.txt')):
            df_m1=pd.read_csv(path+'[TH-003X]/'+date[0:4]+'/'+date[0:7]+'/MDB-01 Invoice/[TH-003X-M2] '+date+'.txt',sep='\t')
            df_m2=pd.read_csv(path+'[TH-003X]/'+date[0:4]+'/'+date[0:7]+'/MDB-02 Invoice/[TH-003X-M4] '+date+'.txt',sep='\t')
            vals_m1=df_m1[['Eac2','PR2']].values.tolist()[0]
            vals_m2=df_m2[['Eac2','PR2']].values.tolist()[0]
            eac=vals_m1[0]+vals_m2[0]
            yld=float(eac)/sum(capacities)
            pr=(vals_m1[1]*capacities[0]+vals_m2[1]*capacities[1])/(capacities[0]+capacities[1])
            return create_template(date,stn,energy=eac,yld=yld,pr=pr)



def locus_flexi_calc(stn,df,date,capacities,provider):
    cols=df.columns.tolist()
    temp_invcols=[]
    temp_mfmcols=[]
    temp_pr=[]
    temp_ga=[]
    temp_pa=[]
    temp_meterdata=[]
    temp_wms=[]
    temp_da=[]
    if(provider=='Locus'):
        for i in cols:
            if(i[-4:]=='Yld2' and i[0:3]!='MFM'):
                temp_invcols.append(i)
            elif(i[-4:]=='Yld2' and i[0:3]=='MFM'):
                temp_mfmcols.append(i)
            elif(i[-3:]=='PR2' and i[0:3]=='MFM'):
                temp_pr.append(i)
            elif(i[-4:]=='Eac2' and i[0:3]=='MFM'):
                temp_meterdata.append(i)
            elif(i[-2:]=='GA' and ('inverter' not in i.lower())):
                temp_ga.append(i)
            elif(i[-2:]=='PA' and ('inverter' not in i.lower())):
                temp_pa.append(i)
            elif('GTI' in i):
                temp_wms.append(i)
            elif('DA' in i and 'MFM' in i):
                temp_da.append(i)    
    else:
        for i in cols:
            if(i[-4:]=='Yld2' and ('Inverter' in i)):
                temp_invcols.append(i)
            elif(i[-4:]=='Yld2' and ('Inverter' not in i) and ('Load' not in i)):
                temp_mfmcols.append(i)
            elif(i[-3:]=='PR2' and ('Inverter' not in i) and ('Load' not in i)):
                temp_pr.append(i)
            elif(i[-4:]=='Eac2' and ('Inverter' not in i) and ('Load' not in i)):
                temp_meterdata.append(i)
            elif(i[-2:]=='GA' and ('inverter' not in i.lower())):
                temp_ga.append(i)
            elif(i[-2:]=='PA' and ('inverter' not in i.lower())):
                temp_pa.append(i)
            elif('GTI' in i):
                temp_wms.append(i)
            elif('DA' in i and 'Inverter' not in i and 'WMS' not in i):
                temp_da.append(i)  
    cols_temp={'INV':temp_invcols,'MFM':temp_mfmcols,'PR':temp_pr,'MD':temp_meterdata,'GA':temp_ga,'PA':temp_pa,'WMS':temp_wms,'DA':temp_da}
    template_df=create_template(date,stn)
    for i in cols_temp:
        df_Yld=df[['Date']+cols_temp[i]].fillna(np.nan).copy()
        df_Subset=df_Yld.loc[df_Yld['Date']==date,].fillna(np.nan)
        vals=[]
        if(df_Subset.empty):
            print('Data not present for the Date!')
            return template_df
        else:
            vals=list(df_Subset.values[0][1:])
            if(i=='INV' or i=='MFM'):
                if(stn=='IN-087' and i=='INV'):#SMB only
                    vals=vals[1:]
                    print('SMB VALS',vals)
                if(len(vals)==0):
                    template_df.loc[:,'CoV_'+i]=0
                else:
                    cov=round(np.nanstd(vals)*100/np.nanmean(vals),1)
                    template_df.loc[:,'CoV_'+i]=cov
            elif(i=='PR'):
                temp2=0
                for i in range(len(capacities)):
                    if(vals[i]==np.nan or math.isnan(vals[i])):
                        continue
                    temp2=temp2+capacities[i]*vals[i]
                PR=temp2/sum(capacities)
                if((stn=='SG-007' and PR>85) or (stn=='SG-008' and PR>85)):
                    template_df.loc[:,'PR']=85
                else:
                    template_df.loc[:,'PR']=PR
            elif(i=='MD'):
                template_df.loc[:,'Energy']=np.nansum(vals)
                template_df.loc[:,'Yield']=round(np.nansum(vals)/sum(capacities),2)
            elif(i=='WMS'):
                if(len(vals)!=0):
                    template_df.loc[:,'GHI']=vals[0]
            elif(i=='DA'):
                temp2=0
                for i in range(len(capacities)):
                    if(vals[i]==np.nan):
                        continue
                    temp2=temp2+capacities[i]*vals[i]
                DA=temp2/sum(capacities)
                if(DA>100):
                    DA=100
                template_df.loc[:,'DA']=DA
            elif(i=='GA' or i=='PA'):
                if(len(temp_ga)==0 or len(temp_pa)==0):
                    pass
                else:
                    avl=0
                    for j in range(len(capacities)):
                        avl=avl+capacities[j]*vals[j]
                    template_df.loc[:,i]=avl/sum(capacities)
    return template_df


def get_status(data,mfm_limit,inv_limit,pr_limit):
    data=data.fillna('NULL')
    print(data['O&M_Code'][0][0:2])
    if(data['O&M_Code'][0][0:2]!='IN' and data['GHI'][0]=='NULL' and data['PR'][0]!='NULL' and data['PR'][0]>85):
        data['PR']=85
    elif(data['O&M_Code'][0][0:2]!='IN' and data['GHI'][0]=='NULL' and data['PR'][0]!='NULL' and data['PR'][0]<70 and data['PR'][0]>50):
        data['PR']=70
    print(data)
    if(data['PR'][0]=='NULL'):
        data['PR_Alarm']='NULL'
    elif(data['PR'][0]>90 and data['PR'][0]!='NULL'):
        data['PR_Alarm']=2
    elif(data['PR'][0]<pr_limit and data['PR'][0]!='NULL'):
        data['PR_Alarm']=1
    else:
        data['PR_Alarm']=0
    if(data['CoV_MFM'][0]=='NULL'):
        data['MFM_CoV_Alarm']='NULL'
    elif(((data['CoV_MFM'][0])>mfm_limit and data['CoV_MFM'][0]!='NULL')):
        data['MFM_CoV_Alarm']=1
    else:
        data['MFM_CoV_Alarm']=0
    if(data['CoV_INV'][0]=='NULL'):
        data['INV_CoV_Alarm']='NULL'
    elif((data['CoV_INV'][0]>inv_limit and data['CoV_INV'][0]!='NULL')):
        data['INV_CoV_Alarm']=1
    else:
        data['INV_CoV_Alarm']=0
    if(data['PR_Alarm'][0]=='NULL'):
        data['Status']='NULL'
    elif(data['PR_Alarm'][0]==0 and (data['INV_CoV_Alarm'][0]==1 or data['MFM_CoV_Alarm'][0]==1)):
        data['Status']=1
    elif(data['PR_Alarm'][0]==2):
        data['Status']=2
    elif(data['PR_Alarm'][0]==1):
        data['Status']=3
    elif(data['PR_Alarm'][0]==0):
        data['Status']=0
    data=data.replace(np.inf, np.nan)
    return data


    

def azure_push(data):
    connStr = pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+ password)
    cursor = connStr.cursor()
    data=data.fillna('NULL')
    data=data.replace(to_replace ="NULL", value =np.nan) 
    data = data.replace({np.nan: None})
    for index,row in data.iterrows():
        try:
            with cursor.execute("INSERT INTO dbo.Station_Alerts([Date],[O&M_Code],[Energy],[Yield],[GHI],[PR],[DA],[GA],[PA],[CoV_MFM],[CoV_INV],[PR_Alarm],[MFM_CoV_Alarm],[INV_CoV_Alarm],[Status]) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", row['Date'], row['O&M_Code'], row['Energy'], row['Yield'] ,row['GHI'],row['PR'],row['DA'],row['GA'],row['PA'], row['CoV_MFM'],row['CoV_INV'],row['PR_Alarm'],row['MFM_CoV_Alarm'],row['INV_CoV_Alarm'],row['Status']):
                pass
            connStr.commit()
        except Exception as e:
            print(e)
            pass
    cursor.close()
    connStr.close()

def teams_alert(data,webhook):
    if((data['Status'][0]==3 or data['Status'][0]==2 or data['Status'][0]==1) and webhook!='NA'):
        print(data)
        data=data.fillna('NA')
        myTeamsMessage = pymsteams.connectorcard(webhook)
        #myTeamsMessage = pymsteams.connectorcard("https://outlook.office.com/webhook/2d5e48a9-107c-41d1-be0b-d482a5f12de0@8ec327b2-e844-412c-8463-3e633202b00f/IncomingWebhook/486b64fd0d59476ba0056e7d883d79d6/b91f3ec7-71fb-437f-8eab-f5905ff0c963")
        if(data['Status'][0]==3 or data['Status'][0]==2):
            myTeamsMessage.title(data['O&M_Code'][0]+" PR ALERT")
        elif(data['Status'][0]==1):
            myTeamsMessage.title(data['O&M_Code'][0]+" CoV ALERT")
        myTeamsMessage.text("<pre>Date: "+str(data['Date'][0])+"<br>PR: "+str(round(data['PR'][0],1))+"<br>Energy: "+str(round(data['Energy'][0],1))+"<br>MFM CoV: "+str(data['CoV_MFM'][0])+"<br>INV CoV: "+str(data['CoV_INV'][0])+"</pre>")
        myTeamsMessage.send()
        time.sleep(5)
    else:
        return 



while(1):
    if(datetime.datetime.now(tz).hour==1):
        date=str(datetime.datetime.now(tz).date()+datetime.timedelta(days=-1))
        for provider_index,n in enumerate((stns)):
            for index,i in enumerate(n):
                try:
                    print(i)
                    if(provider_index==0):
                        if(os.path.exists(path+'['+i+'S]/'+date[0:4]+'/'+date[0:7]+'/['+i+'S] '+date+'.txt')):
                            df=pd.read_csv(path+'['+i+'S]/'+date[0:4]+'/'+date[0:7]+'/['+i+'S] '+date+'.txt',sep='\t')
                            data=seris_calc(i,df,date)
                            data=get_status(data,seris_mfm_limit[index],seris_inv_limit[index],seris_pr_limit[index])
                            azure_push(data)
                            teams_alert(data,seris_w[index])
                        else:
                            print('Not there')
                            data=create_template(date,i)
                            azure_push(data)
                    elif(provider_index==1):
                        if(os.path.exists('/home/admin/Dropbox/Fourth_Gen/['+i+'L]/['+i+'L]-lifetime.txt')):
                            df=pd.read_csv('/home/admin/Dropbox/Fourth_Gen/['+i+'L]/['+i+'L]-lifetime.txt',sep='\t')
                            data=locus_flexi_calc(i,df,date,locus_capacities[index],'Locus')
                            data=get_status(data,locus_mfm_limit[index],locus_inv_limit[index],locus_pr_limit[index])
                            azure_push(data)
                            teams_alert(data,locus_w[index])
                    elif(provider_index==3):
                        data=ebx_calc(i,date,ebx_capacities[index])
                        data=get_status(data,ebx_mfm_limit[index],ebx_inv_limit[index],ebx_pr_limit[index])  
                        azure_push(data)
                        teams_alert(data,ebx_w[index])
                    else:
                        if(i=='IN-039' or i=='IN-006'):
                            continue
                        if(os.path.exists('/home/admin/Dropbox/FlexiMC_Data/Fourth_Gen/['+i+'C]/['+i+'C]-lifetime.txt')):
                            df=pd.read_csv('/home/admin/Dropbox/FlexiMC_Data/Fourth_Gen/['+i+'C]/['+i+'C]-lifetime.txt',sep='\t')
                            data=locus_flexi_calc(i,df,date,flexi_capacities[index],'Flexi')
                            data=get_status(data,flexi_mfm_limit[index],flexi_inv_limit[index],flexi_pr_limit[index])
                            azure_push(data)
                            teams_alert(data,flexi_w[index])
                except Exception as e:
                    print(e)
                    logging.exception("message")
    time.sleep(3600)
