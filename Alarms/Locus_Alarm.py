import requests, json
import requests.auth
import pandas as pd
import datetime
import os
import re 
import time
import shutil
import pytz
import sys
import pyodbc

tz = pytz.timezone('Asia/Calcutta')
tz2 = pytz.timezone('Asia/Bangkok')
path='/home/admin/Dropbox/Gen 1 Data/'
mailpath='/home/admin/CODE/Send_mail/mail_recipients.csv'


server = 'cleantechsolar.database.windows.net'
database = 'Cleantech Meter Readings'
username = 'RohanKN'
password = 'R@h@nKN1'
driver= '{ODBC Driver 13 for SQL Server}'

connStr = pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+ password)
SQL_Query = pd.read_sql_query(
'''SELECT TOP (1000) [Station_Name]
      ,[Station_Columns]
      ,[Station_Irradiation_Center]
      ,[Station_No_Meters]
      ,[Provider],[Alarm_Status]
  FROM [dbo].[stations] ''', connStr)
dflifetime = pd.DataFrame(SQL_Query, columns=['Station_Name','Station_Columns','Station_Irradiation_Center','Station_No_Meters','Provider','Alarm_Status'])
connStr.close()
dflifetime=dflifetime.loc[(dflifetime['Alarm_Status']==1),:]
hmm=(dflifetime.to_dict(orient='records'))

stations={}
for i in hmm:
    if(i['Station_Name'].strip()[0:2]=='IN'):
        tz=pytz.timezone('Asia/Calcutta')
    elif(i['Station_Name'].strip()[0:2]=='TH'):
        tz=pytz.timezone('Asia/Bangkok')
    elif(i['Station_Name'].strip()[0:2]=='VN'):
        tz=pytz.timezone('Asia/Ho_Chi_Minh')
    else:
        tz=pytz.timezone('Asia/Phnom_Penh')
    stations['['+i['Station_Name'].strip()+'L]']=[i['Station_No_Meters']]
    temp=[]
    for index,j in enumerate(i['Station_Columns'].strip().split(',')):
        if(index>0 and index<(2+i['Station_No_Meters'])): #Skip date and include meters only
            temp2=(j.replace('"','').split('.')[:-1])
            temp.append(' '.join(temp2))
    stations['['+i['Station_Name'].strip()+'L]'].append(temp)
    stations['['+i['Station_Name'].strip()+'L]'].append([0]*(i['Station_No_Meters']+1))
    stations['['+i['Station_Name'].strip()+'L]'].append([datetime.datetime.now(tz)]*(i['Station_No_Meters']+1))
print(stations)

def sendmail(station,type,data,rec):
    SERVER = "smtp.office365.com"
    FROM = 'operations@cleantechsolar.com'
    me=['sai.pranav@cleantechsolar.com']
    recipients = rec# must be a list
    cleantech_rec=[]
    for i in recipients:
        if(i.split('@')[1]=='cleantechsolar.com' or i.split('@')[1]=='comin.com.kh'):
            cleantech_rec.append(i)
    TO=", ".join(cleantech_rec)
    if(type=='irr'):
        SUBJECT = station+' Irradiance Alarm'
        text2='Last Timestamp Read: '+str(data[0]) +'\n\n'
    else:
        SUBJECT = station+' Power Trip Alarm'
        text2='Last Timestamp Read: '+str(data[0]) +'\n\n Meter Name: '+str(data[1])
    TEXT = text2
    # Prepare actual message
    message = "From:"+FROM+"\nTo:"+TO+"\nSubject:"+ SUBJECT +"\n\n"+TEXT
    # Send the mail
    import smtplib
    server = smtplib.SMTP(SERVER)
    server.starttls()
    server.login('shravan.karthik@cleantechsolar.com', 'CTS&*(789')
    server.sendmail(FROM, recipients, message)
    server.quit()
    if(station[1:3]=='KH'):
        from twilio.rest import Client
        account = "ACcac80891fc3a1edb14f942975a0a8939"
        token = "d39acf2278b2194b59aa15aa9e3a2072"
        client = Client(account, token)
        smsrec=['+85569313557',"+6591523695",'+85569236207','+85569875847']
        for t in smsrec:
            message = client.messages.create(to="+"+str(int(t)), from_="+12402930809",body=SUBJECT +"\n\n"+TEXT)


def rectolist(station,path):
    df=pd.read_csv(path)
    df2=df[['Recipients',station]]
    a=df2.loc[df2[station] == 1]['Recipients'].tolist()
    return a

starttime=time.time()
while(1):
    for j,i in enumerate(stations):
        if(i[1:3]=='IN'):
            tz=pytz.timezone('Asia/Calcutta')
        elif(i[1:3]=='TH'):
            tz=pytz.timezone('Asia/Bangkok')
        elif(i[1:3]=='KH'):
            tz=pytz.timezone('Asia/Phnom_Penh')
        elif(i[1:3]=='VN'):
            tz=pytz.timezone('Asia/Ho_Chi_Minh')
        date_now=datetime.datetime.now(tz).strftime('%Y-%m-%d')
        print(datetime.datetime.now(tz).hour)
        if(datetime.datetime.now(tz).hour>7 and datetime.datetime.now(tz).hour<18):
            print(i)
            temp=stations[i][1]
            for index,k in enumerate(temp):
                print(k)
                df=pd.read_csv(path+i+'/'+date_now[0:4]+'/'+date_now[0:7]+'/'+k+'/'+i+'-'+k[0:3]+k[4]+'-'+date_now[0:10]+'.txt',sep='\t')
                if(index==0):
                    #Irradiance Alarm
                    df_irr=df[['POAI_avg']]
                    irr=df_irr.tail(6).sum()
                    if(irr[0]==0):
                        try:
                            df_irr=df[['OTI_avg']]
                            irr=df_irr.tail(6).sum()
                        except:
                            pass
                    print('irr is',irr[0])
                    date=df.tail(1)['ts'].values
                    if(irr[0]<1 and stations[i][2][index]==0):
                        recipients=rectolist(i[1:-1]+'_Mail',mailpath)
                        sendmail(i,'irr',[date[0],k],recipients)
                        stations[i][3][index]=datetime.datetime.now(tz)
                        stations[i][2][index]=1
                    elif(irr[0]<1 and stations[i][2][index]==1):
                        diff=datetime.datetime.now(tz)-stations[i][3][index]
                        if((diff.seconds)//60>120):
                            stations[i][3][index]=datetime.datetime.now(tz)
                            recipients=rectolist(i[1:-1]+'_Mail',mailpath)
                            sendmail(i,'irr',[date[0],k],recipients)
                    else:
                         stations[i][2][index]==0
                elif(index>=1):
                    #Power Alarm
                    df_power=df[['W_avg']]
                    pow1=df_power.tail(6).sum()
                    print('Power is',pow1[0])
                    date=df.tail(1)['ts'].values
                    print('Irr is '+str(irr[0]))
                    print('Flag is '+str(stations[i][2][index]))
                    if(((pow1[0]<1 and irr[0]>5) or ( stations[i][2][0]==1 and pow1[0]<1)) and stations[i][2][index]==0):
                        recipients=rectolist(i[1:-1]+'_Mail',mailpath)
                        print('Sending')
                        sendmail(i,'pow',[date[0],k],recipients)
                        stations[i][3][index]=datetime.datetime.now(tz)
                        stations[i][2][index]=1
                    elif(((pow1[0]<1 and irr[0]>5) or ( stations[i][2][0]==1 and pow1[0]<1)) and stations[i][2][index]==1):
                        diff=datetime.datetime.now(tz)-stations[i][3][index]
                        print(diff)
                        if((diff.seconds)//60>120):
                            print('Second time')
                            stations[i][3][index]=datetime.datetime.now(tz)
                            recipients=rectolist(i[1:-1]+'_Mail',mailpath)
                            sendmail(i,'pow',[date[0],k],recipients)
                    else:
                        stations[i][2][index]==0
    time.sleep(900.0 - ((time.time() - starttime) % 900.0))