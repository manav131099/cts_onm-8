source('/home/admin/CODE/common/math.R')
VOLTTRESH = 410
GTHRESH = 20
PACTHRESH = 5
checkdir = function(x)
{
  if(!file.exists(x))
  {
    dir.create(x)
  }
}
rf = function(x)
{
  return(format(round(x,2),nsmall=2))
}
rf1 = function(x)
{
  return(format(round(x,1),nsmall=1))
}
rf3 = function(x)
{
  return(format(round(x,3),nsmall=3))
}
prepareSumm = function(dataread)
{
	APPENDWARNING <<- 0
	da = nrow(dataread)
  daPerc = round(da/14.4,1)
	thresh = 5/1000
	gsi1=gsi2=gsi3=tambsh=tambmxsh=tambmnsh=tamb=tambmx=tambmn=hamb=hambmx=hambmn=NA
  hambsh=hambmxsh=hambmnsh=tmod=tmodsh=tmodmx=tmodmxsh=tmodmn=tmodmnsh=NA
	gridAvail=SystemAvail=NA
	pr2 = yld2=NA
	dataread2 = as.numeric(dataread[complete.cases(dataread[,17]),17])
	if(length(dataread2))
	{
		dataread2 = dataread2[dataread2 <= (1.1*VOLTTRESH)]
		if(length(dataread2))
			dataread2 = dataread2[dataread2 >= (0.9 * VOLTTRESH)]
	}
	gridAvail = length(dataread2)
	dataread2 = dataread[complete.cases(dataread[,c(3,24)]),c(3,24)]
	if(nrow(dataread2))
	{
		dataread2 = dataread2[(as.numeric(dataread2[,1]) > GTHRESH),]
		if(nrow(dataread2))
			dataread2 = dataread2[as.numeric(dataread2[,2]) > PACTHRESH,]
	}
	SystemAvail = nrow(dataread2)
	dataread2 = dataread[complete.cases(dataread[,3]),3]
	if(length(dataread2))
		gsi1 = sum(dataread[complete.cases(dataread[,3]),3])/60000
	dataread2 = dataread[complete.cases(dataread[,4]),4]
	if(length(dataread2))
		gsi2 = sum(dataread[complete.cases(dataread[,4]),4])/60000
	dataread2 = dataread[complete.cases(dataread[,5]),5]
	if(length(dataread2))
		gsi3 = sum(dataread[complete.cases(dataread[,5]),5])/60000
#  gsi2 = sum(dataread[complete.cases(dataread[,4]),4])/60000
#  gsismp = sum(dataread[complete.cases(dataread[,5]),5])/60000
  subdata = dataread[complete.cases(dataread[,3]),]

  subdata = subdata[as.numeric(subdata[,3]) > thresh,]

  dataread2 = subdata[complete.cases(subdata[,6]),6]

	if(length(dataread2))
	{
  	tambsh = mean(dataread2)
  	tambmxsh = max(dataread2)
  	tambmnsh = min(dataread2) 

	}
	dataread2 = subdata[complete.cases(subdata[,7]),7]
	if(length(dataread2))
	{
  	hambsh = mean(dataread2)
		hambmxsh = max(dataread2)
		hambmnsh = min(dataread2)
	}
	dataread2 = subdata[complete.cases(subdata[,8]),8]
	if(length(dataread2))
	{
  	tsish = mean(dataread2)
		tsimxsh = max(dataread2)
		tsimnsh = min(dataread2)
	}
	
	dataread2 = dataread[complete.cases(dataread[,6]),6]
	if(length(dataread2))
	{
  	tamb = mean(dataread2)
		tambmx = max(dataread2)
		tambmn = min(dataread2) 
	}
	dataread2 = dataread[complete.cases(dataread[,7]),7]
	if(length(dataread2))
	{
  	hamb = mean(dataread2)
		hambmx = max(dataread2)
		hambmn = min(dataread2)
	}
	dataread2 = dataread[complete.cases(dataread[,8]),8]
	if(length(dataread2))
	{
  	tsi = mean(dataread2)
		tsimx = max(dataread2)
		tsimn = min(dataread2)
	}

  
 	 gsirat = gsi1 / gsi2
	 gmodrat = gsi3/gsi2
#  smprat = gsismp / gsi2

	Eac11 = Eac21 =LastR1=LastT1=NA
	
	dataread2 = dataread[complete.cases(dataread[,24]),24]
	if(length(dataread2))
	{
		Eac11 = sum(dataread[complete.cases(dataread[,24]),24])/60
	}
	dataread2 = dataread[complete.cases(dataread[,39]),39]
	if(length(dataread2))
	{
  	Eac21 = as.numeric(dataread2[length(dataread2)])  - as.numeric(dataread2[1])
		LastR1 = dataread2[length(dataread2)]
		LastT1 = as.character(dataread[complete.cases(dataread[,39]),1])
		LastT1 = LastT1[length(LastT1)]
	}

	FullSiteProd = Eac21 
	FullSiteProd1 = Eac11

	FullSiteYld=(FullSiteProd / 422.5)
	FullSitePR=(FullSiteProd / 4.225/gsi1)
	FullSitePRsi=(FullSiteProd / 4.225/gsi2)
	
	FullSiteYld1=(FullSiteProd1 / 422.5)
	FullSitePR1=(FullSiteProd1 / 4.225/gsi1)
	FullSitePRsi1=(FullSiteProd1 / 4.225/gsi2)
	
	dateAc = NA

	if(nrow(dataread))
		dateAc = substr(dataread[1,1],1,10)
  TambSH <- 0
  Tamb_vector = vector()
  if(!is.na(tambsh))
  {
  Tamb_vector = c(Tamb_vector, tambsh)
  }
  print("Printing TambSH")
  mean_Tamb = mean(Tamb_vector)
  print(mean_Tamb)
  
  timestamps_irradiance_greater_20 = dataread[dataread[, 4]>20, 1]
  timestamps_freq_greater_40 = dataread[(dataread[,37]>40 & dataread[,37]!= "NaN") ,1]
  timestamps_pow_greater_2 = dataread[abs(dataread[,24])>2,1]
  common = intersect(timestamps_irradiance_greater_20, timestamps_freq_greater_40)
  common2 = intersect(common, timestamps_pow_greater_2)
  GA = round(((length(common)/length(timestamps_irradiance_greater_20))*100), 1)
  PA = round(((length(common2)/length(common))*100), 1)
  
  datawrite = data.frame(Date = dateAc,
	                       PtsRec = rf(da),
												 GPy = rf(gsi1), 
												 GSi = rf(gsi2), 
												 GPyGSiRat = rf3(gsirat),
												 Tamb = rf1(tamb),
												 TambMx = rf1(tambmx),
												 TambMn = rf1(tambmn),
												 TambSH = rf1(mean_Tamb),
												 TambMxSH = rf1(tambmxsh),
												 TambMnSH = rf1(tambmnsh),
												 Hamb = rf1(hamb),
												 HambMx = rf1(hambmx),
												 HambMn = rf1(hambmn),
												 HambSH = rf1(hambsh),
												 HambMxSH = rf1(hambmxsh),
												 HambMnSH = rf1(hambmnsh),
												 TSi = rf1(tamb),
												 TSiMx = rf1(tambmx),
												 TSiMn = rf1(tambmn),
												 TSiSH = rf1(tambsh),
												 TSiMxSH = rf1(tambmxsh),
												 TSiMnSH = rf1(tambmnsh),
												 FullSite = rf(FullSiteProd),
												 FullSiteYld2 = rf(FullSiteYld),
												 FullSitePR2 = rf1(FullSitePR),
												 FullSitePRSi2 = rf1(FullSitePRsi),
												 Eac11 = rf(Eac11),
												 Eac21 = rf(Eac21),
												 LastR1 = LastR1,
												 LastT1 = LastT1,
												 GridAvail=gridAvail,
												 SystemAvail=SystemAvail,
												 Gmod=rf(gsi3),
												 GModGSiRat = rf3(gmodrat),
												 DA=rf1(daPerc),
												 FullSiteYld1 = rf(FullSiteYld1),
												 FullSitePR1 = rf1(FullSitePR1),
												 FullSitePRSi1 = rf1(FullSitePRsi1),
                         GA = GA,
                         PA = PA, 
												 stringsAsFactors=F
												)
  datawrite
}

rewriteSumm = function(datawrite)
{
  df = datawrite
  df
}

path = "/home/admin/Dropbox/Cleantechsolar/1min/[722]"
pathwrite = "/home/admin/Dropbox/Second Gen/[VN-001S]"
checkdir(pathwrite)
years = dir(path)
x=y=z=1
for(x in 1 : length(years))
{
  pathyear = paste(path,years[x],sep="/")
  writeyear = paste(pathwrite,years[x],sep="/")
  checkdir(writeyear)
  months = dir(pathyear)
  for(y in  1: length(months))
  {
    pathmonth = paste(pathyear,months[y],sep="/")
    writemonth = paste(writeyear,months[y],sep="/")
    checkdir(writemonth)
    days = dir(pathmonth)
    sumfilename = paste("[VN-001S] ",substr(months[y],3,4),substr(months[y],6,7),".txt",sep="")
    for(z in 1 : length(days))
    {
      dataread = read.table(paste(pathmonth,days[z],sep="/"),sep="\t",header = T)
      datawrite = prepareSumm(dataread)
      datasum = rewriteSumm(datawrite)
			currdayw = gsub("722","VN-001S",days[z])
      write.table(datawrite,file = paste(writemonth,currdayw,sep="/"),append = F,sep="\t",row.names = F,col.names = T)
      {
        if(!file.exists(paste(writemonth,sumfilename,sep="/")) || (x == 1 && y == 1 && z==1))
        {
          write.table(datasum,file = paste(writemonth,sumfilename,sep="/"),append = F,sep="\t",row.names = F,col.names = T)
        }
        else 
        {
          write.table(datasum,file = paste(writemonth,sumfilename,sep="/"),append = T,sep="\t",row.names = F,col.names = F)
        }
      }
    }
  }
}
