source('/home/admin/CODE/common/aggregate.R')

registerMeterList("KH-003X",c("CKL","CKX","SLX"))
aggNameTemplate = getNameTemplate()
aggColTemplate = getColumnTemplate()

aggColTemplate[1] = 1 #Column no for date
aggColTemplate[2] = 5 #Column no for DA
aggColTemplate[3] = 8 #column for LastRead
aggColTemplate[4] = 7 #column for LastTime
aggColTemplate[5] = 2 #column for Eac-1
aggColTemplate[6] = 3 #column for Eac-2
aggColTemplate[7] = 9 #column for Yld-1
aggColTemplate[8] = 13 #column for Yld-2
aggColTemplate[9] = 11 #column for PR-1
aggColTemplate[10] = 14 #column for PR-2
aggColTemplate[11] = 15 #column for Irr
aggColTemplate[12] = 13 # IrrSrc Value
aggColTemplate[13] = NA #column for Tamb
aggColTemplate[14] = NA #column for Tmod
aggColTemplate[15] = NA #column for Hamb

registerColumnList("KH-003X","CKL",aggNameTemplate,aggColTemplate)

aggNameTemplate = getNameTemplate()
aggColTemplate = getColumnTemplate()

aggColTemplate[1] = 1 #Column no for date
aggColTemplate[2] = 5 #Column no for DA
aggColTemplate[3] = 8 #column for LastRead
aggColTemplate[4] = 7 #column for LastTime
aggColTemplate[5] = 2 #column for Eac-1
aggColTemplate[6] = 3 #column for Eac-2
aggColTemplate[7] = 11 #column for Yld-1
aggColTemplate[8] = 12 #column for Yld-2
aggColTemplate[9] = 14 #column for PR-1
aggColTemplate[10] = 10 #column for PR-2
aggColTemplate[11] = 15 #column for Irr
aggColTemplate[12] = 13 # IrrSrc Value
aggColTemplate[13] = NA #column for Tamb
aggColTemplate[14] = NA #column for Tmod
aggColTemplate[15] = NA #column for Hamb

registerColumnList("KH-003X","CKX",aggNameTemplate,aggColTemplate)

aggNameTemplate = getNameTemplate()
aggColTemplate = getColumnTemplate()

aggColTemplate[1] = 1 #Column no for date
aggColTemplate[2] = 5 #Column no for DA
aggColTemplate[3] = 8 #column for LastRead
aggColTemplate[4] = 7 #column for LastTime
aggColTemplate[5] = 2 #column for Eac-1
aggColTemplate[6] = 3 #column for Eac-2
aggColTemplate[7] = 11 #column for Yld-1
aggColTemplate[8] = 12 #column for Yld-2
aggColTemplate[9] = 10 #column for PR-1
aggColTemplate[10] = 14 #column for PR-2
aggColTemplate[11] = 15 #column for Irr
aggColTemplate[12] = 13 # IrrSrc Value
aggColTemplate[13] = NA #column for Tamb
aggColTemplate[14] = NA #column for Tmod
aggColTemplate[15] = NA #column for Hamb

registerColumnList("KH-003X","SLX",aggNameTemplate,aggColTemplate)

